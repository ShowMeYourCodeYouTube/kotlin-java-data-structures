# CONTRIBUTING

- Follow [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).
    - References:
      - https://gist.github.com/joshbuchea/6f47e86d2510bce28f8e7f42ae84c716
      - https://ec.europa.eu/component-library/v1.15.0/eu/docs/conventions/git/
- If your custom data structures are not detected by PITest library, add them to the classpath in `build.gradle.kts` (PitestPluginExtension).
  - Please notice that poorly tested data structures will make mutation tests running longer. Before adding them, be sure you covered edge cases and wrote solid tests.
  - Move `main` methods outside the package which will be tested. See the project structure.
  - Keep pipline execution time reasonable.

## Format files using ktlint

```
./gradlew ktlintFormat
```

Ref: https://gantsign.com/ktlint-maven-plugin/usage.html

## Coding conventions

Ref: https://kotlinlang.org/docs/reference/coding-conventions.html#naming-rules

### Naming rules - packages

Names of packages are always lower case and do not use underscores (org.example.myproject). Using multi-word names is generally discouraged, but if you do need to use multiple words, you can either simply concatenate them together or use camel humps (org.example.myProject). Ref: https://kotlinlang.org/docs/coding-conventions.html#naming-rules
