# Data structures in Kotlin and Java

| Branch |                                                                                                   Pipeline                                                                                                   |                                                                                                                  Code coverage                                                                                                                   |                                          Jacoco test report                                          |                                            PiTest report                                             |                                 SonarCloud                                 |
|:------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------:|:--------------------------------------------------------------------------:|
| master | [![pipeline status](https://gitlab.com/ShowMeYourCodeYouTube/java-kotlin-data-structures/badges/master/pipeline.svg)](https://gitlab.com/ShowMeYourCodeYouTube/java-kotlin-data-structures/-/commits/master) |                   [![coverage report](https://gitlab.com/ShowMeYourCodeYouTube/java-kotlin-data-structures/badges/master/coverage.svg)](https://gitlab.com/ShowMeYourCodeYouTube/java-kotlin-data-structures/-/commits/master)                   | [link](https://showmeyourcodeyoutube.gitlab.io/java-kotlin-data-structures/jacoco-report/index.html) | [link](https://showmeyourcodeyoutube.gitlab.io/java-kotlin-data-structures/pitest-report/index.html) | [link](https://sonarcloud.io/organizations/showmeyourcodeyoutube/projects) |

*The repository contains some exercises related to data structure. If you need more exercises, check out [Java Kotlin playground](https://gitlab.com/ShowMeYourCodeYouTube/java-kotlin-playground).*

---

***A data structure is a specialized format for organizing, processing, retrieving and storing data.**
There are several basic and advanced types of data structures, all designed to arrange data to suit a specific purpose. Data structures make it easy for users to access and work with the data they need in appropriate ways.* <https://searchsqlserver.techtarget.com/definition/data-structure>

---

The repository has samples of below data structures:

- Array (standard, Java & Kotlin)
- Stack (standard, Java & Kotlin)
- Queue (standard, Java & Kotlin)
- List
  - Singly-Linked List (custom, Java & Kotlin)
  - Skip List (custom, Kotlin)
- Heap (custom, Java & Kotlin)
- Graph (custom, Java & Kotlin)
  - BFS & DFS (traversal & cycle detection)
  - Dijkstra algorithm (the shortest path)
  - Minimum Spanning Tree
    - Prim's algorithm
    - Kruskal's algorithm
- Tree (custom, Java & Kotlin)
  - Binary Search Tree (BST)
    - DFS (in-order, pre-order, post-order), BFS (level order) (traversal)
  - Ternary Tree
    - DFS (in-order, pre-order, post-order), BFS (level order) (traversal)
  - Self-Balancing Tree
    - AVL Tree
      - DFS (in-order, pre-order, post-order), BFS (level order) (traversal)
    - Red-Black Tree
      - DFS (in-order, pre-order, post-order), BFS (level order) (traversal)
      - *The `delete` method hasn't been implemented yet. Only insertion works.*

Resources to check:
- [Ford-Fulkerson Algorithm for Maximum Flow Problem](https://www.geeksforgeeks.org/ford-fulkerson-algorithm-for-maximum-flow-problem/)
- https://www.geeksforgeeks.org/binary-search-tree-data-structure/
  - Easy Standard Problems on BST
  - Medium Standard Problems on BST
  - Hard Standard Problems on BST

## Data structures complexity

**Big O notation** is just a fancy way of describing how your code’s performance depends on the amount of data its processing. It allows us to calculate the worst possible runtime of an algorithm, or how long it takes the algorithm to complete. In other words, it informs us of how much it will slow down based on the input size. Ref: https://medium.com/@mendozabridget777/big-o-notation-time-space-complexity-9bc31952052c

![Big-O Complexity Chart](./docs/big-o-cheat-sheet-poster.png)  

Reference: https://www.bigocheatsheet.com/

More detailed description of each data structure [here](./DATA_STRUCTURE_DESCRIPTIONS.md).

## Kotlin (stdlib)

![Kotlin collections](./docs/kotlin-collections-diagram.png)

Kotlin's collection overview: https://kotlinlang.org/docs/collections-overview.html

The following collection types are relevant for Kotlin:
- **List** is an ordered collection with access to elements by indices – integer numbers that reflect their position. Elements can occur more than once in a list. An example of a list is a telephone number: it's a group of digits, their order is important, and they can repeat.
- **Set** is a collection of unique elements. It reflects the mathematical abstraction of set: a group of objects without repetitions. Generally, the order of set elements has no significance. For example, the numbers on lottery tickets form a set: they are unique, and their order is not important.
- **Map (or dictionary)** is a set of key-value pairs. Keys are unique, and each of them maps to exactly one value. The values can be duplicates. Maps are useful for storing logical connections between objects, for example, an employee's ID and their position.

*Usually data structures in Kotlin are **immutable** and if you want to use mutable collections,   
you have to explicitly use mutable collections e.g. `mutableListOf() instead of listOf()`.*

*Other interfaces e.g. queue or deque are available in Java SDK.*

The Kotlin Standard Library provides implementations for basic collection types: sets, lists, and maps. A pair of interfaces represent each collection type:
- A read-only interface that provides operations for accessing collection elements.
- A mutable interface that extends the corresponding read-only interface with write operations: adding, removing, and updating its elements.

> The read-only collection types are covariant. This means that, if a Rectangle class inherits from Shape, you can use a List<Rectangle> anywhere the List<Shape> is required. In other words, the collection types have the same subtyping relationship as the element types. Maps are covariant on the value type, but not on the key type.

> In turn, mutable collections aren't covariant; otherwise, this would lead to runtime failures. If MutableList<Rectangle> was a subtype of MutableList<Shape>, you could insert other Shape inheritors (for example, Circle) into it, thus violating its Rectangle type argument.

- Array (a fixed size sequence of ordered elements)
  - ArrayDeque
    - can act as Stack or Queue
- List (a dynamic-sized sequence of ordered elements)
  - ArrayList
  - ReversedList
- Set (a collection that has no duplicates)
  - HashSet
    - The default implementation of MutableSet – LinkedHashSet.
  - LinkedHashSet
    - The default implementation of MutableMap – LinkedHashMap.
    - HashSet is an unordered & unsorted collection of the data set, whereas the LinkedHashSet is an ordered and sorted collection of HashSet.
    - Use HashSet if you don't want to maintain any order of elements. 
    - Use LinkedHashSet if you want to maintain insertion order of elements. 
    - Use TreeSet if you want to sort the elements according to some Comparator.
- Map (an object that maps keys to values)
  - HashMap
  - LinkedHashMap

---

**Array vs List**

> The major difference from usage side is that Arrays have a fixed size 
> while (Mutable)List can adjust their size dynamically.   
> Moreover, an array is mutable whereas List is not.

- `Array<T>` is a class with known implementation: it's a sequential fixed-size memory region storing the items (and on JVM it is represented by Java array).
- `Array<T>` is mutable (it can be changed through any reference to it), but `List<T>` doesn't have modifying methods (it is either read-only view of MutableList<T> or an immutable list implementation).
- Arrays have fixed size and cannot expand or shrink retaining identity (you need to copy an array to resize it). As to the lists, MutableList<T> has `add` and `remove` functions, so that it can increase and reduce its size.
- `Array<T>` is invariant on T (Array<Int> is not Array<Number>), the same for MutableList<T>, but List<T> is covariant (List<Int> is List<Number>).

Ref: https://stackoverflow.com/questions/36262305/difference-between-list-and-array-types-in-kotlin

---

**Kotlin's concurrent collections**

Kotlin's standard library (kotlin.collections) primarily provides immutable collections, 
and the standard library does not include specific concurrent collections as part of kotlin.collections. 
However, Kotlin has interoperability with Java, and you can use Java's concurrent collections seamlessly in Kotlin.

## Java (JDK)

![Java API](./docs/data-structures-and-java-apis.jpg)  

Reference: https://www.lavivienpost.com/data-structures-and-java-collections/

![Java Collections](./docs/java-collections.png)  

Reference: https://www.toolsqa.com/java/data-structure/

![Collections](docs/collection-framework.png)

Reference: https://www.geeksforgeeks.org/set-in-java/

![Collections](docs/delayqueue-class-hierarchy.png)

Reference: https://www.happycoders.eu/algorithms/delayqueue-java/

Java 11 Oracle reference: https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/doc-files/coll-reference.html

### Collection interfaces

- **Collection** - A group of objects. No assumptions are made about the order of the collection (if any) or whether it can contain duplicate elements.
- **Set** - The familiar set abstraction. No duplicate elements permitted. May or may not be ordered. Extends the Collection interface.
- **List** - Ordered collection, also known as a sequence. Duplicates are generally permitted. Allows positional access. Extends the Collection interface.
- **Queue** - A collection designed for holding elements before processing. Besides basic Collection operations, queues provide additional insertion, extraction, and inspection operations.
- **Deque** - A double ended queue, supporting element insertion and removal at both ends. Extends the Queue interface.
- **Map** - A mapping from keys to values. Each key can map to one value.
- **SortedSet** - A set whose elements are automatically sorted, either in their natural ordering (see the Comparable interface) or by a Comparator object provided when a SortedSet instance is created. Extends the Set interface. 
- **SortedMap** - A map whose mappings are automatically sorted by key, either using the natural ordering of the keys or by a comparator provided when a SortedMap instance is created. Extends the Map interface.
- **NavigableSet** - A SortedSet extended with navigation methods reporting closest matches for given search targets. A NavigableSet may be accessed and traversed in either ascending or descending order.
- **NavigableMap** - A SortedMap extended with navigation methods returning the closest matches for given search targets. A NavigableMap can be accessed and traversed in either ascending or descending key order.
- **BlockingQueue** - A Queue with operations that wait for the queue to become nonempty when retrieving an element and that wait for space to become available in the queue when storing an element. (This interface is part of the java.util.concurrent package.)
- **TransferQueue** - A BlockingQueue in which producers can wait for consumers to receive elements. (This interface is part of the java.util.concurrent package.)
- **BlockingDeque** - A Deque with operations that wait for the deque to become nonempty when retrieving an element and wait for space to become available in the deque when storing an element. Extends both the Deque and BlockingQueue interfaces. (This interface is part of the java.util.concurrent package.)
- **ConcurrentMap** - A Map with atomic putIfAbsent, remove, and replace methods. (This interface is part of the java.util.concurrent package.)
- **ConcurrentNavigableMap** - A ConcurrentMap that is also a NavigableMap.

### General-purpose implementations

- **HashSet** - Hash table implementation of the Set interface. The best all-around implementation of the Set interface.
- **TreeSet** - Red-black tree implementation of the NavigableSet interface.
- **LinkedHashSet** - Hash table and linked list implementation of the Set interface. An insertion-ordered Set implementation that runs nearly as fast as HashSet.
- **ArrayList** - Resizable array implementation of the List interface (an unsynchronized Vector). The best all-around implementation of the List interface.
- **ArrayDeque** - Efficient, resizable array implementation of the Deque interface.
- **LinkedList** - Doubly-linked list implementation of the List interface. Provides better performance than the ArrayList implementation if elements are frequently inserted or deleted within the list. Also implements the Deque interface. When accessed through the Queue interface, LinkedList acts as a FIFO queue.
- **PriorityQueue** - Heap implementation of an unbounded priority queue.
- **HashMap** - Hash table implementation of the Map interface (an unsynchronized Hashtable that supports null keys and values). The best all-around implementation of the Map interface.
- **TreeMap** - Red-black tree implementation of the NavigableMap interface.
- **LinkedHashMap*** - Hash table and linked list implementation of the Map interface. An insertion-ordered Map implementation that runs nearly as fast as HashMap. Also useful for building caches (see removeEldestEntry(Map.Entry) ).


| Collection       | Interface      | Maintains Insertion Order? | Sorted Order? | Time Complexity (Avg)                      | Time Complexity (Worst)      |  
|-----------------|---------------|----------------------------|--------------|--------------------------------------------|------------------------------| 
| ArrayList      | List         | ✅                         | ❌            | O(1) (get), O(1)/O(n) (add), O(n) (remove) | O(n)                      |  
| LinkedList     | List, Deque | ✅                        | ❌            | O(1) (add/remove first/last), O(n) (get)   | O(n)  |  
| HashSet       | Set          | ❌                         | ❌            | O(1) (add/remove/contains)                 | O(n) (hash collisions)     |  
| LinkedHashSet | Set          | ✅                         | ❌            | O(1) (add/remove/contains)                 | O(n) (hash collisions)     |  
| TreeSet       | NavigableSet | ❌                         | ✅            | O(log n) (add/remove/contains)             | O(log n)           |  
| HashMap       | Map          | ❌                         | ❌            | O(1) (put/get/remove)                      | O(n) (hash collisions)       |  
| LinkedHashMap | Map          | ✅                         | ❌            | O(1) (put/get/remove)                      | O(n) (hash collisions)       |  
| TreeMap       | NavigableMap | ❌                         | ✅            | O(log n) (put/get/remove)                  | O(log n)                    |

### Concurrent implementations

These implementations are part of `java.util.concurrent`:
- **CopyOnWriteArrayList** - A thread-safe variant of ArrayList where all modifications (add, set, remove, etc.) create a new copy of the underlying array. Writing (add/remove) is slow since it creates a new copy of the list.
- **ConcurrentLinkedQueue** - An unbounded first in, first out (FIFO) queue based on linked nodes.
- **LinkedBlockingQueue** - An optionally bounded FIFO blocking queue backed by linked nodes.
  - It's called a blocking queue because it provides built-in synchronization mechanisms that block threads when operations cannot be completed immediately:
    - If the queue is full, a thread calling put() will wait (block) until space becomes available.
    - If the queue is empty, a thread calling take() will wait (block) until an element becomes available.
- **ArrayBlockingQueue** - A bounded FIFO blocking queue backed by an array.
  - Use LinkedBlockingQueue if you need better concurrency and dynamic sizing.
  - Use ArrayBlockingQueue if memory efficiency and a fixed queue size are important.
- **PriorityBlockingQueue** - An unbounded blocking priority queue backed by a priority heap.
- **DelayQueue** - A time-based scheduling queue backed by a priority heap.
- **SynchronousQueue** - A simple rendezvous mechanism that uses the BlockingQueue interface.
- **LinkedBlockingDeque** - An optionally bounded FIFO blocking deque backed by linked nodes.
- **LinkedTransferQueue** - An unbounded TransferQueue backed by linked nodes.
- **ConcurrentSkipListSet** - A thread-safe list implementation of the NavigableSet interface.
  - It supports logarithmic (O(log n)) time complexity for operations like add(), remove(), and contains().
  - It allows lock-free, high-concurrency access compared to TreeSet, which is backed by a TreeMap with explicit locking.
- **CopyOnWriteArraySet** -  A Set implementation which ensures thread safety by copying the entire underlying array every time an element is added or removed. This makes it safe for concurrent reads and modifications.
  - The copy-on-write mechanism leads to higher overhead for writes, as it involves creating a new copy of the underlying array. This operation is O(n) because it requires copying the array.
- **ConcurrentHashMap** - A highly concurrent, high-performance ConcurrentMap implementation based on a hash table. This implementation never blocks when performing retrievals and enables the client to select the concurrency level for updates. It is intended as a drop-in replacement for Hashtable. In addition to implementing ConcurrentMap, it supports all of the legacy methods of Hashtable.
- **ConcurrentSkipListMap** - Skips list implementation of the ConcurrentNavigableMap interface.

| Collection            | Interface     | Maintains Insertion Order? | Sorted Order? | Time Complexity (Avg)               | Time Complexity (Worst)              |  
|-----------------------|---------------|----------------------------|---------------|-------------------------------------|--------------------------------------|
| CopyOnWriteArrayList  | List          | ✅                          | ❌             | O(n) (add/remove), O(1) (get)       | O(n)                                 |  
| ConcurrentLinkedQueue | Queue         | ❌                          | ❌             | O(1) (offer/poll)                   | O(1)                                 |  
| LinkedBlockingQueue   | BlockingQueue | ✅                          | ❌             | O(1) (offer/poll)                   | O(1)                                 |  
| ArrayBlockingQueue    | BlockingQueue | ✅                          | ❌             | O(1) (offer/poll)                   | O(1)                                 |  
| PriorityBlockingQueue | BlockingQueue | ❌                          | ✅             | O(log n) (offer/poll)               | O(log n)                             |
| CopyOnWriteArraySet   | Set           | ❌                          | ❌             | O(1) (add) & O(n) (remove/contains) | O(n) (when re-allocating during add) |
| ConcurrentSkipListSet | NavigableSet  | ❌                          | ✅             | O(log n) (add/remove/contains)      | O(log n)                             | 
| ConcurrentHashMap     | ConcurrentMap | ❌                          | ❌             | O(1) (put/get/remove)               | O(n) (rare)                          |  
| ConcurrentSkipListMap | NavigableMap  | ❌                          | ✅             | O(log n) (put/get/remove)           | O(log n)                             |


### Sequenced Collections

Java 21 Oracle reference: https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/util/doc-files/coll-overview.html

Java 21 introduces new interfaces to represent collections with a defined encounter order. Each such collection has a well-defined first element, second element, and so forth, up to the last element. It also provides uniform APIs for accessing its first and last elements, and for processing its elements in reverse order.

**It doesn't introduce new collections (only the interface).**

Reference: https://openjdk.org/jeps/431

```java
interface SequencedCollection<E> extends Collection<E> {

    // new method
    SequencedCollection<E> reversed();

    // methods promoted from Deque
    void addFirst(E);
    void addLast(E);

    E getFirst();
    E getLast();

    E removeFirst();
    E removeLast();
}
```

### Convenience implementations

High-performance "mini-implementations" of the collection interfaces:
- **Arrays.asList** - Enables an array to be viewed as a list.
- **emptySet, emptyList and emptyMap** - Return an immutable empty set, list, or map.
- **singleton, singletonList, and singletonMap** - Return an immutable singleton set, list, or map, containing only the specified object (or key-value mapping).
- **Collections.nCopies()** - Returns an immutable list consisting of n copies of a specified object.

Wrapper implementations - Functionality-enhancing implementations for use with other implementations. Accessed solely through static factory methods.
- **Collections.unmodifiableInterface** - Returns an unmodifiable view of a specified collection that throws an UnsupportedOperationException if the user attempts to modify it.
- **Collections.synchronizedInterface** - Returns a synchronized collection that is backed by the specified (typically unsynchronized) collection. As long as all accesses to the backing collection are through the returned collection, thread safety is guaranteed.
- **Collections.checkedInterface** - Returns a dynamically type-safe view of the specified collection, which throws a ClassCastException if a client attempts to add an element of the wrong type. The generics mechanism in the language provides compile-time (static) type checking, but it is possible to bypass this mechanism. Dynamically type-safe views eliminate this possibility.

Adapter implementations - Implementations that adapt one collections interface to another:
- **newSetFromMap(Map)** - Creates a general-purpose Set implementation from a general-purpose Map implementation.
- **asLifoQueue(Deque)** - Returns a view of a Deque as a Last In First Out (LIFO) Queue.

## Independent libraries

### Vavr (Java)

Vavr (formerly called Javaslang) is a functional library for Java 8+ that provides persistent data types and functional control structures.   
Reference: https://docs.vavr.io/

![img](./docs/vavr-overview.png)

- https://docs.vavr.io/
- https://github.com/vavr-io/vavr-kotlin
- http://blog.vavr.io/functional-data-structures-in-java-8-with-javaslang/

## Kotlin vs Java

- https://kotlinlang.org/docs/comparison-to-java.html
- https://nix-united.com/blog/kotlin-vs-java-which-is-the-winner/
- https://medium.com/@dharmasai.seerapu/from-java-to-kotlin-e4b1027d6cba
- https://medium.com/@dharmasai.seerapu/kotlin-vs-java-basic-syntax-differences-2a84c3bd9bc7

## Data structure exercises

- https://www.geeksforgeeks.org/binary-search-tree-data-structure/
- https://leetcode.com/problemset/all/
