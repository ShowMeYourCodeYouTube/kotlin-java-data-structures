plugins {
    java
    kotlin("jvm") version "1.9.25" apply false

    id("org.jlleitschuh.gradle.ktlint") version "11.6.1" apply false

    id("org.sonarqube") version "5.1.0.4882"
    id("com.adarshr.test-logger") version "3.2.0"
    id("info.solidsoft.pitest") version "1.15.0" apply false
    jacoco
}

allprojects {
    group = "com.showmeyourcode"
    version = "0.0.1-SNAPSHOT"

    apply(plugin = "java")
    apply(plugin = "com.adarshr.test-logger")
    apply(plugin = "org.sonarqube")

    repositories {
        mavenCentral()
    }

    java {
        toolchain {
            languageVersion.set(JavaLanguageVersion.of(17))
        }
    }

    testlogger {
        theme = com.adarshr.gradle.testlogger.theme.ThemeType.MOCHA
        slowThreshold = 4000
        showSummary = true
        showSimpleNames = true
        showStandardStreams = true
        showPassedStandardStreams = false
        showSkippedStandardStreams = false
        showFailedStandardStreams = true
    }

    sonar {
        properties {
            property("sonar.projectKey", "ShowMeYourCodeYouTube_kotlin-java-data-structures")
            property("sonar.organization", "showmeyourcodeyoutube")
            property("sonar.core.codeCoveragePlugin", "jacoco")
            property(
                "sonar.coverage.jacoco.xmlReportPaths",
                "$rootDir/coverage-report/build/reports/jacoco/testCodeCoverageReport/testCodeCoverageReport.xml"
            )
        }
    }
}

val kotestVersion = "5.9.1"
val kotestPitestVersion = "1.2.0"
val slf4jVersion = "1.7.36"
val logbackVersion = "1.5.17"
val junitJupiterVersion = "5.11.4"

subprojects {
    apply(plugin = "java")
    apply(plugin = "kotlin")
    apply(plugin = "jacoco")

    val implementation by configurations
    val testRuntimeOnly by configurations
    val testImplementation by configurations

    dependencies {
        implementation("org.slf4j:slf4j-api:$slf4jVersion")
        implementation("ch.qos.logback:logback-classic:$logbackVersion")
        implementation("ch.qos.logback:logback-core:$logbackVersion")

        // required to create a Gradle test report
        testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitJupiterVersion")
        testImplementation("org.junit.jupiter:junit-jupiter-params:$junitJupiterVersion")
    }

    tasks.test {
        // report is always generated after tests run
        finalizedBy(tasks.jacocoTestReport)
    }
    tasks.jacocoTestReport {
        // tests are required to run before generating the report
        dependsOn(tasks.test)
    }

    tasks.test {
        useJUnitPlatform()
    }
}
