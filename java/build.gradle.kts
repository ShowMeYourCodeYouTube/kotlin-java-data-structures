plugins {
    id("io.freefair.lombok") version "6.6.3"
}

val varVersion = "0.10.6"
val awaitilityVersion = "3.1.6"
val logCaptorVersion = "2.10.1"

dependencies {
    api(project(":common"))

    // functional programming
    implementation("io.vavr:vavr:$varVersion")

    // other test libraries
    testImplementation("org.awaitility:awaitility:$awaitilityVersion")
    testImplementation("io.github.hakky54:logcaptor:$logCaptorVersion")

}
