package com.showmeyourcode.projects.datastructures.java.standard.queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.SecureRandom;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.TimeUnit;

public class QueueOverview {

    private static final Logger LOGGER = LoggerFactory.getLogger(QueueOverview.class);

    private QueueOverview() {
    }

    public static void main(String[] args) {
        LOGGER.info(
                """
                        A Queue is defined as a linear data structure that is open at both ends
                        and the operations are performed in First In First Out (FIFO) order.
                        
                        """
        );
        queue();
        immutableQueue();

        concurrent();
    }

    private static void immutableQueue() {
        var immutableList = Collections.unmodifiableCollection(new ArrayDeque<>(List.of(1, 3, 5, 2, 4)));

        LOGGER.info("Immutable deck (Collections.unmodifiableList): {}", immutableList);
    }

    private static void concurrent() {
        DelayedElement.concurrentBlockingQueue();
        // BlockingQueue implementations are designed to be used primarily for producer-consumer queues.
        // LinkedBlockingQueue blocks the consumer or the producer
        // when the queue is empty or full and the respective consumer/producer thread is put to sleep.
        // But this blocking feature comes with a cost: every put or take operation is lock contended
        // between the producers or consumers (if many),
        // so in scenarios with many producers/consumers the operation might be slower.
        var linkedBlockingDeque = new LinkedBlockingDeque<Integer>();
        // BlockingQueue guarantees that the element made by producer must be in the queue,
        // while TransferQueue gets one step further, it guarantees that the element "consumed" by some consumer.
        // https://stackoverflow.com/questions/7317579/difference-between-blockingqueue-and-transferqueue
        var linkedTransferQueue = new LinkedTransferQueue<Integer>();
        // ConcurrentLinkedQueue is not using locks, but CAS (compare-and-swap), on its add/poll operations potentially reducing contention
        // with many producer and consumer threads.
        // But being a "wait free" data structure, ConcurrentLinkedQueue will not block when empty,
        // meaning that the consumer will need to deal with the poll() returning null values by "busy waiting",
        // for example, with the consumer thread eating up CPU.
        //
        // One particular use case where the ConcurrentLinkedQueue is clearly better is when producers first produce something
        // and finish their job by placing the work in the queue and only after the consumers starts to consume,
        // knowing that they will be done when queue is empty.
        //
        // ConcurrentLinkedQueue.poll uses CAS to maintain thread safety.
        // LinkedBlockingQueue.poll uses ReentrantLock (is owned by the thread last successfully locking, but not yet unlocking it)!
        // So it is slower. But provides blocking functionality.
        // ConcurrentLinkedQueue is just a thread safe linked list.
        // Ref: https://stackoverflow.com/questions/1426754/linkedblockingqueue-vs-concurrentlinkedqueue
        var concurrentLinkedQueue = new ConcurrentLinkedQueue<Integer>();
        var concurrentLinkedDeque = new ConcurrentLinkedDeque<Integer>();
        // new SynchronousQueue() vs new LinkedBlockingQueue(1)
        // The SynchronousQueue is more of a handoff, whereas the LinkedBlockingQueue just allows a single element.
        // The difference being that the put() call to a SynchronousQueue will not return until there is a corresponding take() call, but with a LinkedBlockingQueue of size 1, the put() call (to an empty queue) will return immediately.
        // Ref: https://stackoverflow.com/questions/8591610/when-should-i-use-synchronousqueue-over-linkedblockingqueue
        var synchronousQueue = new SynchronousQueue<Integer>();
        // The priority part in the queues you are using simply means the items are read from the queue in a specific order
        // (either natural if they implement Comparable or according to a Comparator).
        var priorityBlockingQueue = new PriorityBlockingQueue<Integer>();
        var blockingQueue = new ArrayBlockingQueue<Integer>(5);

        concurrentLinkedQueue.add(5);
        concurrentLinkedQueue.add(1);
        concurrentLinkedQueue.add(3);

        concurrentLinkedDeque.add(5);
        concurrentLinkedDeque.add(1);
        concurrentLinkedDeque.add(3);

        linkedBlockingDeque.add(5);
        linkedBlockingDeque.add(1);
        linkedBlockingDeque.add(3);

        linkedTransferQueue.add(5);
        linkedTransferQueue.add(1);
        linkedTransferQueue.add(3);

        // A synchronous queue does not have any internal capacity, not even a capacity of one.
        // When we want to add an element to the queue, we need to call the put() method.
        // That method will block until some other thread calls the take() method,
        // signaling that it is ready to take an element.
        (new Thread(() -> {
            try {
                synchronousQueue.take();
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        })).start();

        try {
            synchronousQueue.put(5);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        priorityBlockingQueue.add(5);
        priorityBlockingQueue.add(1);
        priorityBlockingQueue.add(3);

        blockingQueue.add(5);
        blockingQueue.add(1);
        blockingQueue.add(3);

        LOGGER.info("\nConcurrent queue (ConcurrentLinkedQueue): {}", concurrentLinkedQueue);
        LOGGER.info("Concurrent queue (ConcurrentLinkedDeque): {}", concurrentLinkedDeque);
        LOGGER.info("Concurrent queue (LinkedBlockingDeque): {}", linkedBlockingDeque);
        LOGGER.info("Concurrent queue (LinkedTransferQueue): {}", linkedTransferQueue);
        LOGGER.info("Concurrent queue (SynchronousQueue): {}", synchronousQueue);
        LOGGER.info("Concurrent queue (PriorityBlockingQueue): {}", priorityBlockingQueue);
        LOGGER.info("Concurrent queue (ArrayBlockingQueue): {}", blockingQueue);
    }

    private static void queue() {
        // A PriorityQueue is used when the objects are supposed to be processed based on the priority.
        // It is known that a Queue follows the First-In-First-Out algorithm,
        // but sometimes the elements of the queue are needed to be processed according to the priority,
        // that’s when the PriorityQueue comes into play.
        // Ref: https://www.geeksforgeeks.org/priority-queue-class-in-java/
        Queue<Integer> priorityQueue = new PriorityQueue<>();
        Deque<Integer> arrayDequeue = new ArrayDeque<>();
        Deque<Integer> linkedList = new LinkedList<>();

        priorityQueue.add(3);
        priorityQueue.add(2);
        priorityQueue.add(1);

        arrayDequeue.add(3);
        arrayDequeue.add(2);
        arrayDequeue.add(1);

        linkedList.add(3);
        linkedList.add(2);
        linkedList.add(1);

        LOGGER.info("""
                        PriorityQueue: {}
                        ArrayDequeue: {}
                        LinkedList: {}
                        """,
                priorityQueue,
                arrayDequeue,
                linkedList
        );

        priorityQueue.remove();
        arrayDequeue.remove();
        linkedList.remove();

        LOGGER.info("""
                        === After removal ===
                        PriorityQueue: {}
                        ArrayDequeue: {}
                        LinkedList: {}
                        """,
                priorityQueue,
                arrayDequeue,
                linkedList
        );
    }
}

class DelayedElement<E extends Comparable<E>> implements Delayed {

    private static final Logger LOGGER = LoggerFactory.getLogger(DelayedElement.class);

    private final E e;
    private final long initialDelayMillis;
    private final long expiration;

    @SuppressWarnings("java:S899")
    // Reference: https://www.happycoders.eu/algorithms/delayqueue-java/
    static void concurrentBlockingQueue() {
        // BlockingQueue supports flow control by introducing blocking if either BlockingQueue is full or empty.
        // The insertion of elements takes place when the queue is not completely filled
        // otherwise the thread performing the operation will get blocked.
        //
        // The java.util.concurrent.DelayQueue class – just like the PriorityQueue it uses internally – is not a FIFO queue.
        // It does not take out the element that has been in the queue the longest.
        // Instead, an element can be taken when a wait time ("delay") assigned to that element has expired.
        //
        // Therefore, the elements must implement the interface java.util.concurrent.Delayed and its getDelay() method.
        // This method returns the remaining waiting time that must elapse before the element can be removed from the queue.
        BlockingQueue<DelayedElement<Integer>> queue = new DelayQueue<>();
        SecureRandom random = new SecureRandom();
        long startTime = System.currentTimeMillis();

        LOGGER.info("Concurrent queue (BlockingQueue): {}", queue);

        // Enqueue random numbers with random initial delays
        for (int i = 0; i < 7; i++) {
            int randomNumber = random.nextInt(10, 100);
            int initialDelayMillis = random.nextInt(100, 1000);
            DelayedElement<Integer> element = new DelayedElement<>(
                    randomNumber, initialDelayMillis
            );
            queue.offer(element);
            LOGGER.info(
                    String.format("[%3dms] queue.offer(%s)   --> queue = %s",
                            System.currentTimeMillis() - startTime,
                            element,
                            queue
                    )
            );
        }

        // Dequeue all elements
        while (!queue.isEmpty()) {
            try {
                DelayedElement<Integer> element = queue.take();
                LOGGER.info(
                        String.format("[%3dms] queue.poll() = %s --> queue = %s",
                                System.currentTimeMillis() - startTime,
                                element,
                                queue
                        )
                );
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    public DelayedElement(E e, long initialDelayMillis) {
        this.e = e;
        this.initialDelayMillis = initialDelayMillis;
        this.expiration = System.currentTimeMillis() + initialDelayMillis;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        long remainingDelayMillis = expiration - System.currentTimeMillis();
        return unit.convert(remainingDelayMillis, TimeUnit.MILLISECONDS);
    }

    @Override
    public int compareTo(Delayed o) {
        DelayedElement<?> other = (DelayedElement<?>) o;
        return Long.compare(expiration, other.expiration);
    }

    @Override
    public String toString() {
        return "{%s, %dms}".formatted(e, initialDelayMillis);
    }
}
