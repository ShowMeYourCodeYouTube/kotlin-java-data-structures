package com.showmeyourcode.projects.datastructures.java.custom.tree.tst;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class TernaryTreeNode<T extends Comparable<T>> {
    T value;
    TernaryTreeNode<T> left;
    TernaryTreeNode<T> middle;
    TernaryTreeNode<T> right;

    public TernaryTreeNode(T value) {
        this.value = value;
        this.left = null;
        this.middle = null;
        this.right = null;
    }
}

class TernaryTree<T extends Comparable<T>> {
    private TernaryTreeNode<T> root;

    public TernaryTree() {
        this.root = null;
    }

    public void insert(T value) {
        root = insertRecursive(root, value);
    }

    private TernaryTreeNode<T> insertRecursive(TernaryTreeNode<T> node, T value) {
        if (node == null) {
            return new TernaryTreeNode<>(value);
        }

        if (value.compareTo(node.value) < 0) {
            node.left = insertRecursive(node.left, value);
        } else if (value.compareTo(node.value) > 0) {
            node.right = insertRecursive(node.right, value);
        } else {
            node.middle = insertRecursive(node.middle, value);
        }

        return node;
    }

    public void delete(T value) {
        root = delete(root, value);
    }

    private TernaryTreeNode<T> delete(TernaryTreeNode<T> node, T value) {
        if (node == null) {
            return null;
        }

        if (value.compareTo(node.value) < 0) {
            node.left = delete(node.left, value);
        } else if (value.compareTo(node.value) > 0) {
            node.right = delete(node.right, value);
        } else {
            if (node.middle != null) {
                node.middle = delete(node.middle, value);
            } else if (node.right != null) {
                TernaryTreeNode<T> successor = findMin(node.right);
                node.value = successor.value;
                node.right = delete(node.right, successor.value);
            } else {
                return null;
            }
        }

        return node;
    }

    private TernaryTreeNode<T> findMin(TernaryTreeNode<T> node) {
        TernaryTreeNode<T> current = node;
        while (current.left != null) {
            current = current.left;
        }
        return current;
    }

    public boolean find(T value) {
        return findRecursive(root, value);
    }

    private boolean findRecursive(TernaryTreeNode<T> node, T value) {
        if (node == null) {
            return false;
        }

        if (value.compareTo(node.value) < 0) {
            return findRecursive(node.left, value);
        } else if (value.compareTo(node.value) > 0) {
            return findRecursive(node.right, value);
        } else {
            return true;
        }
    }

    public List<T> preOrderTraversal() {
        List<T> result = new ArrayList<>();
        preOrderRecursive(root, result);
        return result;
    }

    private void preOrderRecursive(TernaryTreeNode<T> node, List<T> result) {
        if (node != null) {
            result.add(node.value);
            preOrderRecursive(node.left, result);
            preOrderRecursive(node.middle, result);
            preOrderRecursive(node.right, result);
        }
    }

    public List<T> inOrderTraversal() {
        List<T> result = new ArrayList<>();
        inOrderRecursive(root, result);
        return result;
    }

    private void inOrderRecursive(TernaryTreeNode<T> node, List<T> result) {
        if (node != null) {
            inOrderRecursive(node.left, result);
            result.add(node.value);
            inOrderRecursive(node.middle, result);
            inOrderRecursive(node.right, result);
        }
    }

    public List<T> postOrderTraversal() {
        List<T> result = new ArrayList<>();
        postOrderRecursive(root, result);
        return result;
    }

    private void postOrderRecursive(TernaryTreeNode<T> node, List<T> result) {
        if (node != null) {
            postOrderRecursive(node.left, result);
            postOrderRecursive(node.middle, result);
            postOrderRecursive(node.right, result);
            result.add(node.value);
        }
    }

    public List<T> levelOrderTraversal() {
        List<T> result = new ArrayList<>();
        if (root != null) {
            Queue<TernaryTreeNode<T>> queue = new LinkedList<>();
            queue.add(root);

            while (!queue.isEmpty()) {
                TernaryTreeNode<T> current = queue.poll();
                result.add(current.value);

                if (current.left != null) {
                    queue.add(current.left);
                }
                if (current.middle != null) {
                    queue.add(current.middle);
                }
                if (current.right != null) {
                    queue.add(current.right);
                }
            }
        }
        return result;
    }
}
