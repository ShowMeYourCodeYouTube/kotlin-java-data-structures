package com.showmeyourcode.projects.datastructures.java.exercise.arrays;

import java.security.SecureRandom;

/**
 * Create a Matrix and implement transposition method.
 * <br>
 * Write a method to create a 2D array (matrix) of size m x n and fill it with random integers between a given range min and max.
 * <br>
 * <br>
 * Then implement the 'transposeMatrix' method.
 * <br>
 * In linear algebra, the transpose of a matrix is an operator which flips a matrix over its diagonal; that is, it switches the row and column indices of the matrix A by producing another matrix.
 */
class MatrixGeneratorExercise {

    private MatrixGeneratorExercise() {
    }

    public static int[][] createMatrix(int rows, int cols, int min, int max) {
        var random = new SecureRandom();
        var result = new int[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                result[i][j] = random.nextInt(min, max + 1);
            }
        }

        return result;
    }

    public static int[][] transposeMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            throw new IllegalArgumentException("Matrix cannot be null or empty");
        }

        int rows = matrix.length;
        int cols = matrix[0].length;
        var transposedMatrix = new int[cols][rows];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                transposedMatrix[j][i] = matrix[i][j];
            }
        }

        return transposedMatrix;
    }
}
