package com.showmeyourcode.projects.datastructures.java.custom.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public interface WeightedGraphRepresentation extends GraphRepresentation {
    void addEdge(int vertex1, int vertex2, double weight);

    List<Map.Entry<Integer, Double>> getNeighborsWithWeights(int vertex);

    // It needs another method name because of Java generic types erasure
    default void addWeightedEdges(List<List<Map.Entry<Integer, Double>>> adjacencyList) {
        for (int i = 0; i < vertices(); i++) {
            var neighbours = adjacencyList.get(i);
            int finalI = i;
            neighbours.forEach(edge -> {
                addEdge(finalI, edge.getKey(), edge.getValue());
            });
        }
    }
}

class WeightedAdjacencyList implements WeightedGraphRepresentation {
    private final List<List<Map.Entry<Integer, Double>>> adjacencyList;
    private final GraphType graphType;
    private final int vertices;

    public WeightedAdjacencyList(int vertices, GraphType graphType) {
        this.graphType = graphType;
        this.vertices = vertices;
        adjacencyList = new ArrayList<>();
        for (int i = 0; i < vertices; i++) {
            adjacencyList.add(new ArrayList<>());
        }
    }

    @Override
    public void addEdge(int vertex1, int vertex2, double weight) {
        adjacencyList.get(vertex1).add(Map.entry(vertex2, weight));

        if (graphType == GraphType.UNDIRECTED) {
            adjacencyList.get(vertex2).add(Map.entry(vertex1, weight));
        }
    }

    @Override
    public List<Map.Entry<Integer, Double>> getNeighborsWithWeights(int vertex) {
        return adjacencyList.get(vertex);
    }

    @Override
    public List<Integer> getNeighbors(int vertex) {
        return adjacencyList.get(vertex).stream().map(Map.Entry::getKey).toList();
    }

    @Override
    public List<List<Integer>> toList() {
        return IntStream.range(0, vertices).mapToObj(this::getNeighbors).toList();
    }

    @Override
    public void addEdge(int vertex1, int vertex2) {
        throw new UnsupportedOperationException("Cannot add an unweighted edge to the weighted graph.");
    }

    @Override
    public int vertices() {
        return vertices;
    }

    @Override
    public GraphType graphType() {
        return graphType;
    }
}

class WeightedAdjacencyMatrix implements WeightedGraphRepresentation {
    private final double[][] adjacencyMatrix;
    private final GraphType graphType;
    private final int vertices;

    public WeightedAdjacencyMatrix(int vertices, GraphType graphType) {
        this.graphType = graphType;
        this.vertices = vertices;
        adjacencyMatrix = new double[vertices][vertices];
        for (int i = 0; i < vertices; i++) {
            for (int j = 0; j < vertices; j++) {
                adjacencyMatrix[i][j] = Double.POSITIVE_INFINITY;
            }
        }
    }

    @Override
    public void addEdge(int vertex1, int vertex2, double weight) {
        adjacencyMatrix[vertex1][vertex2] = weight;

        if (graphType == GraphType.UNDIRECTED) {
            adjacencyMatrix[vertex2][vertex1] = weight;
        }
    }

    @Override
    public List<Map.Entry<Integer, Double>> getNeighborsWithWeights(int vertex) {
        List<Map.Entry<Integer, Double>> neighbors = new ArrayList<>();
        for (int i = 0; i < vertices; i++) {
            if (adjacencyMatrix[vertex][i] != Double.POSITIVE_INFINITY) {
                neighbors.add(Map.entry(i, adjacencyMatrix[vertex][i]));
            }
        }
        return neighbors;
    }

    @Override
    public List<Integer> getNeighbors(int vertex) {
        List<Integer> neighbors = new ArrayList<>();

        for (int i = 0; i < vertices; i++) {
            if (adjacencyMatrix[vertex][i] != Double.POSITIVE_INFINITY) {
                neighbors.add(i);
            }
        }

        return neighbors;
    }

    @Override
    public List<List<Integer>> toList() {
        List<List<Integer>> adjacencyList = new ArrayList<>();
        for (int i = 0; i < vertices; i++) {
            adjacencyList.add(getNeighbors(i));
        }

        return adjacencyList;
    }

    @Override
    public void addEdge(int vertex1, int vertex2) {
        throw new UnsupportedOperationException("Cannot add an unweighted edge to the weighted graph.");
    }

    @Override
    public int vertices() {
        return vertices;
    }

    @Override
    public GraphType graphType() {
        return graphType;
    }
}
