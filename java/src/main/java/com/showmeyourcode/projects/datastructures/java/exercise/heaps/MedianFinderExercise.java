package com.showmeyourcode.projects.datastructures.java.exercise.heaps;

import java.util.PriorityQueue;

public class MedianFinderExercise {
    private final PriorityQueue<Integer> maxHeap; // to store the smaller half of elements
    private final PriorityQueue<Integer> minHeap; // to store the larger half of elements

    public MedianFinderExercise() {
        maxHeap = new PriorityQueue<>((a, b) -> b - a); // max-heap (inverted priority)
        minHeap = new PriorityQueue<>(); // min-heap
    }

    public void addNum(int num) {
        if (maxHeap.isEmpty() || num <= maxHeap.peek()) {
            maxHeap.offer(num);
        } else {
            minHeap.offer(num);
        }

        // Balance the heaps
        if (maxHeap.size() > minHeap.size() + 1) {
            minHeap.offer(maxHeap.poll());
        } else if (minHeap.size() > maxHeap.size()) {
            maxHeap.offer(minHeap.poll());
        }
    }

    public double findMedian() {
        if (maxHeap.isEmpty()) {
            throw new IllegalStateException("No elements in the MedianFinder");
        }

        if (maxHeap.size() > minHeap.size()) {
            return maxHeap.peek();
        } else {
            // If both heaps have the same size, return the average of the two middle elements
            int maxHeapTop = maxHeap.peek();
            int minHeapTop = minHeap.peek();
            return (maxHeapTop + minHeapTop) / 2.0;
        }
    }
}
