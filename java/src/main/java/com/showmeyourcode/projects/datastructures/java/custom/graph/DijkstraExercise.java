package com.showmeyourcode.projects.datastructures.java.custom.graph;

import java.util.*;

public class DijkstraExercise {

    private DijkstraExercise() {

    }

    /**
     * https://neetcode.io/problems/dijkstra
     * <p>
     * Implement Dijkstra's shortest path algorithm.
     * <p>
     * Given a weighted, directed graph, and a starting vertex, return the shortest distance from the starting vertex to every vertex in the graph.
     * <p>
     * Input:
     * - n - the number of vertices in the graph, where (2 <= n <= 100). Each vertex is labeled from 0 to n - 1.
     * - edges - a list of tuples, each representing a directed edge in the form (u, v, w), where u is the source vertex, v is the destination vertex, and w is the weight of the edge, where (1 <= w <= 10).
     * - src - the source vertex from which to start the algorithm, where (0 <= src < n).
     * <p>
     * Note: If a vertex is unreachable from the source vertex, the shortest path distance for the unreachable vertex should be -1.
     */
    public static Map<Integer, Integer> shortestPath(int n, List<List<Integer>> edges, int src) {
        // the list of lists is a list of pairs where (d,w) for a given key/src
        var adj = new HashMap<Integer, List<int[]>>();
        for (int i = 0; i < n; i++) {
            adj.put(i, new ArrayList<>());
        }
        // fill the adjacency list
        for (List<Integer> edge : edges) {
            int u = edge.get(0), v = edge.get(1), w = edge.get(2);
            adj.get(u).add(new int[]{v, w});
        }

        var shortest = new HashMap<Integer, Integer>();
        // pq is the minimum heap contains pairs (node, w)
        var pq = new PriorityQueue<int[]>(Comparator.comparingInt(a -> a[1]));
        pq.offer(new int[]{src, 0});

        while (!pq.isEmpty()) {
            var node = pq.poll();
            int vertex = node[0], weight = node[1];

            if (shortest.containsKey(vertex)) {
                continue;
            }
            shortest.put(vertex, weight);

            List<int[]> neighbours = adj.get(vertex);

            for (int[] neighbour : neighbours) {
                int n1 = neighbour[0], w1 = neighbour[1];
                if (!shortest.containsKey(n1)) {
                    pq.offer(new int[]{n1, weight + w1});
                }
            }

        }

        for (int i = 0; i < n; i++) {
            shortest.putIfAbsent(i, -1);
        }

        return shortest;
    }
}
