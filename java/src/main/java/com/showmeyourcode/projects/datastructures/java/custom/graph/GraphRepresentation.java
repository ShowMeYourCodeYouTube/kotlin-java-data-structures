package com.showmeyourcode.projects.datastructures.java.custom.graph;

import java.util.ArrayList;
import java.util.List;

public interface GraphRepresentation {
    List<Integer> getNeighbors(int vertex);

    List<List<Integer>> toList();

    void addEdge(int vertex1, int vertex2);

    int vertices();

    GraphType graphType();

    /**
     * The 'adjacencyList' should have size equal to number of nodes.
     * The position in the list corresponds to a list of connections/edges to particular vertices
     * e.g. ((1,2),(2),() represents a graph where:
     * - vertex '0' has edges to 1,2 vertices,
     * - vertex '1' has a single edge to 2,
     * - vertex 2 doesn't have any outgoing edges.
     */
    default void addEdges(List<List<Integer>> adjacencyList) {
        for (int i = 0; i < adjacencyList.size(); i++) {
            var edges = adjacencyList.get(i);
            // Variables used in lambda expression should be final or effectively final
            int finalIdx = i;
            edges.forEach(edge -> addEdge(finalIdx, edge));
        }
    }
}

class AdjacencyList implements GraphRepresentation {
    private final GraphType graphType;
    private final int vertices;
    private final List<List<Integer>> adjacencyList;

    public AdjacencyList(int vertices, GraphType graphType) {
        this.graphType = graphType;
        this.vertices = vertices;
        adjacencyList = new ArrayList<>(vertices);
        for (int i = 0; i < vertices; i++) {
            adjacencyList.add(new ArrayList<>());
        }
    }

    @Override
    public List<Integer> getNeighbors(int vertex) {
        return adjacencyList.get(vertex);
    }

    @Override
    public List<List<Integer>> toList() {
        return adjacencyList;
    }

    @Override
    public void addEdge(int vertex1, int vertex2) {
        adjacencyList.get(vertex1).add(vertex2);

        if (graphType == GraphType.UNDIRECTED) {
            adjacencyList.get(vertex2).add(vertex1);
        }
    }

    @Override
    public int vertices() {
        return vertices;
    }

    @Override
    public GraphType graphType() {
        return graphType;
    }
}

class AdjacencyMatrix implements GraphRepresentation {

    private final GraphType graphType;
    private final boolean[][] adjacencyMatrix;

    public AdjacencyMatrix(int vertices, GraphType graphType) {
        this.graphType = graphType;
        adjacencyMatrix = new boolean[vertices][vertices];
    }

    @Override
    public List<Integer> getNeighbors(int vertex) {
        var result = new ArrayList<Integer>();
        var neighbours = adjacencyMatrix[vertex];
        for (int j = 0; j < adjacencyMatrix.length; j++) {
            if (neighbours[j]) {
                result.add(j);
            }
        }

        return result;
    }

    @Override
    public List<List<Integer>> toList() {
        var result = new ArrayList<List<Integer>>(adjacencyMatrix.length);
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            result.add(getNeighbors(i));
        }

        return result;
    }

    @Override
    public void addEdge(int vertex1, int vertex2) {
        adjacencyMatrix[vertex1][vertex2] = true;

        if (graphType == GraphType.UNDIRECTED) {
            adjacencyMatrix[vertex2][vertex1] = true;
        }
    }

    @Override
    public int vertices() {
        return adjacencyMatrix.length;
    }

    @Override
    public GraphType graphType() {
        return graphType;
    }
}