package com.showmeyourcode.projects.datastructures.java.custom.list;

import java.util.ArrayList;

/**
 * https://neetcode.io/problems/singlyLinkedList
 * <p>
 * Your LinkedList class should support the following operations:
 * - LinkedList() will initialize an empty linked list.
 * - int get(int i) will return the value of the ith node (0-indexed). If the index is out of bounds, return -1.
 * - void insertHead(int val) will insert a node with val at the head of the list.
 * - void insertTail(int val) will insert a node with val at the tail of the list.
 * - bool remove(int i) will remove the ith node (0-indexed). If the index is out of bounds, return false, otherwise return true.
 * - int[] getValues() return an array of all the values in the linked list, ordered from head to tail.
 */
class SinglyLinkedList {

    private Node head;
    private Node tail;

    public SinglyLinkedList() {
        head = null;
        head = null;
    }

    public int get(int index) {
        Node tmp = head;
        int i = 0;
        while (tmp != null) {
            if (i == index) {
                return tmp.value;
            }
            i++;
            tmp = tmp.next;
        }

        return -1;
    }

    public void insertHead(int val) {
        Node newHead = new Node(val, head);
        head = newHead;
        if (tail == null) {
            tail = head;
        }
    }

    public void insertTail(int val) {
        if (head == null) {
            head = new Node(val, null);
            tail = head;
        } else {
            Node newTail = new Node(val, null);
            tail.next = newTail;
            tail = newTail;
        }
    }

    public boolean remove(int index) {
        if (index == 0 && head != null) {
            head = head.next;
            if (head == null) {
                tail = null;  // If the list becomes empty, reset tail.
            }
            return true;
        }
        Node tmp = head;
        int i = 0;
        while (tmp != null) {
            if (i + 1 == index && tmp.next != null) {
                tmp.next = tmp.next.next;
                if (tmp.next == null) {
                    tail = tmp;  // Update tail if we removed the last node
                }
                return true;
            }
            i++;
            tmp = tmp.next;
        }
        return false; // Index out of bounds
    }

    public ArrayList<Integer> getValues() {
        ArrayList<Integer> values = new ArrayList<Integer>();
        Node tmp = head;
        while (tmp != null) {
            values.add(tmp.value);
            tmp = tmp.next;
        }
        return values;
    }

    private class Node {
        protected int value;
        protected Node next;

        private Node(int val, Node n) {
            value = val;
            next = n;
        }
    }
}
