package com.showmeyourcode.projects.datastructures.java.custom.graph;

public enum GraphType {
    DIRECTED,
    UNDIRECTED
}
