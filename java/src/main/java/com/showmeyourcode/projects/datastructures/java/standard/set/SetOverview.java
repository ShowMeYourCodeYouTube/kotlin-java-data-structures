package com.showmeyourcode.projects.datastructures.java.standard.set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArraySet;

public class SetOverview {

    private static final Logger LOGGER = LoggerFactory.getLogger(SetOverview.class);

    private SetOverview() {
    }

    public static void main(String[] args) {
        LOGGER.info(
                """
                        A set is a data structure that stores unique elements of the same type.
                        It is an unordered collection of objects in which duplicate values cannot be stored.
                        
                        """
        );
        immutableSet();
        mutableSet();

        concurrent();
    }

    private static void concurrent() {
        // It has a list of elements in an array, and it makes a new copy of underlying array
        // every time you mutate the collection, so writes are slow and iterators are fast and consistent.
        var copyOnWriteArraySet = new CopyOnWriteArraySet<Integer>();
        // Iy offers performant writes with inconsistent batch operations (addAll, removeAll) and iterators.
        var concurrentSkipListSet = new ConcurrentSkipListSet<Integer>();
        // Like ConcurrentSkipListSet it has inconsistent bach operations.
        var setFromMap = Collections.newSetFromMap(new ConcurrentHashMap<Integer, Boolean>());
        // It uses synchronized method calls to make a set threadsafe. This would be a low-performing version.
        var synchronizedSet = Collections.synchronizedSet(new HashSet<Integer>());
        // SortedSet vs NavigableSet - NavigableSet extends SortedSet and add extra navigable methods e.g.
        // descendingSet(), descendingIterator(), ceiling(), higher(), tailSet(), poolFirst() etc.

        copyOnWriteArraySet.add(5);
        copyOnWriteArraySet.add(1);
        copyOnWriteArraySet.add(3);

        concurrentSkipListSet.add(5);
        concurrentSkipListSet.add(1);
        concurrentSkipListSet.add(3);

        setFromMap.add(5);
        setFromMap.add(1);
        setFromMap.add(3);

        synchronizedSet.add(5);
        synchronizedSet.add(1);
        synchronizedSet.add(3);


        LOGGER.info("Concurrent set (CopyOnWriteArraySet): {}", copyOnWriteArraySet);
        LOGGER.info("Concurrent set (ConcurrentSkipListSet): {}", concurrentSkipListSet);
        LOGGER.info("Concurrent set (Collections.newSetFromMap): {}", setFromMap);
        LOGGER.info("Concurrent set (Collections.synchronizedSet*): {}", synchronizedSet);
        LOGGER.info("*If you need exclusive write access, then you should synchronize.");
    }

    private static void mutableSet() {
        var hashSet = new HashSet<String>();
        var linkedHashSet = new LinkedHashSet<String>();
        var treeSet = new TreeSet<String>();

        hashSet.add("B");
        hashSet.add("C");
        linkedHashSet.add("B");
        linkedHashSet.add("C");
        treeSet.add("B");
        treeSet.add("C");

        LOGGER.info("""
                HashSet: {}
                LinkedHashSet: {}
                TreeSet: {}
                """,
                hashSet,
                linkedHashSet,
                treeSet
        );
    }

    private static void immutableSet() {
        var set = Set.of(1, 3, 5, 2, 4);
        var hashSet = Collections.unmodifiableSet(new HashSet<>(List.of(1, 3, 4, 2)));


        LOGGER.info("Immutable set (Set.of): {}", set);
        LOGGER.info("Immutable hashSet (Collections.unmodifiableSet): {}", hashSet);
    }
}
