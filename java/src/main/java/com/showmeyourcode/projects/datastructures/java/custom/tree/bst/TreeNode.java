package com.showmeyourcode.projects.datastructures.java.custom.tree.bst;

import org.jetbrains.annotations.NotNull;

import java.util.*;

abstract class TreeNode<T extends Comparable<T>> implements Comparable<TreeNode<T>> {
    T value;
    // For multiset semantics (to keep track of how many times a key is present) it is sufficient to store a counter at each node.
    int counter;

    public TreeNode(T value) {
        this.value = value;
        counter = 1;
    }

    // Fields in Java are not overridden but hidden if redeclared in a subclass.
    // Accessing fields depends on the compile-time reference type.
    // To achieve polymorphic behavior, use methods (getters/setters) instead of directly exposing fields.
    abstract TreeNode<T> getLeft();

    abstract TreeNode<T> getRight();

    abstract void insert(T element);

    abstract TreeNode<T> delete(T element);

    public boolean find(T element) {
        if (value.compareTo(element) == 0) {
            return true;
        } else if (value.compareTo(element) > 0) {
            if (getLeft() != null) {
                return getLeft().find(element);
            } else {
                return false;
            }
        } else {
            if (getRight() != null) {
                return getRight().find(element);
            } else {
                return false;
            }
        }
    }

    // =======================
    // standard operations
    // =======================

    public T findMin() {
        if (getLeft() == null) {
            return value;
        } else {
            return getLeft().findMin();
        }
    }

    public T findMax() {
        if (getRight() == null) {
            return value;
        } else {
            return getRight().findMax();
        }
    }

    // =======================
    // other operations
    // =======================

    public int countNodes() {
        return counter + (getLeft() != null ? getLeft().countNodes() : 0) + (getRight() != null ? getRight().countNodes() : 0);
    }

    /**
     * A node with two empty subtrees is called a leaf.
     *
     * @return tree's leaf nodes
     */
    public int countLeafNodes() {
        if (getLeft() == null && getRight() == null) {
            return 1;
        } else {
            return (getLeft() != null ? getLeft().countLeafNodes() : 0) + (getRight() != null ? getRight().countLeafNodes() : 0);
        }
    }

    /**
     * The height of a node in a binary tree is the largest number of edges in a path from a leaf node to a target node.
     *
     * @return tree's height
     */
    public int height() {
        if (getLeft() == null && getRight() == null) {
            return 0;
        }
        return 1 + Math.max(getLeft() != null ? getLeft().height() : 0, getRight() != null ? getRight().height() : 0);
    }

    void increaseCounter(){
        ++counter;
    }

    void decreaseCounter(){
        --counter;
    }

    boolean isSingleElementNode(){
        return counter == 1;
    }

    // =======================
    // traversal
    // =======================

    public List<T> bfs() {
        var result = new ArrayList<T>();
        Queue<TreeNode<T>> nodesToVisit = new LinkedList<>();
        nodesToVisit.add(this);

        while (!nodesToVisit.isEmpty()) {
            var currNode = nodesToVisit.poll();

            for(int i=0;i< currNode.counter;i++){
                result.add(currNode.value);
            }

            if (currNode.getLeft() != null) {
                nodesToVisit.add(currNode.getLeft());
            }
            if (currNode.getRight() != null) {
                nodesToVisit.add(currNode.getRight());
            }
        }

        return result;
    }

    public List<T> dfsPreorder() {
        var result = new ArrayList<T>();

        dfsPreorder(result, this);

        return result;
    }

    private void dfsPreorder(List<T> resultOrder, TreeNode<T> nodeToVisit) {
        resultOrder.add(nodeToVisit.value);
        if (nodeToVisit.getLeft() != null) {
            dfsPreorder(resultOrder, nodeToVisit.getLeft());
        }
        if (nodeToVisit.getRight() != null) {
            dfsPreorder(resultOrder, nodeToVisit.getRight());
        }
    }

    public List<T> dfsInorder() {
        var result = new ArrayList<T>();
        if (getLeft() != null) {
            result.addAll(getLeft().dfsInorder());
        }
        result.add(value);
        if (getRight() != null) {
            result.addAll(getRight().dfsInorder());
        }
        return result;
    }

    public List<T> dfsPostorder() {
        var result = new ArrayList<T>();
        if (getLeft() != null) {
            result.addAll(getLeft().dfsPostorder());
        }
        if (getRight() != null) {
            result.addAll(getRight().dfsPostorder());
        }
        result.add(value);
        return result;
    }

    @Override
    public int compareTo(@NotNull TreeNode<T> o) {
        return value.compareTo(o.value);
    }
}
