package com.showmeyourcode.projects.datastructures.java.custom.tree.bst;

import java.util.Objects;

/**
 * Binary Search Tree (BST) implementation with duplicates where a node contains an additional counter.
 * <p>
 * See package-info.java for more details.
 */
class CounterTreeNode<T extends Comparable<T>> extends TreeNode<T> {
    CounterTreeNode<T> left;
    CounterTreeNode<T> right;

    public CounterTreeNode(T value) {
        super(value);
    }

    @Override
    public CounterTreeNode<T> getLeft() {
        return left;
    }

    @Override
    public CounterTreeNode<T> getRight() {
        return right;
    }

    @Override
    public void insert(T element) {
        var newNode = new CounterTreeNode<>(element);
        var tmp = this;
        int compareResult;
        while (true) {
            compareResult = tmp.compareTo(newNode);
            if (compareResult == 0) {
                tmp.increaseCounter();
                break;
            } else if (compareResult < 0) {
                if (tmp.right == null) {
                    tmp.right = new CounterTreeNode<>(element);
                    break;
                } else {
                    tmp = tmp.right;
                }
            } else {
                if (tmp.left == null) {
                    tmp.left = new CounterTreeNode<>(element);
                    break;
                } else {
                    tmp = tmp.left;
                }
            }
        }
    }

    @Override
    public CounterTreeNode<T> delete(T element) {
        return deleteRecursively(this, element);
    }

    private CounterTreeNode<T> deleteRecursively(CounterTreeNode<T> current, T element) {
        if (current == null) {
            return null;
        }

        int compareResult = current.value.compareTo(element);

        if (compareResult < 0) {
            current.right = deleteRecursively(current.right, element);
        } else if (compareResult > 0) {
            current.left = deleteRecursively(current.left, element);
        } else {
            if (current.counter > 1) {
                current.decreaseCounter();
                return current;
            }

            // Handle node deletion for the three cases:
            // 1. No children (leaf node)
            if (current.left == null && current.right == null) {
                return null;
            }

            // 2. One child
            if (current.left == null) {
                return current.right;
            } else if (current.right == null) {
                return current.left;
            }

            // 3. Two children: Replace with the smallest value in the right subtree
            CounterTreeNode<T> smallestNode = findMin(current.right);
            current.value = smallestNode.value;
            current.counter = smallestNode.counter; // Copy counter from successor
            smallestNode.counter = 1; // Reset successor counter to ensure correct deletion
            current.right = deleteRecursively(current.right, current.value); // Delete successor
        }

        return current;
    }

    private CounterTreeNode<T> findMin(CounterTreeNode<T> node) {
        while (node.left != null) {
            node = node.left;
        }
        return node;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        var node = (CounterTreeNode<T>) o;
        return value == node.value && Objects.equals(node.left, left) && Objects.equals(node.right, right);
    }

    @Override
    public int hashCode() {
        return Objects.hash(left, right, value);
    }
}
