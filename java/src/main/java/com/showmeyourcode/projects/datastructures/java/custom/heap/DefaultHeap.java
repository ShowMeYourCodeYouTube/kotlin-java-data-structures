package com.showmeyourcode.projects.datastructures.java.custom.heap;

import java.util.*;

public class DefaultHeap implements Heap {

    static String EMPTY_HEAP_ERROR = "Heap is empty";

    static Heap minHeap() {
        return new DefaultHeap(MIN_HEAP_COMPARATOR);
    }

    static Heap maxHeap() {
        return new DefaultHeap(MAX_HEAP_COMPARATOR);
    }

    private static final Comparator<Integer> MIN_HEAP_COMPARATOR = Comparator.reverseOrder();
    private static final Comparator<Integer> MAX_HEAP_COMPARATOR = Comparator.naturalOrder();
    private static final int INITIAL_CAPACITY = 10;

    private final Comparator<Integer> comparator;
    private int[] arr = new int[INITIAL_CAPACITY];
    private int size = 0;

    private DefaultHeap(Comparator<Integer> comparator) {
        this.comparator = comparator;
    }

    @Override
    public void insert(int e) {
        expandHeapSize();

        var idxToInsert = size;
        arr[idxToInsert] = e;
        size++;

        heapifyUp(idxToInsert);
    }

    @Override
    public int pop() {
        if (size == 0) {
            throw new NoSuchElementException(EMPTY_HEAP_ERROR);
        }
        var result = arr[0];
        arr[0] = arr[--size];

        if (size != 0) {
            heapifyDown(0);
        }

        return result;
    }

    @Override
    public int peek() {
        if (size == 0) {
            throw new NoSuchElementException(EMPTY_HEAP_ERROR);
        }

        return arr[0];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public List<Integer> toList() {
        return Arrays.stream(Arrays.copyOf(arr, size)).boxed().toList();
    }

    private void swap(int parentIdx, int idx) {
        var tmp = arr[parentIdx];
        arr[parentIdx] = arr[idx];
        arr[idx] = tmp;
    }

    private void heapifyDown(int idx) {
        var currentIdx = idx;
        while (true) {
            var childIdx1 = currentIdx * 2 + 1;
            var childIdx2 = currentIdx * 2 + 2;
            var rootIndex = currentIdx;

            if (childIdx1 < size && comparator.compare(arr[rootIndex], arr[childIdx1]) < 0) {
                rootIndex = childIdx1;
            }

            if (childIdx2 < size && comparator.compare(arr[rootIndex], arr[childIdx2]) < 0) {
                rootIndex = childIdx2;
            }

            if (rootIndex != currentIdx) {
                swap(rootIndex, currentIdx);
                currentIdx = rootIndex;
            } else {
                break;
            }
        }
    }

    private void heapifyUp(int idx) {
        while (idx != 0) {
            var parentIdx = (idx - 1) / 2;
            var compareResult = comparator.compare(arr[parentIdx], arr[idx]);
            if (compareResult < 0) {
                swap(parentIdx, idx);
                idx = parentIdx;
            } else {
                break;
            }
        }
    }

    private void expandHeapSize() {
        if (arr.length <= size) {
            var copy = new int[arr.length * 2];
            System.arraycopy(arr, 0, copy, 0, arr.length);
            arr = copy;
        }
    }
}
