package com.showmeyourcode.projects.datastructures.java.custom.tree.rb;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

enum Color {
    RED,
    BLACK
}

class RedBlackTreeNode<T extends Comparable<T>> {
    T value;
    Color color;
    RedBlackTreeNode<T> left;
    RedBlackTreeNode<T> right;
    RedBlackTreeNode<T> parent;

    RedBlackTreeNode(T value) {
        this(value, Color.RED, null, null, null);
    }

    RedBlackTreeNode(T value, Color color, RedBlackTreeNode<T> left, RedBlackTreeNode<T> right, RedBlackTreeNode<T> parent) {
        this.value = value;
        this.color = color;
        this.left = left;
        this.right = right;
        this.parent = parent;
    }
}

class RedBlackTree<T extends Comparable<T>> {
    private RedBlackTreeNode<T> root;

    public void insert(T value) {
        insertAndFixup(value);
    }

    public List<T> inOrderTraversal() {
        List<T> result = new ArrayList<>();
        inOrderTraversalRecursive(root, result);
        return result;
    }

    private void inOrderTraversalRecursive(RedBlackTreeNode<T> node, List<T> result) {
        if (node != null) {
            inOrderTraversalRecursive(node.left, result);
            result.add(node.value);
            inOrderTraversalRecursive(node.right, result);
        }
    }

    public List<T> preOrderTraversal() {
        List<T> result = new ArrayList<>();
        preOrderTraversalRecursive(root, result);
        return result;
    }

    private void preOrderTraversalRecursive(RedBlackTreeNode<T> node, List<T> result) {
        if (node != null) {
            result.add(node.value);
            preOrderTraversalRecursive(node.left, result);
            preOrderTraversalRecursive(node.right, result);
        }
    }

    public List<T> postOrderTraversal() {
        List<T> result = new ArrayList<>();
        postOrderTraversalRecursive(root, result);
        return result;
    }

    private void postOrderTraversalRecursive(RedBlackTreeNode<T> node, List<T> result) {
        if (node != null) {
            postOrderTraversalRecursive(node.left, result);
            postOrderTraversalRecursive(node.right, result);
            result.add(node.value);
        }
    }

    public List<T> levelOrderTraversal() {
        List<T> result = new ArrayList<>();
        Queue<RedBlackTreeNode<T>> queue = new LinkedList<>();

        if (root != null) {
            queue.add(root);
        }

        while (!queue.isEmpty()) {
            RedBlackTreeNode<T> current = queue.poll();
            result.add(current.value);

            if (current.left != null) {
                queue.add(current.left);
            }
            if (current.right != null) {
                queue.add(current.right);
            }
        }

        return result;
    }

    private void leftRotate(RedBlackTreeNode<T> x) {
        RedBlackTreeNode<T> y = x.right;
        if (y == null) return;

        x.right = y.left;
        if (y.left != null) y.left.parent = x;

        y.parent = x.parent;
        if (x.parent == null) {
            root = y;
        } else if (x == x.parent.left) {
            x.parent.left = y;
        } else {
            x.parent.right = y;
        }

        y.left = x;
        x.parent = y;
    }

    private void rightRotate(RedBlackTreeNode<T> y) {
        RedBlackTreeNode<T> x = y.left;
        if (x == null) return;

        y.left = x.right;
        if (x.right != null) x.right.parent = y;

        x.parent = y.parent;
        if (y.parent == null) {
            root = x;
        } else if (y == y.parent.left) {
            y.parent.left = x;
        } else {
            y.parent.right = x;
        }

        x.right = y;
        y.parent = x;
    }

    private void insertFixup(RedBlackTreeNode<T> z) {
        while (z.parent != null && z.parent.color == Color.RED) {
            if (z.parent == z.parent.parent.left) {
                RedBlackTreeNode<T> y = z.parent.parent.right;
                if (y != null && y.color == Color.RED) {
                    z.parent.color = Color.BLACK;
                    y.color = Color.BLACK;
                    z.parent.parent.color = Color.RED;
                    z = z.parent.parent;
                } else {
                    if (z == z.parent.right) {
                        z = z.parent;
                        leftRotate(z);
                    }
                    z.parent.color = Color.BLACK;
                    z.parent.parent.color = Color.RED;
                    rightRotate(z.parent.parent);
                }
            } else {
                RedBlackTreeNode<T> y = z.parent.parent.left;
                if (y != null && y.color == Color.RED) {
                    z.parent.color = Color.BLACK;
                    y.color = Color.BLACK;
                    z.parent.parent.color = Color.RED;
                    z = z.parent.parent;
                } else {
                    if (z == z.parent.left) {
                        z = z.parent;
                        rightRotate(z);
                    }
                    z.parent.color = Color.BLACK;
                    z.parent.parent.color = Color.RED;
                    leftRotate(z.parent.parent);
                }
            }
        }
        root.color = Color.BLACK;
    }

    private void insertAndFixup(T value) {
        RedBlackTreeNode<T> z = new RedBlackTreeNode<>(value);
        RedBlackTreeNode<T> y = null;
        RedBlackTreeNode<T> x = root;

        while (x != null) {
            y = x;
            if (z.value.compareTo(x.value) < 0) {
                x = x.left;
            } else {
                x = x.right;
            }
        }

        z.parent = y;
        if (y == null) {
            root = z;
        } else if (z.value.compareTo(y.value) < 0) {
            y.left = z;
        } else {
            y.right = z;
        }

        insertFixup(z);
    }

    public void delete(T value) {
        throw new UnsupportedOperationException("Delete operation is not implemented yet.");
    }
}
