package com.showmeyourcode.projects.datastructures.java.custom.heap;

import java.util.List;

public interface Heap {
    int pop();

    int peek();

    int size();

    void insert(int e);

    List<Integer> toList();
}
