package com.showmeyourcode.projects.datastructures.java.exercise.stacks;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Algorithm Outline for Balanced Parentheses Check:
 * <br>
 * 1. Initialize an empty stack.
 * 2. Iterate through each character in the input expression:
 * - If the character is '(', push it onto the stack.
 * - If the character is ')':
 * - Check if the stack is empty. If true, return false (unbalanced).
 * - Pop from the stack. If the popped character is not '(', return false (unbalanced).
 * 3. After iterating through all characters:
 * - If the stack is empty, return true (balanced).
 * - Otherwise, return false (unbalanced) because there are unmatched '('.
 * <br>
 * Complexity Analysis:
 * - Time Complexity: O(n), where n is the length of the input expression. Each character is processed once.
 * - Space Complexity: O(n), due to the stack storing opening parentheses.
 * <br>
 * Justification:
 * - A stack-based approach is ideal for this problem because it allows us to efficiently track and verify nested parentheses in linear time. It ensures that each opening parenthesis has a corresponding closing parenthesis in the correct order, leveraging the Last In, First Out (LIFO) property of stacks.
 */
public class BalancedParenthesesCheckExercise {

    private BalancedParenthesesCheckExercise() {
    }

    static boolean isBalanced(char[] input) {
        Deque<Character> stack = new ArrayDeque<>();

        for (char c : input) {
            if (c == '(') {
                stack.push(c);
            } else if (c == ')') {
                if (stack.isEmpty()) {
                    return false; // More closing parentheses than opening
                }
                char top = stack.pop();
                if (top != '(') {
                    return false; // Mismatched parentheses
                }
            }
        }

        return stack.isEmpty(); // Stack should be empty for balanced expression
    }
}
