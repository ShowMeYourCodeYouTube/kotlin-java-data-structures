package com.showmeyourcode.projects.datastructures.java.custom.array;

/**
 * Reference: https://neetcode.io/problems/dynamicArray
 * <p>
 * Design a Dynamic Array (aka a resizable array) class, such as an ArrayList in Java or a vector in C++.
 * <p>
 * Your DynamicArray class should support the following operations:
 * - DynamicArray(int capacity) will initialize an empty array with a capacity of capacity, where capacity > 0.
 * - int get(int i) will return the element at index i. Assume that index i is valid.
 * - void set(int i, int n) will set the element at index i to n. Assume that index i is valid.
 * - void pushback(int n) will push the element n to the end of the array.
 * - int popback() will pop and return the element at the end of the array. Assume that the array is non-empty.
 * - void resize() will double the capacity of the array.
 * - int getSize() will return the number of elements in the array.
 * - int getCapacity() will return the capacity of the array.
 * If we call void pushback(int n) but the array is full, we should resize the array first.
 */
class DynamicArray {

    private int[] arr;
    private int indexToInsert = 0;

    public DynamicArray(int capacity) {
        arr = new int[capacity];
    }

    public int get(int i) {
        if (arr.length > i) {
            return arr[i];
        } else {
            return -1;
        }
    }

    public void set(int i, int n) {
        if (arr.length > i) {
            arr[i] = n;
        }
    }

    public void pushback(int n) {
        if (arr.length > indexToInsert) {
            arr[indexToInsert++] = n;
        } else {
            resize();
            arr[indexToInsert++] = n;
        }
    }

    public int popback() {
        if (indexToInsert > 0) {
            return arr[--indexToInsert];
        } else {
            return -1;
        }
    }

    private void resize() {
        int[] newArray = new int[arr.length * 2];
        System.arraycopy(arr, 0, newArray, 0, arr.length);
        arr = newArray;
    }

    public int getSize() {
        return indexToInsert;
    }

    public int getCapacity() {
        return arr.length;
    }
}