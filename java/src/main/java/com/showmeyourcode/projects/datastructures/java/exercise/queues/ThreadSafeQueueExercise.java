package com.showmeyourcode.projects.datastructures.java.exercise.queues;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;

/**
 * Thread-Safe Ticket Queue with Supplier-Driven Descriptions
 * <br>
 * Implement a thread-safe ticket queue in Java that allows for dynamic ticket descriptions using a Supplier<String>.
 * This exercise focuses on utilizing concurrent data structures (ConcurrentLinkedQueue and AtomicInteger) to ensure thread safety while enabling flexible ticket description generation through supplier functions.
 */
public class ThreadSafeQueueExercise {

    private final Queue<Ticket> queue = new ConcurrentLinkedQueue<>();
    private final AtomicInteger ticketCounter = new AtomicInteger(0);

    // Method to add a ticket to the end of the queue using a description supplier
    public void enqueueTicket(Supplier<String> descriptionSupplier) {
        int ticketNumber = ticketCounter.incrementAndGet();
        String description = descriptionSupplier.get();
        Ticket ticket = new Ticket(ticketNumber, description);
        queue.add(ticket);
    }

    // Method to process and dequeue the next ticket from the queue
    public Ticket dequeueTicket() {
        return queue.poll();
    }

    // Method to return the number of tickets in the queue
    public int getTicketQueueSize() {
        return queue.size();
    }

    // Record representing a Ticket with a unique identifier and description
    record Ticket(int ticketNumber, String description) {
    }
}
