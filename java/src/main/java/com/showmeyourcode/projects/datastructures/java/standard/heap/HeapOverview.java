package com.showmeyourcode.projects.datastructures.java.standard.heap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * References:
 * - <a href="https://stackoverflow.com/questions/1098277/java-implementation-for-min-max-heap">StackOverflow Heap implementation in Java</a>
 * - <a href="https://codegym.cc/groups/posts/min-heap-in-java">Min Heap in Java</a>
 */
public class HeapOverview {

    private static final Logger LOGGER = LoggerFactory.getLogger(HeapOverview.class);

    private HeapOverview() {
    }

    public static void main(String[] args) {
        LOGGER.info(
                """
                Min-heap and max-heap are both priority queues, it depends on how their order of priority is defined. The order of items is based on value.
                """
        );
        PriorityQueue<Integer> minHeap = new PriorityQueue<>();
        PriorityQueue<Integer> maxHeap = new PriorityQueue<>(Comparator.reverseOrder());

        processHeap(minHeap);
        processHeap(maxHeap);
    }

    private static void processHeap(PriorityQueue<Integer> heap) {
        heap.add(3);
        heap.add(13);
        heap.add(7);
        heap.add(16);
        heap.add(21);
        heap.add(12);
        heap.add(9);

        LOGGER.info("heap.peek() = {}", heap.peek());
        // using "remove" method to remove specified element
        heap.remove(7);
        LOGGER.info("heap.remove(7)");

        // Check if an element is present using contains()
        boolean elementFound = heap.contains(11);
        LOGGER.info("heap.contains(11) = {}", elementFound);
        elementFound = heap.contains(16);
        LOGGER.info("heap.contains(16) = {}", elementFound);
    }
}
