/**
 * Binary Search Tree (BST) implementation.
 * 1. Define a node class.
 * 2. Implement 'insert' method.
 * 3. Implement search function returns boolean.
 * 4. Implement find min/max values.
 * 5. Implement removing a node.
 * 6. Implement calculating number of nodes and leaf nodes.
 * A node with two empty subtrees is called a leaf.
 * 7. Implement calculating tree height.
 * The height of a node in a binary tree is the largest number of edges in a path from a leaf node to a target node.
 * 8. Implement Breadth-first search (BFS) traversal.
 * Breadth-first search is a tree traversal algorithm that explores nodes level by level.
 * 9. Implement Depth-first search (DFS)  preorder, inorder, postorder traversals.
 * Depth-first search is another tree traversal algorithm that goes deep into a tree exploring for nodes branch by branch.
 *
 * <br>
 * <br>
 * More about traversing: <a href="https://medium.com/@Roshan-jha/a-comprehensive-guide-to-binary-tree-traversal-in-java-74c86ee23725">A Comprehensive Guide to Binary Tree Traversal in Java</a>
 * <br>
 * <br>
 * An implementation allowing duplicates.
 * <br>
 * How about duplicates?
 * Let's assume we have a node with the same value as root.
 * Answer: one common approach is to only allow a single node for each value (i.e. key) in the BST.
 * For multiset semantics (to keep track of how many times a key is present) it is sufficient to store a counter at each node. This approach is simple and quite efficient.
 * It might happen that we have node with the same value so the rest should go to the right as this is our rule of inserting
 *
 * <br>
 * <br>
 * Reference: <a href="https://stackoverflow.com/questions/68207641/which-side-do-we-place-a-node-if-it-is-equal-to-the-parent-in-a-binary-search-tr">Binary Search and duplicates</a>
 */
package com.showmeyourcode.projects.datastructures.java.custom.tree.bst;