package com.showmeyourcode.projects.datastructures.java.exercise.maps;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TemperatureTrackerExerciseTest {

    @ParameterizedTest
    @CsvSource({
            "NewYork;2024-06-20T14:00:00;85, NewYork, '2024-06-20T00:00:00', '2024-06-21T23:59:59', 85, 85.0",
            "NewYork;2024-06-21T14:00:00;88, NewYork, '2024-06-20T00:00:00', '2024-06-21T23:59:59', 88, 88.0",
            "NewYork;2024-06-21T14:00:00;90, NewYork, '2024-06-20T00:00:00', '2024-06-21T23:59:59', 90, 90.00",
            "LosAngeles;2024-06-20T14:00:00;75, LosAngeles, '2024-06-20T00:00:00', '2024-06-21T23:59:59', 75, 75.0",
            "LosAngeles;2024-06-21T14:00:00;77, LosAngeles, '2024-06-20T00:00:00', '2024-06-21T23:59:59', 77, 77.0"
    })
    void testAddAndQueryReadings(String readings, String city, String start, String end, int expectedMax, double expectedAvg) {
        TemperatureTracker tracker = new TemperatureTrackerExercise();
        for (String reading : readings.split(",")) {
            tracker.addReading(reading);
        }

        assertEquals(expectedMax, tracker.getMaxTemperatureInRange(city, start, end));
        assertEquals(expectedAvg, tracker.getAverageTemperatureInRange(city, start, end), 0.01);
    }

    @ParameterizedTest
    @CsvSource({
            "NewYork;2024-06-20T14:00:00;85, NewYork, '2024-06-21T00:00:00', '2024-06-21T23:59:59'",
            "NewYork;2024-06-21T14:00:00;88, NewYork, '2024-06-22T00:00:00', '2024-06-22T23:59:59'"
    })
    void testNoReadingsInRange(String readings, String city, String start, String end) {
        TemperatureTracker tracker = new TemperatureTrackerExercise();
        tracker.addReading(readings);

        assertThrows(IllegalArgumentException.class, () -> tracker.getMaxTemperatureInRange(city, start, end));
        assertThrows(IllegalArgumentException.class, () -> tracker.getAverageTemperatureInRange(city, start, end));
    }

    @ParameterizedTest
    @CsvSource({
            "NewYork;2024-06-20T14:00:00;85",
            "LosAngeles;2024-06-20T14:00:00;75"
    })
    void testInvalidCity(String readings) {
        TemperatureTracker tracker = new TemperatureTrackerExercise();
        tracker.addReading(readings);

        assertThrows(IllegalArgumentException.class, () -> tracker.getMaxTemperatureInRange("InvalidCity", "2024:06:20T00:00:00", "2024:06:21T23:59:59"));
        assertThrows(IllegalArgumentException.class, () -> tracker.getAverageTemperatureInRange("InvalidCity", "2024:06:20T00:00:00", "2024:06:21T23:59:59"));
    }
}
