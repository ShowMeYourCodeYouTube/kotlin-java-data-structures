package com.showmeyourcode.projects.datastructures.java.custom.graph;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DijkstraExerciseTest {

    @Test
    void shouldReturnCorrectShortestPathsForSimpleGraph() {
        List<List<Integer>> edges = Arrays.asList(
                Arrays.asList(0, 1, 10),
                Arrays.asList(1, 2, 5),
                Arrays.asList(2, 3, 2),
                Arrays.asList(3, 4, 1)
        );

        // Apply Dijkstra starting from vertex 0
        Map<Integer, Integer> result = DijkstraExercise.shortestPath(5, edges, 0);

        // Validate the distances from the source vertex (0)
        assertEquals(0, result.get(0), "Distance to vertex 0 should be 0");
        assertEquals(10, result.get(1), "Distance to vertex 1 should be 10");
        assertEquals(15, result.get(2), "Distance to vertex 2 should be 15");
        assertEquals(17, result.get(3), "Distance to vertex 3 should be 17");
        assertEquals(18, result.get(4), "Distance to vertex 4 should be 18");
    }

    @Test
    void shouldReturnUnreachableVerticesAsMinusOne() {
        // Define a graph with 5 vertices, but no edges between them
        List<List<Integer>> edges = new ArrayList<>();

        // Apply Dijkstra starting from vertex 0
        Map<Integer, Integer> result = DijkstraExercise.shortestPath(5, edges, 0);

        // Since there are no edges, only vertex 0 is reachable (distance = 0)
        assertEquals(0, result.get(0), "Distance to vertex 0 should be 0");
        for (int i = 1; i < 5; i++) {
            assertEquals(-1, result.get(i), "Unreachable vertices should have a distance of -1");
        }
    }

    @Test
    void shouldReturnCorrectShortestPathsWhenMultiplePathsExist() {
        // Define a graph where multiple paths exist between vertices
        List<List<Integer>> edges = Arrays.asList(
                Arrays.asList(0, 1, 10),
                Arrays.asList(0, 2, 5),
                Arrays.asList(1, 2, 2),
                Arrays.asList(2, 3, 1),
                Arrays.asList(3, 4, 3)
        );

        // Apply Dijkstra starting from vertex 0
        Map<Integer, Integer> result = DijkstraExercise.shortestPath(5, edges, 0);

        // Shortest path distances from vertex 0
        assertEquals(0, result.get(0), "Distance to vertex 0 should be 0");
        assertEquals(10, result.get(1), "Distance to vertex 1 should be 1");
        assertEquals(5, result.get(2), "Distance to vertex 2 should be 5");
        assertEquals(6, result.get(3), "Distance to vertex 3 should be 6");
        assertEquals(9, result.get(4), "Distance to vertex 4 should be 9");
    }

    @Test
    void shouldHandleDisconnectedGraphWithUnreachableVertex() {
        // Define a graph with some vertices disconnected
        List<List<Integer>> edges = Arrays.asList(
                Arrays.asList(0, 1, 5),
                Arrays.asList(1, 2, 2),
                Arrays.asList(2, 3, 3)
        );

        // Apply Dijkstra starting from vertex 0
        Map<Integer, Integer> result = DijkstraExercise.shortestPath(5, edges, 0);

        // Check distances from vertex 0
        assertEquals(0, result.get(0), "Distance to vertex 0 should be 0");
        assertEquals(5, result.get(1), "Distance to vertex 1 should be 5");
        assertEquals(7, result.get(2), "Distance to vertex 2 should be 7");
        assertEquals(10, result.get(3), "Distance to vertex 3 should be 10");

        // Vertex 4 is unreachable
        assertEquals(-1, result.get(4), "Vertex 4 should be unreachable");
    }

    @Test
    void shouldHandleSingleVertexGraph() {
        // Test a graph with a single vertex
        List<List<Integer>> edges = new ArrayList<>();

        // Apply Dijkstra starting from vertex 0
        Map<Integer, Integer> result = DijkstraExercise.shortestPath(1, edges, 0);

        // The only vertex is the source, so distance should be 0
        assertEquals(0, result.get(0), "The only vertex should have a distance of 0 to itself");
    }

    @Test
    void shouldReturnMinusOneForUnreachableVerticesInSparseGraph() {
        // Test a sparse graph with a few edges but some vertices disconnected
        List<List<Integer>> edges = Arrays.asList(
                Arrays.asList(0, 1, 3),
                Arrays.asList(1, 2, 2),
                Arrays.asList(2, 3, 4)
        );

        // Apply Dijkstra starting from vertex 0
        Map<Integer, Integer> result = DijkstraExercise.shortestPath(5, edges, 0);

        // Check that vertices 4 is unreachable
        assertEquals(-1, result.get(4), "Vertex 4 should be unreachable");
    }

    @Test
    void shouldReturnShortestPathsForComplexGraph() {
        // Define a complex graph with various edge weights
        List<List<Integer>> edges = Arrays.asList(
                Arrays.asList(0, 1, 2),
                Arrays.asList(0, 2, 1),
                Arrays.asList(1, 3, 7),
                Arrays.asList(2, 3, 3),
                Arrays.asList(3, 4, 1)
        );

        // Apply Dijkstra starting from vertex 0
        Map<Integer, Integer> result = DijkstraExercise.shortestPath(5, edges, 0);

        // Check shortest path distances from vertex 0
        assertEquals(0, result.get(0), "Distance to vertex 0 should be 0");
        assertEquals(2, result.get(1), "Distance to vertex 1 should be 2");
        assertEquals(1, result.get(2), "Distance to vertex 2 should be 1");
        assertEquals(4, result.get(3), "Distance to vertex 3 should be 4");
        assertEquals(5, result.get(4), "Distance to vertex 4 should be 5");
    }

    @Test
    void shouldReturnMinusOneForUnreachableVertices() {
        // Test graph where some vertices are unreachable
        List<List<Integer>> edges = Arrays.asList(
                Arrays.asList(0, 1, 4),
                Arrays.asList(1, 2, 1)
        );

        // Apply Dijkstra starting from vertex 0
        Map<Integer, Integer> result = DijkstraExercise.shortestPath(5, edges, 0);

        // Check that vertices 3 and 4 are unreachable
        assertEquals(-1, result.get(3), "Vertex 3 should be unreachable");
        assertEquals(-1, result.get(4), "Vertex 4 should be unreachable");
    }
}
