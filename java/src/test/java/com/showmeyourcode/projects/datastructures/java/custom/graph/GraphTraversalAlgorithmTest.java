package com.showmeyourcode.projects.datastructures.java.custom.graph;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static com.showmeyourcode.projects.datastructures.java.custom.graph.Common.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GraphTraversalAlgorithmTest {

    static Stream<Arguments> bfs() {
        return Stream.of(
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 2), Arrays.asList(3, 4), Arrays.asList(4), Arrays.asList(5), Arrays.asList(5), Arrays.asList()),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3, 4, 5)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1), Arrays.asList(0, 2), Arrays.asList(1)),
                        GraphType.DIRECTED,
                        1,
                        Arrays.asList(1, 0, 2)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 2), Arrays.asList(0, 3), Arrays.asList(0, 4), Arrays.asList(1), Arrays.asList(2)),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3, 4)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(), Arrays.asList(2, 3), Arrays.asList(3), Arrays.asList(), Arrays.asList(1)),
                        GraphType.DIRECTED,
                        4,
                        Arrays.asList(4, 1, 2, 3)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1), Arrays.asList(2), Arrays.asList(3), Arrays.asList(4), Arrays.asList(5), Arrays.asList()),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3, 4, 5)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 2, 3, 4), Arrays.asList(0), Arrays.asList(0), Arrays.asList(0), Arrays.asList(0)),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3, 4)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1, 2, 3),
                                Arrays.asList(4),
                                Arrays.asList(5),
                                Arrays.asList(6),
                                Arrays.asList(),
                                Arrays.asList(),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3, 4, 5, 6)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1), Arrays.asList(2), Arrays.asList(0, 3), Arrays.asList()),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 3), Arrays.asList(2), Arrays.asList(), Arrays.asList(4), Arrays.asList()),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0, 1, 3, 2, 4)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 4), Arrays.asList(2, 3), Arrays.asList(), Arrays.asList(), Arrays.asList(5), Arrays.asList()),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0, 1, 4, 2, 3, 5)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1, 2),
                                Arrays.asList(3),
                                Arrays.asList(4),
                                Arrays.asList(5),
                                Arrays.asList(5),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3, 4, 5)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1, 2),
                                Arrays.asList(3),
                                Arrays.asList(4),
                                Arrays.asList(5),
                                Arrays.asList(5),
                                Arrays.asList()
                        ),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3, 4, 5)
                )
        );
    }

    @ParameterizedTest
    @MethodSource("bfs")
    void shouldTraverseBfsGraphCorrectly(List<List<Integer>> adjacencyList, GraphType graphType, int startVertex, List<Integer> expected) {
        var graphAdjacencyList = createGraphWithAdjacencyList(adjacencyList, graphType);
        var graphAdjacencyMatrix = createGraphWithAdjacencyMatrix(adjacencyList, graphType);

        assertEquals(expected, graphAdjacencyList.bfs(startVertex));
        assertEquals(expected, graphAdjacencyMatrix.bfs(startVertex));

        // DFS or BFS on weighted graphs is exactly the same as on unweighted graphs,
        // since both algorithms don't use any edge weights.
        var weightedGraphAdjacencyList = createWeightedGraphWithAdjacencyList(addRandomWeights(adjacencyList), graphType);
        var weightedGraphAdjacencyMatrix = createWeightedGraphWithAdjacencyMatrix(addRandomWeights(adjacencyList), graphType);

        assertEquals(expected, weightedGraphAdjacencyList.bfs(startVertex));
        assertEquals(expected, weightedGraphAdjacencyMatrix.bfs(startVertex));
    }

    static Stream<Arguments> dfs() {
        return Stream.of(
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 2), Arrays.asList(3, 4), Arrays.asList(4), Arrays.asList(5), Arrays.asList(5), Arrays.asList()),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 3, 5, 4, 2)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1), Arrays.asList(0, 2), Arrays.asList(1)),
                        GraphType.DIRECTED,
                        1,
                        Arrays.asList(1, 0, 2)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 2), Arrays.asList(0, 3), Arrays.asList(0, 4), Arrays.asList(1), Arrays.asList(2)),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 3, 2, 4)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(), Arrays.asList(2, 3), Arrays.asList(3), Arrays.asList(), Arrays.asList(1)),
                        GraphType.DIRECTED,
                        4,
                        Arrays.asList(4, 1, 2, 3)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1), Arrays.asList(2), Arrays.asList(3), Arrays.asList(4), Arrays.asList(5), Arrays.asList()),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3, 4, 5)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 2, 3, 4), Arrays.asList(0), Arrays.asList(0), Arrays.asList(0), Arrays.asList(0)),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3, 4)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1, 2, 3),
                                Arrays.asList(4),
                                Arrays.asList(5),
                                Arrays.asList(6),
                                Arrays.asList(),
                                Arrays.asList(),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0, 1, 4, 2, 5, 3, 6)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1), Arrays.asList(2), Arrays.asList(0, 3), Arrays.asList()),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 3), Arrays.asList(2), Arrays.asList(), Arrays.asList(4), Arrays.asList()),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3, 4)
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 4), Arrays.asList(2, 3), Arrays.asList(), Arrays.asList(), Arrays.asList(5), Arrays.asList()),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0, 1, 2, 3, 4, 5)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1, 2),
                                Arrays.asList(3),
                                Arrays.asList(4),
                                Arrays.asList(5),
                                Arrays.asList(5),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        0,
                        Arrays.asList(0, 1, 3, 5, 2, 4)
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1, 2),
                                Arrays.asList(3),
                                Arrays.asList(4),
                                Arrays.asList(5),
                                Arrays.asList(5),
                                Arrays.asList()
                        ),
                        GraphType.UNDIRECTED,
                        0,
                        Arrays.asList(0, 1, 3, 5, 4, 2)
                )
        );
    }

    @ParameterizedTest
    @MethodSource("dfs")
    void shouldTraverseDfsGraphCorrectly(List<List<Integer>> adjacencyList, GraphType graphType, int startVertex, List<Integer> expected) {
        var graphAdjacencyList = createGraphWithAdjacencyList(adjacencyList, graphType);
        var graphAdjacencyMatrix = createGraphWithAdjacencyMatrix(adjacencyList, graphType);

        assertEquals(expected, graphAdjacencyList.dfs(startVertex));
        assertEquals(expected, graphAdjacencyMatrix.dfs(startVertex));

        // DFS or BFS on weighted graphs is exactly the same as on unweighted graphs,
        // since both algorithms don't use any edge weights.
        var weightedGraphAdjacencyList = createWeightedGraphWithAdjacencyList(addRandomWeights(adjacencyList), graphType);
        var weightedGraphAdjacencyMatrix = createWeightedGraphWithAdjacencyMatrix(addRandomWeights(adjacencyList), graphType);

        assertEquals(expected, weightedGraphAdjacencyList.dfs(startVertex));
        assertEquals(expected, weightedGraphAdjacencyMatrix.dfs(startVertex));
    }
}
