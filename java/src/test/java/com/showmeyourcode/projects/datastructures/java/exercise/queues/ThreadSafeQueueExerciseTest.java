package com.showmeyourcode.projects.datastructures.java.exercise.queues;


import com.showmeyourcode.projects.datastructures.java.StringToListConverter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class ThreadSafeQueueExerciseTest {

    private ThreadSafeQueueExercise queue;

    @BeforeEach
    void setUp() {
        queue = new ThreadSafeQueueExercise();
    }

    @ParameterizedTest
    @CsvSource({
            "Issue with login,Database connectivity problem,Performance degradation,Feature request,UI bug",
    })
    void enqueueTicket_ShouldAddTicketWithDynamicDescription(@ConvertWith(StringToListConverter.class) List<String> descriptions) {
        for (String s : descriptions) {
            queue.enqueueTicket(() -> s);
        }

        descriptions = descriptions.stream().sorted(Comparator.reverseOrder()).toList();
        for (String s : descriptions) {
            ThreadSafeQueueExercise.Ticket ticket = queue.dequeueTicket();
            assertNotNull(ticket);
            assertEquals(s, ticket.description());
        }
    }

    @Test
    void enqueueTickets_ConcurrentAccess_ShouldHandleMultipleThreads() throws InterruptedException {
        // Create multiple threads to enqueue tickets concurrently
        Thread[] threads = new Thread[5];

        for (int i = 0; i < threads.length; i++) {
            final int ticketNumber = i + 1;
            threads[i] = new Thread(() -> queue.enqueueTicket(() -> "Ticket " + ticketNumber));
            threads[i].start();
        }

        // Wait for all threads to complete
        for (Thread thread : threads) {
            thread.join();
        }

        assertEquals(5, queue.getTicketQueueSize());
        for (int i = 0; i < threads.length; i++) {
            ThreadSafeQueueExercise.Ticket ticket = queue.dequeueTicket();
            assertNotNull(ticket);
        }
    }
}
