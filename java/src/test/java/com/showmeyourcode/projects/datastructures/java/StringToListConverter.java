package com.showmeyourcode.projects.datastructures.java;

import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StringToListConverter extends SimpleArgumentConverter {

    @Override
    protected Object convert(Object source, Class<?> targetType) throws ArgumentConversionException {
        if (source == null || source.equals("")) {
            return new ArrayList<Integer>();
        }
        if (!List.class.isAssignableFrom(targetType)) {
            throw new IllegalArgumentException("Conversion target type is not List.");
        }
        return Arrays.stream(((String) source).split(","))
                .map(String::trim)
                .toList();
    }
}
