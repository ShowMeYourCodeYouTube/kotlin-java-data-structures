package com.showmeyourcode.projects.datastructures.java.exercise.arrays;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MatrixGeneratorExerciseTest {

    private static Stream<Arguments> provideMatricesForTransposeTest() {
        return Stream.of(
                Arguments.of(
                        new int[][]{
                                {1, 2, 3},
                                {4, 5, 6},
                                {7, 8, 9}
                        },
                        new int[][]{
                                {1, 4, 7},
                                {2, 5, 8},
                                {3, 6, 9}
                        }
                ),
                Arguments.of(
                        new int[][]{
                                {1, 2},
                                {3, 4},
                                {5, 6}
                        },
                        new int[][]{
                                {1, 3, 5},
                                {2, 4, 6}
                        }
                ),
                Arguments.of(
                        new int[][]{
                                {1}
                        },
                        new int[][]{
                                {1}
                        }
                ),
                Arguments.of(
                        new int[][]{
                                {1, 2, 3}
                        },
                        new int[][]{
                                {1},
                                {2},
                                {3}
                        }
                ),
                Arguments.of(
                        new int[][]{
                                {1},
                                {2},
                                {3}
                        },
                        new int[][]{
                                {1, 2, 3}
                        }
                )
        );
    }

    @ParameterizedTest
    @CsvSource({
            "2, 3, 1, 5",
            "4, 4, 0, 10",
            "3, 2, -5, 5",
            "10, 1, 100, 200"
    })
    void testCreateMatrix(int rows, int cols, int min, int max) {
        int[][] matrix = MatrixGeneratorExercise.createMatrix(rows, cols, min, max);

        assertEquals(rows, matrix.length, "Number of rows is incorrect");
        assertEquals(cols, matrix[0].length, "Number of columns is incorrect");

        Set<Integer> uniqueElements = new HashSet<>();
        for (int[] row : matrix) {
            for (int element : row) {
                assertTrue(element >= min && element <= max, "Element out of range: " + element);
                uniqueElements.add(element);
            }
        }

        // Check that the generated matrix is not trivially uniform (e.g., all elements are the same)
        assertTrue(uniqueElements.size() > 1, "Matrix elements are not diverse enough");
    }

    @ParameterizedTest
    @MethodSource("provideMatricesForTransposeTest")
    void testTransposeMatrix(int[][] input, int[][] expected) {
        int[][] result = MatrixGeneratorExercise.transposeMatrix(input);
        assertArrayEquals(expected, result);
    }

    @Test
    void testTransposeMatrixThrowsIllegalArgumentExceptionForNullMatrix() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            MatrixGeneratorExercise.transposeMatrix(null);
        });
        assertEquals("Matrix cannot be null or empty", exception.getMessage());
    }

    @Test
    void testTransposeMatrixThrowsIllegalArgumentExceptionForEmptyMatrix() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            MatrixGeneratorExercise.transposeMatrix(new int[0][0]);
        });
        assertEquals("Matrix cannot be null or empty", exception.getMessage());
    }

    @Test
    void testTransposeMatrixThrowsIllegalArgumentExceptionForMatrixWithEmptyRows() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            MatrixGeneratorExercise.transposeMatrix(new int[][]{{}});
        });
        assertEquals("Matrix cannot be null or empty", exception.getMessage());
    }
}
