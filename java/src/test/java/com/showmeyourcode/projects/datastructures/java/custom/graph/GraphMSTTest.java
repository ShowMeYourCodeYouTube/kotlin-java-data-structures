package com.showmeyourcode.projects.datastructures.java.custom.graph;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GraphMSTTest {

    private WeightedGraph graphMinimumSpanningTree;

    @BeforeEach
    void setup() {
        graphMinimumSpanningTree = new WeightedGraph(new WeightedAdjacencyMatrix(5, GraphType.UNDIRECTED));
        graphMinimumSpanningTree.addEdge(0, 1, 2.0);
        graphMinimumSpanningTree.addEdge(0, 3, 1.0);
        graphMinimumSpanningTree.addEdge(1, 2, 3.0);
        graphMinimumSpanningTree.addEdge(1, 3, 3.0);
        graphMinimumSpanningTree.addEdge(2, 4, 2.0);
        graphMinimumSpanningTree.addEdge(3, 4, 1.0);
    }

    @Test
    void shouldFindMstUsingPrimAlgorithm() {
        assertEquals(
                Arrays.asList(
                        Map.entry(0, 3),
                        Map.entry(3, 4),
                        Map.entry(0, 1),
                        Map.entry(4, 2)
                ),
                graphMinimumSpanningTree.primMST()
        );
    }

    @Test
    void shouldFindMstUsingKruskalAlgorithm() {
        assertEquals(
                Arrays.asList(
                        Map.entry(0, 3),
                        Map.entry(3, 4),
                        Map.entry(0, 1),
                        Map.entry(2, 4)
                ),
                graphMinimumSpanningTree.kruskalMST()
        );
    }
}
