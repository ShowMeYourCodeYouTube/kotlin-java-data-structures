package com.showmeyourcode.projects.datastructures.java.custom.graph;

import java.util.List;
import java.util.Map;
import java.util.Random;

class Common {

    private static final Random random = new Random();

    public static Graph createGraphWithAdjacencyList(List<List<Integer>> adjacencyList, GraphType type) {
        var representation = new AdjacencyList(adjacencyList.size(), type);
        return createGraph(adjacencyList, representation);
    }

    public static Graph createGraphWithAdjacencyMatrix(List<List<Integer>> adjacencyList, GraphType type) {
        var representation = new AdjacencyMatrix(adjacencyList.size(), type);
        return createGraph(adjacencyList, representation);
    }

    public static WeightedGraph createWeightedGraphWithAdjacencyList(List<List<Map.Entry<Integer, Double>>> adjacencyList, GraphType type) {
        var representation = new WeightedAdjacencyList(adjacencyList.size(), type);
        return createWeightedGraph(adjacencyList, representation);
    }

    public static WeightedGraph createWeightedGraphWithAdjacencyMatrix(List<List<Map.Entry<Integer, Double>>> adjacencyList, GraphType type) {
        var representation = new WeightedAdjacencyMatrix(adjacencyList.size(), type);
        return createWeightedGraph(adjacencyList, representation);
    }

    private static Graph createGraph(List<List<Integer>> adjacencyList, GraphRepresentation representation) {
        representation.addEdges(adjacencyList);
        return new Graph(representation);
    }

    private static WeightedGraph createWeightedGraph(List<List<Map.Entry<Integer, Double>>> adjacencyList, WeightedGraphRepresentation representation) {
        representation.addWeightedEdges(adjacencyList);
        return new WeightedGraph(representation);
    }

    public static List<List<Map.Entry<Integer, Double>>> addRandomWeights(List<List<Integer>> adjacencyList) {
        return adjacencyList.stream().map(l -> l.stream().map(e -> Map.entry(e, random.nextDouble())).toList()).toList();
    }
}
