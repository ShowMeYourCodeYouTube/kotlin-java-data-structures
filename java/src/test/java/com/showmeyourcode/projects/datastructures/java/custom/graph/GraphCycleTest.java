package com.showmeyourcode.projects.datastructures.java.custom.graph;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static com.showmeyourcode.projects.datastructures.java.custom.graph.Common.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GraphCycleTest {

    static Stream<Arguments> cycles() {
        return Stream.of(
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 2), Arrays.asList(3, 4), Arrays.asList(4), Arrays.asList(5), Arrays.asList(5), Arrays.asList()),
                        GraphType.UNDIRECTED,
                        true
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1), Arrays.asList(0, 2), Arrays.asList(1)),
                        GraphType.DIRECTED,
                        true
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 2), Arrays.asList(0, 3), Arrays.asList(0, 4), Arrays.asList(1), Arrays.asList(2)),
                        GraphType.UNDIRECTED,
                        true
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(), Arrays.asList(2, 3), Arrays.asList(3), Arrays.asList(), Arrays.asList(1)),
                        GraphType.DIRECTED,
                        false
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1), Arrays.asList(2), Arrays.asList(3), Arrays.asList(4), Arrays.asList(5), Arrays.asList()),
                        GraphType.UNDIRECTED,
                        true
                ),
                Arguments.of(
                        Arrays.asList(Arrays.asList(1, 2, 3, 4), Arrays.asList(0), Arrays.asList(0), Arrays.asList(0), Arrays.asList(0)),
                        GraphType.UNDIRECTED,
                        true
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1, 2, 3),
                                Arrays.asList(4),
                                Arrays.asList(5),
                                Arrays.asList(6),
                                Arrays.asList(),
                                Arrays.asList(),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        false
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1), Arrays.asList(2), Arrays.asList(0, 3), Arrays.asList()
                        ),
                        GraphType.UNDIRECTED,
                        true
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1, 3), Arrays.asList(2), Arrays.asList(), Arrays.asList(4), Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        false
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1, 4), Arrays.asList(2, 3), Arrays.asList(), Arrays.asList(), Arrays.asList(5), Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        false
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1, 2),
                                Arrays.asList(3),
                                Arrays.asList(4),
                                Arrays.asList(5),
                                Arrays.asList(5),
                                Arrays.asList()
                        ),
                        GraphType.DIRECTED,
                        false
                ),
                Arguments.of(
                        Arrays.asList(
                                Arrays.asList(1, 2),
                                Arrays.asList(),
                                Arrays.asList()
                        ),
                        GraphType.UNDIRECTED,
                        true
                )
        );
    }

    @ParameterizedTest
    @MethodSource("cycles")
    void shouldCorrectlyDetectCycles(List<List<Integer>> adjacencyList, GraphType graphType, boolean isCycle) {
        var graphAdjacencyList = createGraphWithAdjacencyList(adjacencyList, graphType);
        var graphAdjacencyMatrix = createGraphWithAdjacencyMatrix(adjacencyList, graphType);

        assertEquals(isCycle, graphAdjacencyList.hasCycleDFS());
        assertEquals(isCycle, graphAdjacencyList.hasCycleBFS());
        assertEquals(isCycle, graphAdjacencyMatrix.hasCycleDFS());
        assertEquals(isCycle, graphAdjacencyMatrix.hasCycleBFS());

        // DFS or BFS on weighted graphs is exactly the same as on unweighted graphs,
        // since both algorithms don't use any edge weights.
        var weightedGraphAdjacencyList = createWeightedGraphWithAdjacencyList(addRandomWeights(adjacencyList), graphType);
        var weightedGraphAdjacencyMatrix = createWeightedGraphWithAdjacencyMatrix(addRandomWeights(adjacencyList), graphType);

        assertEquals(isCycle, weightedGraphAdjacencyList.hasCycleDFS());
        assertEquals(isCycle, weightedGraphAdjacencyList.hasCycleBFS());
        assertEquals(isCycle, weightedGraphAdjacencyMatrix.hasCycleDFS());
        assertEquals(isCycle, weightedGraphAdjacencyMatrix.hasCycleBFS());
    }
}
