package com.showmeyourcode.projects.datastructures.java.exercise.heaps;

import com.showmeyourcode.projects.datastructures.java.StringToIntListConverter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MedianFinderExerciseTest {

    @ParameterizedTest
    @CsvSource({
            "1, 1.0",
            "'1, 2', 1.5",
            "'1, 2, 3', 2.0",
            "'1, 2, 3, 4', 2.5",
            "'1,2,3,4,5,6,7', 4.0",
            "'1,2,3,4,5,6,7,8', 4.5",
            "'1,1,1,1,1', 1.0",
            "'-1,-2,-3,-4,-5', -3.0"
    })
    void testFindMedian(@ConvertWith(StringToIntListConverter.class) List<Integer> nums, double expectedMedian) {
        MedianFinderExercise mf = new MedianFinderExercise();

        for (int num : nums) {
            mf.addNum(num);
        }

        assertEquals(expectedMedian, mf.findMedian());
    }

    @Test
    void testFindMedianOnEmptyList() {
        var mf = new MedianFinderExercise();

        IllegalStateException exception = assertThrows(IllegalStateException.class, mf::findMedian);

        assertEquals("No elements in the MedianFinder", exception.getMessage());
    }
}
