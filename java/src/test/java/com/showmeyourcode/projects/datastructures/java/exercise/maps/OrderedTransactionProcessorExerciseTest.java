package com.showmeyourcode.projects.datastructures.java.exercise.maps;

import nl.altindag.log.LogCaptor;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OrderedTransactionProcessorExerciseTest {

    @ParameterizedTest
    @CsvSource({
            "'1:income:1000,2:expense:200,3:income:500,4:expense:300,5:income:200', '1=1000,2=800,3=1300,4=1000,5=1200'",
            // invalid and mixed records
            "'', ''",
            "'1:income:1000,2:expense200,3:income:five00,4:expense:300,five:income:200', '1=1000,4=700'",
            "'1:income:1000,2:income:200,3:expense:300,4:expense:500,5:income:100', '1=1000,2=1200,3=900,4=400,5=500'"
    })
    void testProcessOrderedTransactions(String transactions, String expected) {
        List<String> transactionList = transactions.isEmpty() ? Collections.emptyList() : Arrays.asList(transactions.split(","));
        Map<Integer, Integer> expectedMap = parseExpectedMap(expected);

        assertEquals(expectedMap, OrderedTransactionProcessorExercise.processOrderedTransactions(transactionList));
    }

    @Test
    void testNullInput() {
        assertThrows(IllegalArgumentException.class, () -> OrderedTransactionProcessorExercise.processOrderedTransactions(null));
    }

    @Test
    void testPrintElements() {
        Map<Integer, String> elements = new HashMap<>();
        elements.put(1, "One");
        elements.put(2, "Two");
        elements.put(3, "Three");

        // Start capturing logs
        LogCaptor logCaptor = LogCaptor.forClass(OrderedTransactionProcessorExercise.class);

        // Call the method
        OrderedTransactionProcessorExercise.printElements(elements);

        // Get the captured logs
        var logs = logCaptor.getLogs();

        // Verify the logs
        assertEquals(3, logs.size());
        assertEquals("1: One", logs.get(0));
        assertEquals("2: Two", logs.get(1));
        assertEquals("3: Three", logs.get(2));

        // Stop capturing logs
        logCaptor.close();
    }

    private Map<Integer, Integer> parseExpectedMap(String expected) {
        if (expected.isEmpty()) return new LinkedHashMap<>();

        Map<Integer, Integer> resultMap = new LinkedHashMap<>();
        String[] entries = expected.split(",");
        for (String entry : entries) {
            String[] keyValue = entry.split("=");
            int key = Integer.parseInt(keyValue[0]);
            int value = Integer.parseInt(keyValue[1]);
            resultMap.put(key, value);
        }
        return resultMap;
    }
}
