package com.showmeyourcode.projects.datastructures.java.custom.heap;

public class DefaultHeapTest extends HeapBaseTest {
    @Override
    Heap constructMaxHeap() {
        return DefaultHeap.maxHeap();
    }

    @Override
    Heap constructMinHeap() {
        return DefaultHeap.minHeap();
    }
}
