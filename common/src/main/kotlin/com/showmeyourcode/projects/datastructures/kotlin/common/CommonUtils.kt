package com.showmeyourcode.projects.datastructures.kotlin.common

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER

const val word: String = "A long time ago in a galaxy far far away"

fun printAll(collection: Collection<Any>, prefix: String = "") =
    LOGGER.info("$prefix${collection.joinToString(prefix = "[", postfix = "]\n")}")
