package com.showmeyourcode.projects.datastructures.kotlin.custom.tree

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class RedBlackTreeTest : StringSpec({

    "should insert nodes and maintain Red-Black Tree properties" {
        val redBlackTree = RedBlackTree<Int>()

        redBlackTree.insert(10)
        redBlackTree.insert(7)
        redBlackTree.insert(6)
        redBlackTree.insert(2)
        redBlackTree.insert(11)
        redBlackTree.insert(8)
        redBlackTree.insert(1)
        redBlackTree.insert(5)
        redBlackTree.insert(0)
        redBlackTree.insert(15)
        redBlackTree.insert(13)
        redBlackTree.insert(17)
        redBlackTree.insert(16)
        redBlackTree.insert(14)

        val inOrderResult = redBlackTree.inOrderTraversal()
        inOrderResult shouldBe listOf(0, 1, 2, 5, 6, 7, 8, 10, 11, 13, 14, 15, 16, 17)
    }

    "should delete nodes and maintain Red-Black Tree properties" {
        val redBlackTree = RedBlackTree<Int>()

        shouldThrow<NotImplementedError> {
            redBlackTree.delete(1)
        }
    }

    "should return nodes in pre-order" {
        val redBlackTree = RedBlackTree<Int>()

        redBlackTree.insert(10)
        redBlackTree.insert(5)
        redBlackTree.insert(15)
        redBlackTree.insert(3)
        redBlackTree.insert(7)

        val preOrderResult = redBlackTree.preOrderTraversal()
        preOrderResult shouldBe listOf(10, 5, 3, 7, 15)
    }

    "should return nodes in post-order" {
        val redBlackTree = RedBlackTree<Int>()

        redBlackTree.insert(10)
        redBlackTree.insert(5)
        redBlackTree.insert(15)
        redBlackTree.insert(3)
        redBlackTree.insert(7)

        val postOrderResult = redBlackTree.postOrderTraversal()
        postOrderResult shouldBe listOf(3, 7, 5, 15, 10)
    }

    "should return nodes in level-order" {
        val redBlackTree = RedBlackTree<Int>()

        redBlackTree.insert(10)
        redBlackTree.insert(5)
        redBlackTree.insert(15)
        redBlackTree.insert(3)
        redBlackTree.insert(7)

        val levelOrderResult = redBlackTree.levelOrderTraversal()
        levelOrderResult shouldBe listOf(10, 5, 15, 3, 7)
    }
})
