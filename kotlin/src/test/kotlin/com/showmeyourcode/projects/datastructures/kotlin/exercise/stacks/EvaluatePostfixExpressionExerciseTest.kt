package com.showmeyourcode.projects.datastructures.kotlin.exercise.stacks

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.blocking.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe

class EvaluatePostfixExpressionExerciseTest : AnnotationSpec() {

    @Test
    fun `evaluate postfix expressions`() {
        forAll(
            row("3 4 +", 7),
            row("5 1 2 + 4 * + 3 -", 14),
            row("1 2 3 * + 4 +", 11),
            row("10 5 /", 2),
            row("20 4 / 2 *", 10),
            row("7 2 3 * - 5 +", 6),
            row("8 4 / 2 *", 4),
            row("5 2 /", 2),
            row("6 2 /", 3),
            row("4 2 * 5 + 1 -", 12),
            row("9 3 1 - /", 4),
            row("7", 7)
        ) { expression, expected ->
            evaluatePostfixExpression(expression) shouldBe expected
        }
    }

    @Test
    fun `handle errors`() {
        forAll(
            row("5 6 + -", "ArrayDeque is empty."),
            row("", "Invalid token in expression: "),
            row("4 0 /", "/ by zero"),
            row("4 + 2", "ArrayDeque is empty."),
            row("4 2 @", "Invalid token in expression: @")
        ) { expression, expectedMessage ->
            val exception = shouldThrow<Exception> {
                evaluatePostfixExpression(expression)
            }
            exception.message shouldBe expectedMessage
        }
    }
}
