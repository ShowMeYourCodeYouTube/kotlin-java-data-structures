package com.showmeyourcode.projects.datastructures.kotlin.exercise.maps

import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.blocking.forAll
import io.kotest.data.row
import io.kotest.matchers.maps.shouldContainExactly

class StudentGradeAnalyzerExerciseTest : AnnotationSpec() {

    @Test
    fun `should correctly calculate average grades for students`() {
        forAll(
            row(
                listOf(
                    StudentGrade("A001", 85.0),
                    StudentGrade("A002", 75.0),
                    StudentGrade("A001", 90.0),
                    StudentGrade("A003", 80.0),
                    StudentGrade("A002", 70.0),
                    StudentGrade("A001", 95.0)
                ),
                mapOf(
                    "A001" to 90.0,
                    "A002" to 72.5,
                    "A003" to 80.0
                )
            ),
            row(
                listOf(
                    StudentGrade("B001", 100.0),
                    StudentGrade("B001", 100.0)
                ),
                mapOf(
                    "B001" to 100.0
                )
            ),
            row(
                listOf(),
                mapOf<String, Double>()
            ),
            row(
                listOf(
                    StudentGrade("C001", 60.0),
                    StudentGrade("C002", 70.0),
                    StudentGrade("C001", 80.0),
                    StudentGrade("C002", 90.0),
                    StudentGrade("C001", 100.0)
                ),
                mapOf(
                    "C001" to 80.0,
                    "C002" to 80.0
                )
            )
        ) { grades, expected ->
            analyzeStudentGrades(grades) shouldContainExactly expected
        }
    }

    @Test
    fun `should log map entries`() {
        print(mapOf(1 to "one", 2 to "two"))
    }
}
