package com.showmeyourcode.projects.datastructures.kotlin.custom.graph

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class GraphMSTTest : StringSpec({

    val graphMinimumSpanningTree = WeightedGraph(WeightedAdjacencyMatrix(5, GraphType.UNDIRECTED))

    // Add weighted edges
    graphMinimumSpanningTree.addEdge(0, 1, 2.0)
    graphMinimumSpanningTree.addEdge(0, 3, 1.0)
    graphMinimumSpanningTree.addEdge(1, 2, 3.0)
    graphMinimumSpanningTree.addEdge(1, 3, 3.0)
    graphMinimumSpanningTree.addEdge(2, 4, 2.0)
    graphMinimumSpanningTree.addEdge(3, 4, 1.0)

    "should find MST using Prim's algorithm" {
        graphMinimumSpanningTree.primMST() shouldBe arrayListOf(Pair(0, 3), Pair(3, 4), Pair(0, 1), Pair(4, 2))
    }

    "should find MST using Kruskal's algorithm" {
        graphMinimumSpanningTree.kruskalMST() shouldBe arrayListOf(Pair(0, 3), Pair(3, 4), Pair(0, 1), Pair(2, 4))
    }
})
