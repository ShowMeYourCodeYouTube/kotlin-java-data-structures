package com.showmeyourcode.projects.datastructures.kotlin.custom.graph

import io.kotest.core.spec.style.StringSpec
import io.kotest.data.blocking.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe

class GraphShortestPathTest : StringSpec({

    "should find the shortest path using Dijkstra algorithm" {
        forAll(
            row(
                listOf(
                    listOf(Pair(1, 2.0), Pair(2, 4.0)),
                    listOf(Pair(2, 1.0), Pair(3, 7.0)),
                    listOf(Pair(4, 3.0)),
                    listOf(Pair(4, 1.0), Pair(5, 5.0)),
                    listOf(Pair(5, 2.0)),
                    listOf()
                ),
                GraphType.UNDIRECTED,
                0,
                listOf(0.0, 2.0, 3.0, 7.0, 6.0, 8.0)
            ),
            row(
                listOf(
                    listOf(Pair(1, 2.0), Pair(3, 1.0)),
                    listOf(Pair(2, 3.0), Pair(3, 2.0), Pair(4, 4.0)),
                    listOf(Pair(4, 5.0)),
                    listOf(Pair(4, 1.0)),
                    listOf()
                ),
                GraphType.UNDIRECTED,
                0,
                listOf(0.0, 2.0, 5.0, 1.0, 2.0)
            ),
            row(
                listOf(
                    listOf(Pair(1, 10.0)),
                    listOf(Pair(2, 5.0)),
                    listOf(Pair(3, 2.0)),
                    listOf(),
                    listOf(Pair(5, 3.0)),
                    listOf()
                ),
                GraphType.DIRECTED,
                0,
                listOf(0.0, 10.0, 15.0, 17.0, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY)
            ),
            row(
                listOf(
                    listOf(Pair(1, 1.0), Pair(2, 4.0)),
                    listOf(Pair(2, 2.0), Pair(3, 6.0)),
                    listOf(Pair(3, 3.0)),
                    listOf(Pair(4, 7.0)),
                    listOf()
                ),
                GraphType.DIRECTED,
                0,
                listOf(0.0, 1.0, 3.0, 6.0, 13.0)
            ),
            row(
                listOf(
                    listOf(Pair(1, 5.0), Pair(2, 10.0)),
                    listOf(Pair(2, 2.0)),
                    listOf(Pair(3, 1.0)),
                    listOf(Pair(4, 7.0)),
                    listOf()
                ),
                GraphType.UNDIRECTED,
                0,
                listOf(0.0, 5.0, 7.0, 8.0, 15.0)
            ),
            row(
                listOf(
                    listOf(Pair(1, 3.0), Pair(2, 6.0)),
                    listOf(Pair(2, 1.0)),
                    listOf(Pair(3, 4.0)),
                    listOf(Pair(4, 5.0)),
                    listOf()
                ),
                GraphType.DIRECTED,
                0,
                listOf(0.0, 3.0, 4.0, 8.0, 13.0)
            ),
            row(
                listOf(
                    listOf(Pair(1, 2.0)),
                    listOf(Pair(2, 3.0)),
                    listOf(Pair(3, 1.0)),
                    listOf(),
                    listOf(Pair(5, 6.0)),
                    listOf()
                ),
                GraphType.DIRECTED,
                0,
                listOf(0.0, 2.0, 5.0, 6.0, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY)
            ),
            row(
                listOf(
                    listOf(Pair(1, 4.0)),
                    listOf(Pair(2, 1.0)),
                    listOf(Pair(3, 3.0)),
                    listOf(),
                    listOf(Pair(5, 2.0)),
                    listOf()
                ),
                GraphType.UNDIRECTED,
                0,
                listOf(0.0, 4.0, 5.0, 8.0, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY)
            ),
            row(
                listOf(
                    listOf(Pair(1, 7.0)),
                    listOf(Pair(2, 2.0)),
                    listOf(Pair(3, 6.0)),
                    listOf(),
                    listOf(Pair(5, 3.0)),
                    listOf()
                ),
                GraphType.DIRECTED,
                0,
                listOf(0.0, 7.0, 9.0, 15.0, Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY)
            ),
            row(
                listOf(
                    listOf(Pair(1, 5.0), Pair(3, 2.0)),
                    listOf(Pair(2, 2.0)),
                    listOf(Pair(4, 1.0)),
                    listOf(Pair(4, 7.0)),
                    listOf()
                ),
                GraphType.UNDIRECTED,
                0,
                listOf(0.0, 5.0, 7.0, 2.0, 8.0)
            )
        ) { adjacencyList, graphType, startVertex, expected ->
            val weightedGraphAdjacencyList = createWeightedGraphWithAdjacencyList(adjacencyList, graphType)
            val weightedGraphAdjacencyMatrix = createWeightedGraphWithAdjacencyMatrix(adjacencyList, graphType)

            weightedGraphAdjacencyList.dijkstra(startVertex) shouldBe expected
            weightedGraphAdjacencyMatrix.dijkstra(startVertex) shouldBe expected
        }
    }
})
