package com.showmeyourcode.projects.datastructures.kotlin.custom.tree

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class TernaryTreeTest : StringSpec({
    "should insert nodes" {
        val ternaryTree = TernaryTree<Int>()

        ternaryTree.insert(10)
        ternaryTree.insert(5)
        ternaryTree.insert(15)
        ternaryTree.insert(3)
        ternaryTree.insert(7)

        val inOrderResult = ternaryTree.inOrderTraversal()
        inOrderResult shouldBe listOf(3, 5, 7, 10, 15)
    }

    "should delete a node" {
        val ternaryTree = TernaryTree<Int>()

        ternaryTree.insert(10)
        ternaryTree.insert(5)
        ternaryTree.insert(15)
        ternaryTree.insert(3)
        ternaryTree.insert(7)
        ternaryTree.insert(1)
        ternaryTree.insert(2)
        ternaryTree.insert(0)
        ternaryTree.insert(5)
        ternaryTree.insert(5)
        ternaryTree.insert(5)

        val inOrderResult = ternaryTree.inOrderTraversal()
        inOrderResult shouldBe listOf(0, 1, 2, 3, 5, 5, 5, 5, 7, 10, 15)

        ternaryTree.delete(5)
        ternaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 5, 5, 5, 7, 10, 15)

        ternaryTree.delete(5)
        ternaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 5, 5, 7, 10, 15)

        ternaryTree.delete(5)
        ternaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 5, 7, 10, 15)

        ternaryTree.delete(5)
        ternaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 7, 10, 15)

        ternaryTree.delete(5)
        ternaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 7, 10, 15)

        ternaryTree.delete(10)
        ternaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 7, 15)

        ternaryTree.delete(0)
        ternaryTree.inOrderTraversal() shouldBe listOf(1, 2, 3, 7, 15)
    }

    "should find relevant nodes" {
        val ternaryTree = TernaryTree<Int>()

        ternaryTree.insert(10)
        ternaryTree.insert(5)
        ternaryTree.insert(15)
        ternaryTree.insert(3)
        ternaryTree.insert(7)
        ternaryTree.insert(1)
        ternaryTree.insert(2)
        ternaryTree.insert(0)
        ternaryTree.insert(5)
        ternaryTree.insert(5)
        ternaryTree.insert(5)

        ternaryTree.find(12345) shouldBe false
        ternaryTree.find(45) shouldBe false
        ternaryTree.find(-5) shouldBe false
        ternaryTree.find(66) shouldBe false
        ternaryTree.find(77) shouldBe false

        ternaryTree.find(0) shouldBe true
        ternaryTree.find(1) shouldBe true
        ternaryTree.find(2) shouldBe true
        ternaryTree.find(3) shouldBe true
        ternaryTree.find(5) shouldBe true
        ternaryTree.find(7) shouldBe true
        ternaryTree.find(10) shouldBe true
        ternaryTree.find(15) shouldBe true
    }

    "should return nodes in pre-order" {
        val ternaryTree = TernaryTree<Int>()

        ternaryTree.insert(10)
        ternaryTree.insert(5)
        ternaryTree.insert(15)
        ternaryTree.insert(3)
        ternaryTree.insert(7)

        val preOrderResult = ternaryTree.preOrderTraversal()
        preOrderResult shouldBe listOf(10, 5, 3, 7, 15)
    }

    "should return nodes in post-order" {
        val ternaryTree = TernaryTree<Int>()

        ternaryTree.insert(10)
        ternaryTree.insert(5)
        ternaryTree.insert(15)
        ternaryTree.insert(3)
        ternaryTree.insert(7)

        val postOrderResult = ternaryTree.postOrderTraversal()
        postOrderResult shouldBe listOf(3, 7, 5, 15, 10)
    }

    "should return nodes in level-order" {
        val ternaryTree = TernaryTree<Int>()

        ternaryTree.insert(10)
        ternaryTree.insert(5)
        ternaryTree.insert(15)
        ternaryTree.insert(3)
        ternaryTree.insert(7)

        val levelOrderResult = ternaryTree.levelOrderTraversal()
        levelOrderResult shouldBe listOf(10, 5, 15, 3, 7)
    }
})
