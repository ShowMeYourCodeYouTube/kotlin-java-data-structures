package com.showmeyourcode.projects.datastructures.kotlin.exercise.maps

import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.blocking.forAll
import io.kotest.data.row
import io.kotest.matchers.maps.shouldContainExactly

class SalesDataAnalysisExerciseTest : AnnotationSpec() {

    @Test
    fun `should correctly calculate total sales for each month`() {
        forAll(
            row(
                listOf(
                    SaleRecord("ProductA", 100.0, "2023-05"),
                    SaleRecord("ProductB", 150.0, "2023-05"),
                    SaleRecord("ProductA", 200.0, "2023-06"),
                    SaleRecord("ProductB", 100.0, "2023-04"),
                    SaleRecord("ProductC", 300.0, "2023-06"),
                    SaleRecord("ProductC", 50.0, "2023-05")
                ),
                sortedMapOf(
                    "2023-04" to 100.0,
                    "2023-05" to 300.0,
                    "2023-06" to 500.0
                )
            ),
            row(
                listOf(
                    SaleRecord("ProductA", 100.0, "2022-01"),
                    SaleRecord("ProductA", 150.0, "2022-02")
                ),
                sortedMapOf(
                    "2022-01" to 100.0,
                    "2022-02" to 150.0
                )
            ),
            row(
                emptyList<SaleRecord>(),
                sortedMapOf<String, Double>()
            )
        ) { sales, expected ->
            analyzeMonthlySales(sales) shouldContainExactly expected
        }
    }
}
