package com.showmeyourcode.projects.datastructures.kotlin.exercise.lists

import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.forAll
import io.kotest.data.headers
import io.kotest.data.row
import io.kotest.data.table
import io.kotest.matchers.shouldBe

class GeneratePascalTriangleExerciseTest : AnnotationSpec() {

    @Test
    fun `should generate Pascal's Triangle correctly`() {
        forAll(
            table(
                headers("triangle height", "expected output"),
                row(1, listOf(listOf(1))),
                row(2, listOf(listOf(1), listOf(1, 1))),
                row(3, listOf(listOf(1), listOf(1, 1), listOf(1, 2, 1))),
                row(4, listOf(listOf(1), listOf(1, 1), listOf(1, 2, 1), listOf(1, 3, 3, 1))),
                row(5, listOf(listOf(1), listOf(1, 1), listOf(1, 2, 1), listOf(1, 3, 3, 1), listOf(1, 4, 6, 4, 1)))
            )
        ) { input, expected ->
            generatePascalTriangle(input) shouldBe expected
        }
    }
}
