package com.showmeyourcode.projects.datastructures.kotlin.custom.graph

import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.blocking.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe

class GraphTypeTest : AnnotationSpec() {

    @Test
    fun `test isAcyclic`() {
        forAll(
            row(createGraphWithAdjacencyList(listOf(mutableListOf(1), mutableListOf(0)), GraphType.UNDIRECTED), false),
            row(createGraphWithAdjacencyList(listOf(mutableListOf(1), mutableListOf(0, 2), mutableListOf(1)), GraphType.DIRECTED), false),
            row(
                createGraphWithAdjacencyList(listOf(mutableListOf(1), mutableListOf(2), mutableListOf(3), mutableListOf()), GraphType.DIRECTED),
                true
            ),
            row(createGraphWithAdjacencyList(listOf(mutableListOf(1, 2), mutableListOf(0, 2), mutableListOf(0, 1)), GraphType.DIRECTED), false),
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf(4), mutableListOf(4), mutableListOf(0, 3), mutableListOf(0), mutableListOf()),
                    GraphType.DIRECTED
                ),
                true
            )
        ) { graph, expected ->
            graph.isAcyclic() shouldBe expected
        }
    }

    @Test
    fun `test isComplete`() {
        forAll(
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf()),
                    GraphType.DIRECTED
                ),
                true
            ),
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf(1), mutableListOf(0)),
                    GraphType.DIRECTED
                ),
                true
            ),
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf(1, 2), mutableListOf(0, 2), mutableListOf(0, 1)),
                    GraphType.DIRECTED
                ),
                true
            ),
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf(1, 2), mutableListOf(0, 2), mutableListOf(0, 1, 3), mutableListOf(2)),
                    GraphType.DIRECTED
                ),
                false
            ),
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf(1, 2, 3), mutableListOf(0, 2, 3), mutableListOf(0, 1, 3), mutableListOf(0, 1, 2)),
                    GraphType.DIRECTED
                ),
                true
            )
        ) { graph, expected ->
            graph.isComplete() shouldBe expected
        }
    }

    @Test
    fun `test isTrivial`() {
        forAll(
            row(createGraphWithAdjacencyList(listOf(mutableListOf()), GraphType.DIRECTED), true),
            row(createGraphWithAdjacencyList(listOf(mutableListOf(1), mutableListOf(0)), GraphType.DIRECTED), false),
            row(createGraphWithAdjacencyList(listOf(mutableListOf(1, 2), mutableListOf(0, 2), mutableListOf(0, 1)), GraphType.DIRECTED), false),
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf(1, 2), mutableListOf(0, 2), mutableListOf(0, 1, 3), mutableListOf(2)),
                    GraphType.DIRECTED
                ),
                false
            ),
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf(1, 2, 3), mutableListOf(0, 2, 3), mutableListOf(0, 1, 3), mutableListOf(0, 1, 2)),
                    GraphType.DIRECTED
                ),
                false
            )
        ) { graph, expected ->
            graph.isTrivial() shouldBe expected
        }
    }

    @Test
    fun `test isSimple`() {
        forAll(
            row(createGraphWithAdjacencyList(listOf(mutableListOf(1), mutableListOf(0)), GraphType.DIRECTED), true),
            row(createGraphWithAdjacencyList(listOf(mutableListOf(1), mutableListOf(0, 2), mutableListOf(1)), GraphType.DIRECTED), true),
            row(createGraphWithAdjacencyList(listOf(mutableListOf(1, 1), mutableListOf(0, 0)), GraphType.DIRECTED), false),
            row(createGraphWithAdjacencyList(listOf(mutableListOf(1), mutableListOf(0, 2, 2), mutableListOf(1)), GraphType.DIRECTED), false)
        ) { graph, expected ->
            graph.isSimple() shouldBe expected
        }
    }

    @Test
    fun `test isRegular`() {
        forAll(
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf(1, 2), mutableListOf(0, 2), mutableListOf(0, 1)),
                    GraphType.DIRECTED
                ),
                true
            ),
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf(1), mutableListOf(0, 2), mutableListOf(1)),
                    GraphType.DIRECTED
                ),
                false
            ),
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf(1, 2, 3), mutableListOf(0, 2, 3), mutableListOf(0, 1, 3), mutableListOf(0, 1, 2)),
                    GraphType.DIRECTED
                ),
                true
            ),
            row(
                createGraphWithAdjacencyList(
                    listOf(mutableListOf(1, 2), mutableListOf(0, 2), mutableListOf(0, 1, 3), mutableListOf(2)),
                    GraphType.DIRECTED
                ),
                false
            )
        ) { graph, expected ->
            graph.isRegular() shouldBe expected
        }
    }
}
