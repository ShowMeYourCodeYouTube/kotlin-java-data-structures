package com.showmeyourcode.projects.datastructures.kotlin.exercise.heaps

import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.blocking.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe

class KthLargestExerciseTest : AnnotationSpec() {

    @Test
    fun `test KthLargest with initial elements and additions`() {
        val kthLargest = KthLargestExercise(3, intArrayOf(4, 5, 8, 2))

        forAll(
            row(3, 4),
            row(5, 5),
            row(10, 5),
            row(9, 8),
            row(4, 8)
        ) { value, expected ->
            kthLargest.add(value) shouldBe expected
        }
    }

    @Test
    fun `test KthLargest with smaller initial array`() {
        val kthLargest = KthLargestExercise(3, intArrayOf(1, 2))

        forAll(
            row(3, 1),
            row(4, 2),
            row(5, 3),
            row(6, 4)
        ) { value, expected ->
            kthLargest.add(value) shouldBe expected
        }
    }

    @Test
    fun `test KthLargest with empty initial array`() {
        val kthLargest = KthLargestExercise(1, intArrayOf())

        forAll(
            row(1, 1),
            row(2, 2),
            row(3, 3),
            row(4, 4)
        ) { value, expected ->
            kthLargest.add(value) shouldBe expected
        }
    }
}
