package com.showmeyourcode.projects.datastructures.kotlin.custom.tree

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class AVLTreeTest : StringSpec({

    "should traversal the tree using the in-order algorithm" {
        val avlTree = AVLTree<Int>()

        avlTree.insert(10)
        avlTree.insert(5)
        avlTree.insert(15)
        avlTree.insert(3)
        avlTree.insert(7)

        val inOrderResult = avlTree.inOrderTraversal()
        inOrderResult shouldBe listOf(3, 5, 7, 10, 15)

        val complexAvlTree = AVLTree<Int>()

        complexAvlTree.insert(10)
        complexAvlTree.insert(7)
        complexAvlTree.insert(6)
        complexAvlTree.insert(2)
        complexAvlTree.insert(11)
        complexAvlTree.insert(8)
        complexAvlTree.insert(1)
        complexAvlTree.insert(5)
        complexAvlTree.insert(0)
        complexAvlTree.insert(15)
        complexAvlTree.insert(13)
        complexAvlTree.insert(17)
        complexAvlTree.insert(16)
        complexAvlTree.insert(14)

        val inOrderComplexResult = complexAvlTree.inOrderTraversal()
        inOrderComplexResult shouldBe listOf(0, 1, 2, 5, 6, 7, 8, 10, 11, 13, 14, 15, 16, 17)
    }

    "should traversal the tree using the in-order algorithm when deleting and inserting nodes" {
        val avlTree = AVLTree<Int>()

        avlTree.insert(10)
        avlTree.insert(5)
        avlTree.insert(15)
        avlTree.insert(3)
        avlTree.insert(7)
        avlTree.insert(1)
        avlTree.insert(2)
        avlTree.insert(0)

        val inOrderResult = avlTree.inOrderTraversal()
        inOrderResult shouldBe listOf(0, 1, 2, 3, 5, 7, 10, 15)

        avlTree.delete(5)
        avlTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 7, 10, 15)

        avlTree.delete(5)
        avlTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 7, 10, 15)

        avlTree.delete(10)
        avlTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 7, 15)

        avlTree.delete(1)
        avlTree.inOrderTraversal() shouldBe listOf(0, 2, 3, 7, 15)

        avlTree.delete(2)
        avlTree.inOrderTraversal() shouldBe listOf(0, 3, 7, 15)

        avlTree.delete(0)
        avlTree.inOrderTraversal() shouldBe listOf(3, 7, 15)

        avlTree.delete(3)
        avlTree.inOrderTraversal() shouldBe listOf(7, 15)

        avlTree.delete(15)
        avlTree.inOrderTraversal() shouldBe listOf(7)
    }

    "should traversal the tree using the pre-order algorithm" {
        val avlTree = AVLTree<Int>()

        avlTree.insert(10)
        avlTree.insert(5)
        avlTree.insert(15)
        avlTree.insert(3)
        avlTree.insert(7)

        val preOrderResult = avlTree.preOrderTraversal()
        preOrderResult shouldBe listOf(10, 5, 3, 7, 15)
    }

    "should traversal the tree using the post-order algorithm" {
        val avlTree = AVLTree<Int>()

        avlTree.insert(10)
        avlTree.insert(5)
        avlTree.insert(15)
        avlTree.insert(3)
        avlTree.insert(7)

        val postOrderResult = avlTree.postOrderTraversal()
        postOrderResult shouldBe listOf(3, 7, 5, 15, 10)
    }

    "should traversal the tree using the level-order algorithm" {
        val avlTree = AVLTree<Int>()

        avlTree.insert(10)
        avlTree.insert(5)
        avlTree.insert(15)
        avlTree.insert(3)
        avlTree.insert(7)

        val levelOrderResult = avlTree.levelOrderTraversal()
        levelOrderResult shouldBe listOf(10, 5, 15, 3, 7)
    }
})
