package com.showmeyourcode.projects.datastructures.kotlin.exercise.lists

import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.blocking.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe

class ProcessTransactionsExerciseTest : AnnotationSpec() {

    @Test
    fun `processTransactions should correctly process and summarize transactions`() {
        forAll(
            row(
                listOf("income:1000", "expense:200", "income:500", "expense:300", "income:200"),
                TransactionSummary(
                    sortedIncomes = listOf(1000, 500, 200),
                    sortedExpenses = listOf(300, 200),
                    totalIncome = 1700,
                    totalExpense = 500,
                    netBalance = 1200
                )
            ),
            row(
                listOf("income:150", "expense:50", "income:50", "expense:25", "income:25"),
                TransactionSummary(
                    sortedIncomes = listOf(150, 50, 25),
                    sortedExpenses = listOf(50, 25),
                    totalIncome = 225,
                    totalExpense = 75,
                    netBalance = 150
                )
            ),
            row(
                listOf("income:200", "expense:100", "income:100", "expense:200"),
                TransactionSummary(
                    sortedIncomes = listOf(200, 100),
                    sortedExpenses = listOf(200, 100),
                    totalIncome = 300,
                    totalExpense = 300,
                    netBalance = 0
                )
            ),
            row(
                listOf("expense:500", "expense:300", "income:800", "income:200"),
                TransactionSummary(
                    sortedIncomes = listOf(800, 200),
                    sortedExpenses = listOf(500, 300),
                    totalIncome = 1000,
                    totalExpense = 800,
                    netBalance = 200
                )
            ),
            row(
                listOf("income:100", "income:200", "income:300"),
                TransactionSummary(
                    sortedIncomes = listOf(300, 200, 100),
                    sortedExpenses = listOf(),
                    totalIncome = 600,
                    totalExpense = 0,
                    netBalance = 600
                )
            ),
            row(
                listOf("expense:100", "expense:200", "expense:300"),
                TransactionSummary(
                    sortedIncomes = listOf(),
                    sortedExpenses = listOf(300, 200, 100),
                    totalIncome = 0,
                    totalExpense = 600,
                    netBalance = -600
                )
            )
        ) { transactions, expectedSummary ->

            val result = processTransactions(transactions)

            result shouldBe expectedSummary
        }
    }
}
