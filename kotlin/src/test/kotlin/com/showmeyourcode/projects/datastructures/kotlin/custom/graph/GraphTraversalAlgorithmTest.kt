package com.showmeyourcode.projects.datastructures.kotlin.custom.graph

import io.kotest.core.spec.style.StringSpec
import io.kotest.data.blocking.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe

class GraphTraversalAlgorithmTest : StringSpec({

    "BFS should traverse a graph correctly" {
        forAll(
            row(
                listOf(mutableListOf(1, 2), mutableListOf(3, 4), mutableListOf(4), mutableListOf(5), mutableListOf(5), mutableListOf()),
                GraphType.UNDIRECTED,
                0,
                listOf(0, 1, 2, 3, 4, 5)
            ),
            row(
                listOf(mutableListOf(1), mutableListOf(0, 2), mutableListOf(1)),
                GraphType.DIRECTED,
                1,
                listOf(1, 0, 2)
            ),
            row(

                listOf(mutableListOf(1, 2), mutableListOf(0, 3), mutableListOf(0, 4), mutableListOf(1), mutableListOf(2)),
                GraphType.UNDIRECTED,

                0,
                listOf(0, 1, 2, 3, 4)
            ),
            row(

                listOf(mutableListOf(), mutableListOf(2, 3), mutableListOf(3), mutableListOf(), mutableListOf(1)),
                GraphType.DIRECTED,
                4,
                listOf(4, 1, 2, 3)
            ),
            row(

                listOf(mutableListOf(1), mutableListOf(2), mutableListOf(3), mutableListOf(4), mutableListOf(5), mutableListOf()),
                GraphType.UNDIRECTED,

                0,
                listOf(0, 1, 2, 3, 4, 5)
            ),
            row(

                listOf(mutableListOf(1, 2, 3, 4), mutableListOf(0), mutableListOf(0), mutableListOf(0), mutableListOf(0)),
                GraphType.UNDIRECTED,

                0,
                listOf(0, 1, 2, 3, 4)
            ),
            row(

                listOf(
                    mutableListOf(1, 2, 3),
                    mutableListOf(4),
                    mutableListOf(5),
                    mutableListOf(6),
                    mutableListOf(),
                    mutableListOf(),
                    mutableListOf()
                ),
                GraphType.DIRECTED,

                0,
                listOf(0, 1, 2, 3, 4, 5, 6)
            ),
            row(

                listOf(mutableListOf(1), mutableListOf(2), mutableListOf(0, 3), mutableListOf()),
                GraphType.UNDIRECTED,

                0,
                listOf(0, 1, 2, 3)
            ),
            row(

                listOf(mutableListOf(1, 3), mutableListOf(2), mutableListOf(), mutableListOf(4), mutableListOf()),
                GraphType.DIRECTED,

                0,
                listOf(0, 1, 3, 2, 4)
            ),
            row(

                listOf(mutableListOf(1, 4), mutableListOf(2, 3), mutableListOf(), mutableListOf(), mutableListOf(5), mutableListOf()),
                GraphType.DIRECTED,

                0,
                listOf(0, 1, 4, 2, 3, 5)
            ),
            row(

                listOf(
                    mutableListOf(1, 2),
                    mutableListOf(3),
                    mutableListOf(4),
                    mutableListOf(5),
                    mutableListOf(5),
                    mutableListOf()
                ),
                GraphType.DIRECTED,
                0,
                listOf(0, 1, 2, 3, 4, 5)
            ),
            row(

                listOf(
                    mutableListOf(1, 2),
                    mutableListOf(3),
                    mutableListOf(4),
                    mutableListOf(5),
                    mutableListOf(5),
                    mutableListOf()
                ),
                GraphType.UNDIRECTED,
                0,
                listOf(0, 1, 2, 3, 4, 5)
            )
        ) { adjacencyList, graphType, startVertex, expected ->
            val graphAdjacencyList = createGraphWithAdjacencyList(adjacencyList, graphType)
            val graphAdjacencyMatrix = createGraphWithAdjacencyMatrix(adjacencyList, graphType)

            graphAdjacencyList.bfs(startVertex) shouldBe expected
            graphAdjacencyMatrix.bfs(startVertex) shouldBe expected

            // DFS or BFS on weighted graphs is exactly the same as on unweighted graphs,
            // since both algorithms don't use any edge weights.
            val weightedGraphAdjacencyList = createWeightedGraphWithAdjacencyList(addRandomWeights(adjacencyList), graphType)
            val weightedGraphAdjacencyMatrix = createWeightedGraphWithAdjacencyMatrix(addRandomWeights(adjacencyList), graphType)

            weightedGraphAdjacencyList.bfs(startVertex) shouldBe expected
            weightedGraphAdjacencyMatrix.bfs(startVertex) shouldBe expected
        }
    }

    "DFS should traverse a graph correctly" {
        forAll(
            row(
                listOf(mutableListOf(1, 2), mutableListOf(3, 4), mutableListOf(4), mutableListOf(5), mutableListOf(5), mutableListOf()),
                GraphType.UNDIRECTED,
                0,
                listOf(0, 1, 3, 5, 4, 2)
            ),
            row(
                listOf(mutableListOf(1), mutableListOf(0, 2), mutableListOf(1)),
                GraphType.DIRECTED,
                1,
                listOf(1, 0, 2)
            ),
            row(

                listOf(mutableListOf(1, 2), mutableListOf(0, 3), mutableListOf(0, 4), mutableListOf(1), mutableListOf(2)),
                GraphType.UNDIRECTED,

                0,
                listOf(0, 1, 3, 2, 4)
            ),
            row(

                listOf(mutableListOf(), mutableListOf(2, 3), mutableListOf(3), mutableListOf(), mutableListOf(1)),
                GraphType.DIRECTED,
                4,
                listOf(4, 1, 2, 3)
            ),
            row(

                listOf(mutableListOf(1), mutableListOf(2), mutableListOf(3), mutableListOf(4), mutableListOf(5), mutableListOf()),
                GraphType.UNDIRECTED,

                0,
                listOf(0, 1, 2, 3, 4, 5)
            ),
            row(

                listOf(mutableListOf(1, 2, 3, 4), mutableListOf(0), mutableListOf(0), mutableListOf(0), mutableListOf(0)),
                GraphType.UNDIRECTED,

                0,
                listOf(0, 1, 2, 3, 4)
            ),
            row(

                listOf(
                    mutableListOf(1, 2, 3),
                    mutableListOf(4),
                    mutableListOf(5),
                    mutableListOf(6),
                    mutableListOf(),
                    mutableListOf(),
                    mutableListOf()
                ),
                GraphType.DIRECTED,

                0,
                listOf(0, 1, 4, 2, 5, 3, 6)
            ),
            row(

                listOf(mutableListOf(1), mutableListOf(2), mutableListOf(0, 3), mutableListOf()),
                GraphType.UNDIRECTED,

                0,
                listOf(0, 1, 2, 3)
            ),
            row(

                listOf(mutableListOf(1, 3), mutableListOf(2), mutableListOf(), mutableListOf(4), mutableListOf()),
                GraphType.DIRECTED,

                0,
                listOf(0, 1, 2, 3, 4)
            ),
            row(

                listOf(mutableListOf(1, 4), mutableListOf(2, 3), mutableListOf(), mutableListOf(), mutableListOf(5), mutableListOf()),
                GraphType.DIRECTED,

                0,
                listOf(0, 1, 2, 3, 4, 5)
            ),
            row(

                listOf(
                    mutableListOf(1, 2),
                    mutableListOf(3),
                    mutableListOf(4),
                    mutableListOf(5),
                    mutableListOf(5),
                    mutableListOf()
                ),
                GraphType.DIRECTED,
                0,
                listOf(0, 1, 3, 5, 2, 4)
            ),
            row(

                listOf(
                    mutableListOf(1, 2),
                    mutableListOf(3),
                    mutableListOf(4),
                    mutableListOf(5),
                    mutableListOf(5),
                    mutableListOf()
                ),
                GraphType.UNDIRECTED,
                0,
                listOf(0, 1, 3, 5, 4, 2)
            )
        ) { adjacencyList, graphType, startVertex, expected ->
            val graphAdjacencyList = createGraphWithAdjacencyList(adjacencyList, graphType)
            val graphAdjacencyMatrix = createGraphWithAdjacencyMatrix(adjacencyList, graphType)

            graphAdjacencyList.dfs(startVertex) shouldBe expected
            graphAdjacencyMatrix.dfs(startVertex) shouldBe expected

            // DFS or BFS on weighted graphs is exactly the same as on unweighted graphs,
            // since both algorithms don't use any edge weights.
            val weightedGraphAdjacencyList = createWeightedGraphWithAdjacencyList(addRandomWeights(adjacencyList), graphType)
            val weightedGraphAdjacencyMatrix = createWeightedGraphWithAdjacencyMatrix(addRandomWeights(adjacencyList), graphType)

            weightedGraphAdjacencyList.dfs(startVertex) shouldBe expected
            weightedGraphAdjacencyMatrix.dfs(startVertex) shouldBe expected
        }
    }
})
