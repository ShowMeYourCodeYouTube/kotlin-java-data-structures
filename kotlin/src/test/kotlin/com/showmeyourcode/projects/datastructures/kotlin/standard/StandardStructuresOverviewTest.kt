package com.showmeyourcode.projects.datastructures.kotlin.standard

import io.kotest.assertions.throwables.shouldNotThrow
import io.kotest.core.spec.style.AnnotationSpec

class StandardStructuresOverviewTest : AnnotationSpec() {

    @Test
    fun `should run the main method without errors`() {
        shouldNotThrow<Exception> {
            main()
        }
    }
}
