package com.showmeyourcode.projects.datastructures.kotlin.custom.heap

class AlternativeHeapTest : HeapBaseTest() {
    override fun constructMaxHeap(): Heap = AlternativeHeap.maxHeap(20)

    override fun constructMinHeap(): Heap = AlternativeHeap.minHeap(20)
}
