package com.showmeyourcode.projects.datastructures.kotlin.custom.graph

import io.kotest.core.spec.style.StringSpec
import io.kotest.data.blocking.forAll
import io.kotest.data.row
import io.kotest.matchers.shouldBe

class GraphCycleTest : StringSpec({

    "should correctly detect cycles" {
        forAll(
            row(
                listOf(mutableListOf(1, 2), mutableListOf(3, 4), mutableListOf(4), mutableListOf(5), mutableListOf(5), mutableListOf()),
                GraphType.UNDIRECTED,
                true
            ),
            row(
                listOf(mutableListOf(1), mutableListOf(0, 2), mutableListOf(1)),
                GraphType.DIRECTED,
                true
            ),
            row(

                listOf(mutableListOf(1, 2), mutableListOf(0, 3), mutableListOf(0, 4), mutableListOf(1), mutableListOf(2)),
                GraphType.UNDIRECTED,
                true
            ),
            row(

                listOf(mutableListOf(), mutableListOf(2, 3), mutableListOf(3), mutableListOf(), mutableListOf(1)),
                GraphType.DIRECTED,
                false
            ),
            row(

                listOf(mutableListOf(1), mutableListOf(2), mutableListOf(3), mutableListOf(4), mutableListOf(5), mutableListOf()),
                GraphType.UNDIRECTED,
                true
            ),
            row(

                listOf(mutableListOf(1, 2, 3, 4), mutableListOf(0), mutableListOf(0), mutableListOf(0), mutableListOf(0)),
                GraphType.UNDIRECTED,
                true
            ),
            row(
                listOf(
                    mutableListOf(1, 2, 3),
                    mutableListOf(4),
                    mutableListOf(5),
                    mutableListOf(6),
                    mutableListOf(),
                    mutableListOf(),
                    mutableListOf()
                ),
                GraphType.DIRECTED,
                false
            ),
            row(
                listOf(mutableListOf(1), mutableListOf(2), mutableListOf(0, 3), mutableListOf()),
                GraphType.UNDIRECTED,
                true
            ),
            row(

                listOf(mutableListOf(1, 3), mutableListOf(2), mutableListOf(), mutableListOf(4), mutableListOf()),
                GraphType.DIRECTED,
                false
            ),
            row(

                listOf(mutableListOf(1, 4), mutableListOf(2, 3), mutableListOf(), mutableListOf(), mutableListOf(5), mutableListOf()),
                GraphType.DIRECTED,
                false
            ),
            row(
                listOf(
                    mutableListOf(1, 2),
                    mutableListOf(3),
                    mutableListOf(4),
                    mutableListOf(5),
                    mutableListOf(5),
                    mutableListOf()
                ),
                GraphType.DIRECTED,
                false
            ),
            row(
                listOf(
                    mutableListOf(1, 2),
                    mutableListOf(3),
                    mutableListOf(4),
                    mutableListOf(5),
                    mutableListOf(5),
                    mutableListOf()
                ),
                GraphType.DIRECTED,
                false
            ),
            row(
                listOf(
                    mutableListOf(1, 2),
                    mutableListOf(),
                    mutableListOf()
                ),
                GraphType.UNDIRECTED,
                true
            )
        ) { adjacencyList, graphType, hasCycle ->
            val graphAdjacencyList = createGraphWithAdjacencyList(adjacencyList, graphType)
            val graphAdjacencyMatrix = createGraphWithAdjacencyMatrix(adjacencyList, graphType)

            graphAdjacencyList.hasCycleDFS() shouldBe hasCycle
            graphAdjacencyList.hasCycleBFS() shouldBe hasCycle
            graphAdjacencyMatrix.hasCycleDFS() shouldBe hasCycle
            graphAdjacencyMatrix.hasCycleBFS() shouldBe hasCycle

            // DFS or BFS on weighted graphs is exactly the same as on unweighted graphs,
            // since both algorithms don't use any edge weights.
            val weightedGraphAdjacencyList = createWeightedGraphWithAdjacencyList(addRandomWeights(adjacencyList), graphType)
            val weightedGraphAdjacencyMatrix = createWeightedGraphWithAdjacencyMatrix(addRandomWeights(adjacencyList), graphType)

            weightedGraphAdjacencyList.hasCycleDFS() shouldBe hasCycle
            weightedGraphAdjacencyList.hasCycleBFS() shouldBe hasCycle
            weightedGraphAdjacencyMatrix.hasCycleDFS() shouldBe hasCycle
            weightedGraphAdjacencyMatrix.hasCycleBFS() shouldBe hasCycle
        }
    }
})
