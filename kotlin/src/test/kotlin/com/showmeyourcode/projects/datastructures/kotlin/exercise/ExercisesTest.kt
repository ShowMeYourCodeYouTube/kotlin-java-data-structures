package com.showmeyourcode.projects.datastructures.kotlin.exercise

import io.kotest.assertions.throwables.shouldNotThrow
import io.kotest.core.spec.style.AnnotationSpec

class ExercisesTest : AnnotationSpec() {

    @Test
    fun `should run the main method without errors`() {
        shouldNotThrow<Exception> {
            myTestFunction()
        }
    }
}
