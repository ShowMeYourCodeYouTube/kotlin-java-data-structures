package com.showmeyourcode.projects.datastructures.kotlin.custom

import io.kotest.assertions.throwables.shouldNotThrow
import io.kotest.core.spec.style.AnnotationSpec

class CustomStructuresOverviewTest : AnnotationSpec() {

    @Test
    fun `should run the main method without errors`() {
        shouldNotThrow<Exception> {
            main()
        }
    }
}
