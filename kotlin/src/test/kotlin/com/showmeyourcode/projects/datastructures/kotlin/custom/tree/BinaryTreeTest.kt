package com.showmeyourcode.projects.datastructures.kotlin.custom.tree

import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe

class BinaryTreeTest : StringSpec({

    "should traversal the tree using the in-order algorithm" {
        val binaryTree = BinaryTree<Int>()

        binaryTree.insert(10)
        binaryTree.insert(5)
        binaryTree.insert(15)
        binaryTree.insert(3)
        binaryTree.insert(7)

        binaryTree.inorderTraversal() shouldBe listOf(3, 5, 7, 10, 15)
    }

    "should traversal the tree using the in-order algorithm when deleting and inserting nodes" {
        val binaryTree = BinaryTree<Int>()

        binaryTree.insert(10)
        binaryTree.insert(5)
        binaryTree.insert(15)
        binaryTree.insert(3)
        binaryTree.insert(7)

        binaryTree.delete(5)

        binaryTree.inorderTraversal() shouldBe listOf(3, 7, 10, 15)
    }

    "should find relevant values" {
        val binaryTree = BinaryTree<Int>()

        binaryTree.insert(10)
        binaryTree.insert(5)
        binaryTree.insert(15)
        binaryTree.insert(3)
        binaryTree.insert(7)
        binaryTree.insert(1)
        binaryTree.insert(2)
        binaryTree.insert(0)
        binaryTree.insert(5)
        binaryTree.insert(5)
        binaryTree.insert(5)

        binaryTree.find(12345) shouldBe false
        binaryTree.find(45) shouldBe false
        binaryTree.find(-5) shouldBe false
        binaryTree.find(66) shouldBe false
        binaryTree.find(77) shouldBe false

        binaryTree.find(0) shouldBe true
        binaryTree.find(1) shouldBe true
        binaryTree.find(2) shouldBe true
        binaryTree.find(3) shouldBe true
        binaryTree.find(5) shouldBe true
        binaryTree.find(7) shouldBe true
        binaryTree.find(10) shouldBe true
        binaryTree.find(15) shouldBe true
    }

    "should delete a node from the Ternary Tree" {
        val binaryTree = TernaryTree<Int>()

        binaryTree.insert(10)
        binaryTree.insert(5)
        binaryTree.insert(15)
        binaryTree.insert(3)
        binaryTree.insert(7)
        binaryTree.insert(1)
        binaryTree.insert(2)
        binaryTree.insert(0)
        binaryTree.insert(5)
        binaryTree.insert(5)
        binaryTree.insert(5)

        val inOrderResult = binaryTree.inOrderTraversal()
        inOrderResult shouldBe listOf(0, 1, 2, 3, 5, 5, 5, 5, 7, 10, 15)

        binaryTree.delete(5)
        binaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 5, 5, 5, 7, 10, 15)

        binaryTree.delete(5)
        binaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 5, 5, 7, 10, 15)

        binaryTree.delete(5)
        binaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 5, 7, 10, 15)

        binaryTree.delete(5)
        binaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 7, 10, 15)

        binaryTree.delete(5)
        binaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 7, 10, 15)

        binaryTree.delete(10)
        binaryTree.inOrderTraversal() shouldBe listOf(0, 1, 2, 3, 7, 15)

        binaryTree.delete(0)
        binaryTree.inOrderTraversal() shouldBe listOf(1, 2, 3, 7, 15)
    }

    "should check if the tree is complete" {
        val binaryTree = BinaryTree<Int>()

        binaryTree.isComplete() shouldBe true

        binaryTree.insert(10)
        binaryTree.insert(5)
        binaryTree.insert(15)
        binaryTree.insert(3)
        binaryTree.insert(7)

        binaryTree.isComplete() shouldBe true
    }

    "should check if the tree is balanced" {
        val binaryTree = BinaryTree<Int>()

        binaryTree.isBalanced() shouldBe true

        binaryTree.insert(10)
        binaryTree.isBalanced() shouldBe true

        binaryTree.insert(5)
        binaryTree.isBalanced() shouldBe true

        binaryTree.insert(4)
        binaryTree.isBalanced() shouldBe false

        binaryTree.insert(3)
        binaryTree.isBalanced() shouldBe false

        binaryTree.delete(3)
        binaryTree.delete(4)
        binaryTree.isBalanced() shouldBe true

        binaryTree.insert(31)
        binaryTree.insert(41)
        binaryTree.insert(51)
        binaryTree.isBalanced() shouldBe false

        binaryTree.delete(31)
        binaryTree.delete(41)
        binaryTree.delete(51)
        binaryTree.isBalanced() shouldBe true

        binaryTree.insert(15)
        binaryTree.isBalanced() shouldBe true

        binaryTree.insert(3)
        binaryTree.isBalanced() shouldBe true

        binaryTree.insert(7)
        binaryTree.isBalanced() shouldBe true
    }

    "should check if the tree is perfect" {
        val binaryTree = BinaryTree<Int>()

        binaryTree.isPerfect() shouldBe true

        binaryTree.insert(10)
        binaryTree.insert(5)
        binaryTree.insert(15)
        binaryTree.insert(3)
        binaryTree.insert(7)

        binaryTree.isPerfect() shouldBe false
    }

    "should check if the tree is full" {
        val binaryTree = BinaryTree<Int>()

        binaryTree.isFull() shouldBe true

        binaryTree.insert(10)
        binaryTree.insert(5)
        binaryTree.insert(15)
        binaryTree.insert(3)
        binaryTree.insert(7)

        binaryTree.isFull() shouldBe true
    }

    "should check if the tree is degenerate" {
        val binaryTree = BinaryTree<Int>()

        binaryTree.isDegenerate() shouldBe true

        binaryTree.insert(10)
        binaryTree.insert(5)
        binaryTree.insert(15)
        binaryTree.insert(3)
        binaryTree.insert(7)
        binaryTree.isDegenerate() shouldBe false
    }
})
