package com.showmeyourcode.projects.datastructures.kotlin.custom.heap

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.AnnotationSpec
import io.kotest.data.blocking.forAll
import io.kotest.data.row
import io.kotest.matchers.collections.shouldContainExactly
import io.kotest.matchers.shouldBe
import io.kotest.matchers.throwable.shouldHaveMessage

class KHeapTest : AnnotationSpec() {

    @Test
    fun `should insert elements and maintain heap property`() {
        forAll(
            row(3, listOf(5, 3, 8, 1, 6, 2), listOf(1, 2, 8, 3, 6, 5)),
            row(4, listOf(10, 4, 3, 8, 2), listOf(2, 10, 4, 8, 3)),
            row(2, listOf(6, 7, 4, 5), listOf(4, 5, 6, 7))
        ) { k, input, expected ->
            val heap = KHeap(k)
            input.forEach { heap.insert(it) }
            heap.toList() shouldContainExactly expected
        }
    }

    @Test
    fun `should extract the minimum element and maintain heap property`() {
        forAll(
            row(3, listOf(5, 3, 8, 1, 6, 2), listOf(1, 2, 3, 5, 6, 8), 1, listOf(2, 5, 8, 3, 6)),
            row(4, listOf(10, 4, 3, 8, 2), listOf(2, 3, 4, 8, 10), 2, listOf(3, 10, 4, 8)),
            row(2, listOf(6, 7, 4, 5), listOf(4, 5, 6, 7), 4, listOf(5, 7, 6))
        ) { k, input, expectedSorted, minElement, expectedHeap ->
            val heap = KHeap(k)
            input.forEach { heap.insert(it) }
            heap.toList().sorted() shouldContainExactly expectedSorted
            heap.extractMin() shouldBe minElement
            heap.toList() shouldContainExactly expectedHeap
        }
    }

    @Test
    fun `should return the minimum element without removing it`() {
        forAll(
            row(3, listOf(5, 3, 8, 1), 1, listOf(1, 5, 8, 3)),
            row(4, listOf(10, 4, 3, 8, 2), 2, listOf(2, 10, 4, 8, 3)),
            row(2, listOf(6, 7, 4, 5), 4, listOf(4, 5, 6, 7))
        ) { k, input, minElement, expected ->
            val heap = KHeap(k)
            input.forEach { heap.insert(it) }
            heap.peek() shouldBe minElement
            heap.toList() shouldContainExactly expected
        }
    }

    @Test
    fun `should return the correct size of the heap`() {
        forAll(
            row(3, listOf(5, 3, 8), 3),
            row(4, listOf(10, 4, 3, 8, 2), 5),
            row(2, listOf(6, 7, 4, 5), 4)
        ) { k, input, expectedSize ->
            val heap = KHeap(k)
            input.forEach { heap.insert(it) }
            heap.size() shouldBe expectedSize
        }
    }

    @Test
    fun `should throw NoSuchElementException when extracting from an empty heap`() {
        forAll(
            row(3, "Heap is empty"),
            row(4, "Heap is empty"),
            row(2, "Heap is empty")
        ) { k, errorMessage ->
            val heap = KHeap(k)
            val exception = shouldThrow<NoSuchElementException> {
                heap.extractMin()
            }
            exception.shouldHaveMessage(errorMessage)
        }
    }

    @Test
    fun `should throw NoSuchElementException when peeking an empty heap`() {
        forAll(
            row(3, "Heap is empty"),
            row(4, "Heap is empty"),
            row(2, "Heap is empty")
        ) { k, errorMessage ->
            val heap = KHeap(k)
            val exception = shouldThrow<NoSuchElementException> {
                heap.peek()
            }
            exception.shouldHaveMessage(errorMessage)
        }
    }

    @Test
    fun `should handle multiple inserts and extracts correctly`() {
        forAll(
            row(3, listOf(10, 20, 15, 30, 25, 5), 5, listOf(10, 20, 15, 30, 25)),
            row(4, listOf(10, 40, 30, 20, 50), 10, listOf(20, 40, 30, 50)),
            row(2, listOf(6, 7, 4, 5), 4, listOf(5, 7, 6))
        ) { k, input, minElement, expectedHeap ->
            val heap = KHeap(k)
            input.forEach { heap.insert(it) }
            heap.extractMin() shouldBe minElement
            heap.toList() shouldContainExactly expectedHeap
        }
    }
}
