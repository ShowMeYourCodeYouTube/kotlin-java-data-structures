package com.showmeyourcode.projects.datastructures.kotlin.custom.tree

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import java.util.*

class AVLTreeNode<T : Comparable<T>>(var value: T) {
    var left: AVLTreeNode<T>? = null
    var right: AVLTreeNode<T>? = null
    var height: Int = 1
}

class AVLTree<T : Comparable<T>> {
    private var root: AVLTreeNode<T>? = null

    private fun height(node: AVLTreeNode<T>?): Int {
        return node?.height ?: 0
    }

    private fun updateHeight(node: AVLTreeNode<T>?) {
        node?.height = 1 + maxOf(height(node?.left), height(node?.right))
    }

    private fun balanceFactor(node: AVLTreeNode<T>?): Int {
        return height(node?.left) - height(node?.right)
    }

    private fun leftRotate(y: AVLTreeNode<T>): AVLTreeNode<T> {
        val x = y.right
        val t2 = x?.left

        // Perform rotation
        x?.left = y
        y.right = t2

        // Update heights
        updateHeight(y)
        updateHeight(x)

        return x ?: y
    }

    private fun rightRotate(x: AVLTreeNode<T>): AVLTreeNode<T> {
        val y = x.left
        val t2 = y?.right

        // Perform rotation
        y?.right = x
        x.left = t2

        // Update heights
        updateHeight(x)
        updateHeight(y)

        return y ?: x
    }

    fun insert(value: T) {
        root = insertRecursive(root, value)
    }

    private fun insertRecursive(node: AVLTreeNode<T>?, value: T): AVLTreeNode<T> {
        if (node == null) {
            return AVLTreeNode(value)
        }

        when {
            value < node.value -> node.left = insertRecursive(node.left, value)
            value > node.value -> node.right = insertRecursive(node.right, value)
            else -> return node // Duplicate values are not allowed
        }

        // Update height
        updateHeight(node)

        // Get the balance factor to check for rotation
        val balance = balanceFactor(node)

        // Left Left Case
        if (balance > 1 && value < node.left?.value ?: value) {
            return rightRotate(node)
        }

        // Right Right Case
        if (balance < -1 && value > node.right?.value ?: value) {
            return leftRotate(node)
        }

        // Left Right Case
        if (balance > 1 && value > node.left?.value ?: value) {
            node.left = leftRotate(node.left ?: return node)
            return rightRotate(node)
        }

        // Right Left Case
        if (balance < -1 && value < node.right?.value ?: value) {
            node.right = rightRotate(node.right ?: return node)
            return leftRotate(node)
        }

        return node
    }

    fun delete(value: T) {
        root = deleteRecursive(root, value)
    }

    private fun deleteRecursive(node: AVLTreeNode<T>?, value: T): AVLTreeNode<T>? {
        if (node == null) {
            return null
        }

        when {
            value < node.value -> node.left = deleteRecursive(node.left, value)
            value > node.value -> node.right = deleteRecursive(node.right, value)
            else -> {
                // Node with only one child or no child
                if (node.left == null || node.right == null) {
                    return node.left ?: node.right
                }

                // Node with two children, get the inorder successor (smallest
                // in the right subtree)
                val successor = minValueNode(node.right ?: return null)

                // Copy the inorder successor's data to this node
                node.value = successor.value

                // Delete the inorder successor
                node.right = deleteRecursive(node.right, successor.value)
            }
        }

        // Update height
        updateHeight(node)

        // Get the balance factor to check for rotation
        val balance = balanceFactor(node)

        // Left Left Case
        if (balance > 1 && balanceFactor(node.left) >= 0) {
            return rightRotate(node)
        }

        // Left Right Case
        if (balance > 1 && balanceFactor(node.left) < 0) {
            node.left = leftRotate(node.left ?: return node)
            return rightRotate(node)
        }

        // Right Right Case
        if (balance < -1 && balanceFactor(node.right) <= 0) {
            return leftRotate(node)
        }

        // Right Left Case
        if (balance < -1 && balanceFactor(node.right) > 0) {
            node.right = rightRotate(node.right ?: return node)
            return leftRotate(node)
        }

        return node
    }

    private fun minValueNode(node: AVLTreeNode<T>): AVLTreeNode<T> {
        var current = node
        while (current.left != null) {
            current = current.left ?: break
        }
        return current
    }

    fun inOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        inOrderRecursive(root, result)
        return result
    }

    private fun inOrderRecursive(node: AVLTreeNode<T>?, result: MutableList<T>) {
        if (node != null) {
            inOrderRecursive(node.left, result)
            result.add(node.value)
            inOrderRecursive(node.right, result)
        }
    }

    fun preOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        preOrderRecursive(root, result)
        return result
    }

    private fun preOrderRecursive(node: AVLTreeNode<T>?, result: MutableList<T>) {
        if (node != null) {
            result.add(node.value)
            preOrderRecursive(node.left, result)
            preOrderRecursive(node.right, result)
        }
    }

    fun postOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        postOrderRecursive(root, result)
        return result
    }

    private fun postOrderRecursive(node: AVLTreeNode<T>?, result: MutableList<T>) {
        if (node != null) {
            postOrderRecursive(node.left, result)
            postOrderRecursive(node.right, result)
            result.add(node.value)
        }
    }

    fun levelOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        val queue: Queue<AVLTreeNode<T>> = LinkedList()

        root?.let { queue.add(it) }

        while (queue.isNotEmpty()) {
            val current = queue.poll()
            result.add(current.value)

            current.left?.let { queue.add(it) }
            current.right?.let { queue.add(it) }
        }

        return result
    }
}

object AVLTreeOverview {

    fun main() {
        val avlTree = AVLTree<Int>()

        // Example: Insert nodes into the AVL tree
        avlTree.insert(10)
        avlTree.insert(5)
        avlTree.insert(15)
        avlTree.insert(3)
        avlTree.insert(7)

        // Example: Perform in-order traversal
        val inOrderResult = avlTree.inOrderTraversal()
        LOGGER.info("In-order traversal: $inOrderResult")

        // Example: Delete a node
        avlTree.delete(5)
        LOGGER.info("\nIn-order traversal after deleting 5: ${avlTree.inOrderTraversal()}")

        // Example: Perform traversals
        LOGGER.info("In-order traversal: ${avlTree.inOrderTraversal()}")
        LOGGER.info("Pre-order traversal: ${avlTree.preOrderTraversal()}")
        LOGGER.info("Post-order traversal: ${avlTree.postOrderTraversal()}")
        LOGGER.info("Level-order traversal: ${avlTree.levelOrderTraversal()}")
    }
}
