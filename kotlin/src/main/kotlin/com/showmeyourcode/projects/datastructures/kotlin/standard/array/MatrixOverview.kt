package com.showmeyourcode.projects.datastructures.kotlin.standard.array

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER

object MatrixOverview {

    fun main() {
        LOGGER.info(
            """
            A matrix is a two-dimensional arrangement of numbers or values arranged in rows and columns. 
            
            """.trimIndent()
        )
        matrix()
    }

    private fun matrix() {
        val matrix = arrayOf(
            arrayOf(0, 0, 0, 0, 1),
            arrayOf(0, 0, 0, 1, 1),
            arrayOf(0, 0, 1, 1, 1),
            arrayOf(0, 0, 0, 1, 1),
            arrayOf(0, 0, 0, 0, 1)
        )
        printMatrix(matrix)
    }

    // It's not possible use Array<Array<Any>> instead of Array<Array<Int>>
    // Arrays in Kotlin are invariant (the same in Java),
    // which means that an array of a specific type cannot be assigned to an array of its parent type.
    // This provides implicit type safety and prevents possible runtime errors in the application.
    // Read more about it here: https://kotlinlang.org/docs/generics.html#variance
    //
    // What's a generic class?
    // Generic classes encapsulate operations that are not specific to a particular data type.
    // The most common use for generic classes is with collections like linked lists, hash tables, stacks, queues, trees, and so on.
    //
    // Ref:
    // https://stackoverflow.com/questions/75769726/kotlin-not-accepting-string-as-any
    // https://stackoverflow.com/questions/36569421/kotlin-how-to-work-with-list-casts-unchecked-cast-kotlin-collections-listkot
    // https://www.oreilly.com/library/view/kotlin-for-enterprise/9781788997270/0357cf7d-f001-4735-b73a-d2ca2554e03d.xhtml
    private fun printMatrix(matrix: Array<Array<Int>>) {
        for (array in matrix) {
            for (value in array) {
                print("$value ")
            }
            LOGGER.info("")
        }
    }
}
