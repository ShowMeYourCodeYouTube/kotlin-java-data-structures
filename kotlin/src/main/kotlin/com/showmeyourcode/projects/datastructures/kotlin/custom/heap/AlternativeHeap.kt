package com.showmeyourcode.projects.datastructures.kotlin.custom.heap

/**
 * This implementation by default inserts the first element as MIN/MAX which should be omitted when calculating/returning values.
 * For example, the peek method returns array[1] because the first element is skipped.
 *
 * Reference: https://gist.github.com/nwillc/f54b50eb1ea53658293178b457a1fda5
 */
class AlternativeHeap private constructor(
    size: Int,
    seed: Int,
    val compare: (Int, Int) -> Boolean
) : Heap {
    private var heapSize = 0
    private val array = IntArray(size + 1).also { it[0] = seed }

    companion object {
        private const val FRONT = 1
        private const val MAX_SIZE_ERROR = "At max size"

        fun minHeap(size: Int = 10): Heap = AlternativeHeap(size, Int.MIN_VALUE) { a, b -> a < b }
        fun maxHeap(size: Int = 10): Heap = AlternativeHeap(size, Int.MAX_VALUE) { a, b -> a > b }

        private fun parent(i: Int) = i shr 1
        private fun left(i: Int) = i shl 1
        private fun right(i: Int) = left(i) + 1
    }

    override fun pop(): Int {
        require(heapSize > 0) { throw NoSuchElementException(Heap.EMPTY_HEAP_ERROR) }

        val popped = peek()
        array[FRONT] = array[heapSize--]
        heapify(FRONT)
        return popped
    }

    override fun peek() = if (heapSize > 0) array[1] else throw NoSuchElementException(Heap.EMPTY_HEAP_ERROR)

    override fun size() = heapSize

    override fun toList(): List<Int> = array.drop(1).take(size())

    override operator fun plusAssign(value: Int) {
        require(heapSize < array.size - 1) { error(MAX_SIZE_ERROR) }

        array[++heapSize] = value
        var current = heapSize
        var parent = parent(current)
        while (parent != 0 && compare(array[current], array[parent])) {
            swap(parent, current)
            current = parent.also { parent = parent(parent) }
        }
    }

    private fun swap(i: Int, j: Int) {
        array[i] = array[j].also { array[j] = array[i] }
    }

    private tailrec fun heapify(i: Int) {
        val left = left(i)
        val right = right(i)

        var ordered = when {
            left <= heapSize && compare(array[i], array[left]) -> i
            left <= heapSize -> left
            else -> i
        }

        if (right <= heapSize && !compare(array[ordered], array[right])) {
            ordered = right
        }

        if (ordered != i) {
            swap(ordered, i)
            heapify(ordered)
        }
    }

    override fun toString() = toList().joinToString(", ", "[", "]")
}
