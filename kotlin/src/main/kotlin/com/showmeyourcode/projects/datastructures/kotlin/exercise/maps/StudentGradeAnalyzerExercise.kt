package com.showmeyourcode.projects.datastructures.kotlin.exercise.maps

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging

data class StudentGrade(val studentId: String, val grade: Double)

/**
 * Write a Kotlin function analyzeStudentGrades that analyzes
 * a list of student grades represented as StudentGrade objects.
 * <br>
 * Each StudentGrade has a studentId and a grade.
 * The function should calculate and return a map where keys are student IDs (String) and values are the average grade (Double) for each student.
 */
fun analyzeStudentGrades(grades: List<StudentGrade>): Map<String, Double> {
    val gradeSum = mutableMapOf<String, Pair<Int, Double>>()
    for (grade in grades) {
        val current = gradeSum.getOrDefault(grade.studentId, Pair(0, 0.0))
        gradeSum[grade.studentId] = Pair(current.first + 1, current.second + grade.grade)
    }
    return gradeSum.mapValues {
        it.value.second / it.value.first
    }
}

fun <K, V> print(map: Map<K, V>) {
    map.forEach { (k, v) ->
        Logging.LOGGER.info("$k: $v")
    }
}
