package com.showmeyourcode.projects.datastructures.kotlin.custom.heap

/**
 * By default, Heap is considered as Binary-Heap, but a heap can be k-ary Heap also, but with k children, it follows the same properties as binary heap does.
 *
 */
class DefaultHeap(private val comparator: Comparator<Int>) : Heap {
    private val heapArray = mutableListOf<Int>()

    companion object {
        fun minHeap(): Heap = DefaultHeap(Comparator.reverseOrder())
        fun maxHeap(): Heap = DefaultHeap(Comparator.naturalOrder())
    }

    override fun pop(): Int {
        if (isEmpty()) {
            throw NoSuchElementException(Heap.EMPTY_HEAP_ERROR)
        }

        val root = heapArray[0]
        val last = heapArray.removeAt(heapArray.size - 1)

        if (heapArray.isNotEmpty()) {
            heapArray[0] = last
            heapifyDown(0)
        }

        return root
    }

    override fun peek(): Int {
        if (isEmpty()) {
            throw NoSuchElementException(Heap.EMPTY_HEAP_ERROR)
        }
        return heapArray[0]
    }

    override fun size(): Int {
        return heapArray.size
    }

    override fun toList(): List<Int> {
        return heapArray.toList()
    }

    override fun plusAssign(value: Int) {
        heapArray.add(value)
        heapifyUp(heapArray.size - 1)
    }

    private fun heapifyUp(index: Int) {
        var currentIndex = index
        while (currentIndex > 0) {
            val parentIndex = (currentIndex - 1) / 2
            if (comparator.compare(heapArray[currentIndex], heapArray[parentIndex]) > 0) {
                swap(currentIndex, parentIndex)
                currentIndex = parentIndex
            } else {
                break
            }
        }
    }

    private fun heapifyDown(index: Int) {
        var currentIndex = index
        while (true) {
            val leftChild = 2 * currentIndex + 1
            val rightChild = 2 * currentIndex + 2
            var rootIndex = currentIndex

            if (leftChild < heapArray.size && comparator.compare(heapArray[leftChild], heapArray[rootIndex]) > 0) {
                rootIndex = leftChild
            }

            if (rightChild < heapArray.size && comparator.compare(heapArray[rightChild], heapArray[rootIndex]) > 0) {
                rootIndex = rightChild
            }

            if (rootIndex != currentIndex) {
                swap(currentIndex, rootIndex)
                currentIndex = rootIndex
            } else {
                break
            }
        }
    }

    private fun swap(index1: Int, index2: Int) {
        val temp = heapArray[index1]
        heapArray[index1] = heapArray[index2]
        heapArray[index2] = temp
    }

    private fun isEmpty(): Boolean {
        return heapArray.isEmpty()
    }
}
