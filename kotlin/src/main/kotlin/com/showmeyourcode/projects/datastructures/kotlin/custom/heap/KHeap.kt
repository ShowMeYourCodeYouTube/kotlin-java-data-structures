package com.showmeyourcode.projects.datastructures.kotlin.custom.heap

/**
 * By default, Heap is considered as Binary-Heap, but a heap can be k-ary Heap also, but with k children, it follows the same properties as binary heap does.
 * This is min heap implementation.
 * <br>
 * Reference: https://www.geeksforgeeks.org/k-ary-heap/
 */
class KHeap(private val k: Int) {
    private val heap = mutableListOf<Int>()

    fun insert(value: Int) {
        heap.add(value)
        siftUp(heap.size - 1)
    }

    fun extractMin(): Int {
        if (heap.isEmpty()) throw NoSuchElementException("Heap is empty")
        val min = heap.first()
        val last = heap.removeAt(heap.size - 1)
        if (heap.isNotEmpty()) {
            heap[0] = last
            siftDown(0)
        }
        return min
    }

    fun peek(): Int {
        if (heap.isEmpty()) throw NoSuchElementException("Heap is empty")
        return heap.first()
    }

    fun size(): Int = heap.size

    fun toList(): List<Int> = heap.toList()

    private fun siftUp(index: Int) {
        var currentIndex = index
        while (currentIndex > 0) {
            val parentIndex = (currentIndex - 1) / k
            if (heap[currentIndex] < heap[parentIndex]) {
                heap.swap(currentIndex, parentIndex)
                currentIndex = parentIndex
            } else {
                break
            }
        }
    }

    private fun siftDown(index: Int) {
        var currentIndex = index
        while (true) {
            val childrenIndices = (1..k).map { k * currentIndex + it }.filter { it < heap.size }
            if (childrenIndices.isEmpty()) break
            val minChildIndex = childrenIndices.minByOrNull { heap[it] } ?: break
            if (heap[currentIndex] > heap[minChildIndex]) {
                heap.swap(currentIndex, minChildIndex)
                currentIndex = minChildIndex
            } else {
                break
            }
        }
    }

    private fun MutableList<Int>.swap(i: Int, j: Int) {
        val temp = this[i]
        this[i] = this[j]
        this[j] = temp
    }
}
