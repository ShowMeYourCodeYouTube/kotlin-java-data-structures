package com.showmeyourcode.projects.datastructures.kotlin.exercise.lists

/**
 * Generate Pascal triangle for given rows.
 */
fun generatePascalTriangle(rows: Int): List<List<Int>> {
    val triangle = mutableListOf<List<Int>>()

    for (row in 0 until rows) {
        val newRow = mutableListOf<Int>()
        for (col in 0..row) {
            if (col == 0 || col == row) {
                newRow.add(1)
            } else {
                val aboveLeft = triangle[row - 1][col - 1]
                val aboveRight = triangle[row - 1][col]
                newRow.add(aboveLeft + aboveRight)
            }
        }
        triangle.add(newRow)
    }

    return triangle
}
