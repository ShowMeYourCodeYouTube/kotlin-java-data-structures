package com.showmeyourcode.projects.datastructures.kotlin.standard.array

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER

/**
 * Reference: https://www.baeldung.com/kotlin/multidimentional-arrays
 */
object MultidimensionalArrayOverview {

    fun main() {
        LOGGER.info(
            """
            A multi-dimensional array is an array with more than one level or dimension. 
            For example, a 2D array, or two-dimensional array, is an array of arrays, meaning it is a matrix of rows and columns (think of a table). 
            A 3D array adds another dimension, turning it into an array of arrays of arrays.
            
            """.trimIndent()
        )
        multidimensionalArray()
    }

    private fun multidimensionalArray() {
        val matrix = arrayOf(
            arrayOf(
                arrayOf(0, 0, 0, 0, 0, 0),
                arrayOf(0, 0, 0, 0, 0, 0)
            ),
            arrayOf(
                arrayOf(1, 1, 1, 1, 1, 1),
                arrayOf(1, 1, 1, 1, 1, 1)
            ),
            arrayOf(
                arrayOf(2, 2, 2, 2, 2, 2),
                arrayOf(2, 2, 2, 2, 2, 2)
            )
        )
        printArray(matrix)
    }

    private fun printArray(array: Array<Array<Array<Int>>>) {
        for (nestedArray1 in array) {
            for (nestedArray2 in nestedArray1) {
                for (value in nestedArray2) {
                    print("$value ")
                }
                LOGGER.info("")
            }
        }
    }
}
