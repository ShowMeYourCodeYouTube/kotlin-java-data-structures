package com.showmeyourcode.projects.datastructures.kotlin.exercise.heaps

import java.util.*

/**
 * You are given a stream of integers, and you need to efficiently find the Kth largest element in the stream at any point.
 * Utilize the heap data structure to solve this problem.
 */
class KthLargestExercise(private val k: Int, nums: IntArray) {
    private val heap = PriorityQueue<Int>()

    init {
        for (num in nums) {
            add(num)
        }
    }

    fun add(value: Int): Int {
        if (heap.size < k) {
            heap.offer(value)
        } else if (value > heap.peek()) {
            heap.poll()
            heap.offer(value)
        }
        return heap.peek()
    }
}
