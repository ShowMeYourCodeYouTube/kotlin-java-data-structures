package com.showmeyourcode.projects.datastructures.kotlin.custom

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging
import com.showmeyourcode.projects.datastructures.kotlin.custom.heap.DefaultHeap

object HeapOverview {

    fun main() {
        Logging.LOGGER.info(
            """
                A heap is a special tree-based data structure in which the tree is a complete binary tree.
            """.trimIndent()
        )
        val maxHeap = DefaultHeap.maxHeap()

        maxHeap += 2
        maxHeap += 12
        Logging.LOGGER.info("$maxHeap - the largest element: ${maxHeap.peek()}")
        maxHeap += 3
        Logging.LOGGER.info("$maxHeap - the largest element: ${maxHeap.peek()}")
        maxHeap += 1
        maxHeap += 123
        Logging.LOGGER.info("$maxHeap - the largest element: ${maxHeap.peek()}")
    }
}
