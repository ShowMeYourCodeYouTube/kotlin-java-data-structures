package com.showmeyourcode.projects.datastructures.kotlin.standard.list

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import com.showmeyourcode.projects.datastructures.kotlin.common.printAll
import com.showmeyourcode.projects.datastructures.kotlin.common.word

object ListOverview {

    fun main() {
        LOGGER.info(
            """
            A List is a data structure that stores elements in an ordered and sequential manner. 
            A list can store repetitive elements which means a single element can occur more than once in a list.
            
            """.trimIndent()
        )
        // List implementations in Kotlin are based on ArrayList.
        immutableList()
        mutableList()
    }

    private fun mutableList() {
        val mutableList = mutableListOf(word)
        mutableList.add("NEXT")

        printAll(mutableList, "Mutable list: ")
        printAll(mutableList.asReversed(), "Reversed list: ")
    }

    private fun immutableList() {
        val immutableList = listOf(word)

        printAll(immutableList, "Immutable list: ")
    }
}
