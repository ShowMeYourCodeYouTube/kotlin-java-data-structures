package com.showmeyourcode.projects.datastructures.kotlin.exercise

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging

// here you can try to solve any of exercises
fun myTestFunction(): Unit = Logging.LOGGER.info("Buum!")
