package com.showmeyourcode.projects.datastructures.kotlin.custom

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import com.showmeyourcode.projects.datastructures.kotlin.custom.graph.*

object GraphOverview {

    fun main() {
        LOGGER.info(
            """
                A graph is a non-linear data structure consisting of vertices and edges.
            """.trimIndent()
        )

        // Example for Adjacency List
        val adjacencyListGraph = Graph(AdjacencyList(6, GraphType.UNDIRECTED))
        adjacencyListGraph.addEdge(0, 1)
        adjacencyListGraph.addEdge(0, 2)
        adjacencyListGraph.addEdge(1, 3)
        adjacencyListGraph.addEdge(1, 4)
        adjacencyListGraph.addEdge(2, 4)
        adjacencyListGraph.addEdge(3, 5)
        adjacencyListGraph.addEdge(4, 5)

        LOGGER.info("Graph Representation using Adjacency List:")
        adjacencyListGraph.toList().forEachIndexed { index, list ->
            LOGGER.info("$index: $list")
        }
        LOGGER.info("\nBFS starting from vertex 0:")
        adjacencyListGraph.bfs(0)
        LOGGER.info("\nDFS starting from vertex 0:")
        adjacencyListGraph.dfs(0)

        // Example for Weighted Adjacency List
        val weightedAdjacencyListGraph = WeightedGraph(WeightedAdjacencyList(6, GraphType.UNDIRECTED))
        weightedAdjacencyListGraph.addEdge(0, 1, 1.5)
        weightedAdjacencyListGraph.addEdge(0, 2, 2.3)
        weightedAdjacencyListGraph.addEdge(1, 3, 0.8)
        weightedAdjacencyListGraph.addEdge(1, 4, 1.2)
        weightedAdjacencyListGraph.addEdge(2, 4, 0.5)
        weightedAdjacencyListGraph.addEdge(3, 5, 3.0)
        weightedAdjacencyListGraph.addEdge(4, 5, 2.2)

        LOGGER.info("\n\nWeighted Graph Representation using Weighted Adjacency List:")
        weightedAdjacencyListGraph.toList().forEachIndexed { index, list ->
            LOGGER.info("$index: $list")
        }
        LOGGER.info("\nBFS starting from vertex 0:")
        weightedAdjacencyListGraph.bfs(0)
        LOGGER.info("\nDFS starting from vertex 0:")
        weightedAdjacencyListGraph.dfs(0)

        // Example for Adjacency Matrix
        val adjacencyMatrixGraph = Graph(AdjacencyMatrix(6, GraphType.UNDIRECTED))
        adjacencyMatrixGraph.addEdge(0, 1)
        adjacencyMatrixGraph.addEdge(0, 2)
        adjacencyMatrixGraph.addEdge(1, 3)
        adjacencyMatrixGraph.addEdge(1, 4)
        adjacencyMatrixGraph.addEdge(2, 4)
        adjacencyMatrixGraph.addEdge(3, 5)
        adjacencyMatrixGraph.addEdge(4, 5)

        LOGGER.info("\n\nGraph Representation using Adjacency Matrix:")
        adjacencyMatrixGraph.toList().forEachIndexed { index, list ->
            LOGGER.info("$index: $list")
        }
        LOGGER.info("\nBFS starting from vertex 0:")
        adjacencyMatrixGraph.bfs(0)
        LOGGER.info("\nDFS starting from vertex 0:")
        adjacencyMatrixGraph.dfs(0)

        // Example for Weighted Adjacency Matrix
        val weightedAdjacencyMatrixGraph = WeightedGraph(WeightedAdjacencyMatrix(6, GraphType.UNDIRECTED))
        weightedAdjacencyMatrixGraph.addEdge(0, 1, 1.5)
        weightedAdjacencyMatrixGraph.addEdge(0, 2, 2.3)
        weightedAdjacencyMatrixGraph.addEdge(1, 3, 0.8)
        weightedAdjacencyMatrixGraph.addEdge(1, 4, 1.2)
        weightedAdjacencyMatrixGraph.addEdge(2, 4, 0.5)
        weightedAdjacencyMatrixGraph.addEdge(3, 5, 3.0)
        weightedAdjacencyMatrixGraph.addEdge(4, 5, 2.2)

        LOGGER.info("\n\nWeighted Graph Representation using Weighted Adjacency Matrix:")
        weightedAdjacencyMatrixGraph.toList().forEachIndexed { index, list ->
            LOGGER.info("$index: $list")
        }
        LOGGER.info("\nBFS starting from vertex 0:")
        weightedAdjacencyMatrixGraph.bfs(0)
        LOGGER.info("\nDFS starting from vertex 0:")
        weightedAdjacencyMatrixGraph.dfs(0)

        // ==================================

        // Example for Directed Adjacency List
        val directedAdjacencyListGraph = Graph(AdjacencyList(6, GraphType.DIRECTED))
        directedAdjacencyListGraph.addEdge(0, 1)
        directedAdjacencyListGraph.addEdge(0, 2)
        directedAdjacencyListGraph.addEdge(1, 3)
        directedAdjacencyListGraph.addEdge(2, 4)
        directedAdjacencyListGraph.addEdge(3, 5)
        directedAdjacencyListGraph.addEdge(4, 5)

        LOGGER.info("Directed Graph Representation using Adjacency List:")
        directedAdjacencyListGraph.toList().forEachIndexed { index, list ->
            LOGGER.info("$index: $list")
        }
        LOGGER.info("\nBFS starting from vertex 0:")
        directedAdjacencyListGraph.bfs(0)
        LOGGER.info("\nDFS starting from vertex 0:")
        directedAdjacencyListGraph.dfs(0)

        // Example for Directed Weighted Adjacency List
        val directedWeightedAdjacencyListGraph = WeightedGraph(WeightedAdjacencyList(6, GraphType.DIRECTED))
        directedWeightedAdjacencyListGraph.addEdge(0, 1, 1.5)
        directedWeightedAdjacencyListGraph.addEdge(0, 2, 2.3)
        directedWeightedAdjacencyListGraph.addEdge(1, 3, 0.8)
        directedWeightedAdjacencyListGraph.addEdge(2, 4, 0.5)
        directedWeightedAdjacencyListGraph.addEdge(3, 5, 3.0)
        directedWeightedAdjacencyListGraph.addEdge(4, 5, 2.2)

        LOGGER.info("\n\nDirected Weighted Graph Representation using Weighted Adjacency List:")
        directedWeightedAdjacencyListGraph.toList().forEachIndexed { index, list ->
            LOGGER.info("$index: $list")
        }
        LOGGER.info("\nBFS starting from vertex 0:")
        directedWeightedAdjacencyListGraph.bfs(0)
        LOGGER.info("\nDFS starting from vertex 0:")
        directedWeightedAdjacencyListGraph.dfs(0)

        // Example for Directed Weighted Adjacency Matrix
        val directedWeightedAdjacencyMatrixGraph = WeightedGraph(WeightedAdjacencyMatrix(6, GraphType.DIRECTED))
        directedWeightedAdjacencyMatrixGraph.addEdge(0, 1, 1.5)
        directedWeightedAdjacencyMatrixGraph.addEdge(0, 2, 2.3)
        directedWeightedAdjacencyMatrixGraph.addEdge(1, 3, 0.8)
        directedWeightedAdjacencyMatrixGraph.addEdge(2, 4, 0.5)
        directedWeightedAdjacencyMatrixGraph.addEdge(3, 5, 3.0)
        directedWeightedAdjacencyMatrixGraph.addEdge(4, 5, 2.2)

        LOGGER.info("\n\nDirected Weighted Graph Representation using Weighted Adjacency Matrix:")
        directedWeightedAdjacencyMatrixGraph.toList().forEachIndexed { index, list ->
            LOGGER.info("$index: $list")
        }
        LOGGER.info("\nBFS starting from vertex 0:")
        directedWeightedAdjacencyMatrixGraph.bfs(0)
        LOGGER.info("\nDFS starting from vertex 0:")
        directedWeightedAdjacencyMatrixGraph.dfs(0)

        LOGGER.info("\nDetecting a cycle in an undirected graph with:")
        val cyclicGraphDfs = Graph(AdjacencyList(6, GraphType.UNDIRECTED))
        adjacencyListGraph.toList().forEachIndexed { index, list ->
            LOGGER.info("$index: $list")
        }
        cyclicGraphDfs.addEdge(0, 1)
        cyclicGraphDfs.addEdge(0, 2)
        cyclicGraphDfs.addEdge(1, 3)
        cyclicGraphDfs.addEdge(2, 4)
        cyclicGraphDfs.addEdge(3, 5)
        cyclicGraphDfs.addEdge(4, 5)

        val hasCycleDfs = cyclicGraphDfs.hasCycleDFS()
        LOGGER.info("Graph has cycle (DFS): $hasCycleDfs")

        LOGGER.info("\n\nFinding a minimum spanning tree")
        val graphMinimumSpanningTree = WeightedGraph(WeightedAdjacencyMatrix(5, GraphType.UNDIRECTED))

        // Add weighted edges
        graphMinimumSpanningTree.addEdge(0, 1, 2.0)
        graphMinimumSpanningTree.addEdge(0, 3, 1.0)
        graphMinimumSpanningTree.addEdge(1, 2, 3.0)
        graphMinimumSpanningTree.addEdge(1, 3, 3.0)
        graphMinimumSpanningTree.addEdge(2, 4, 2.0)
        graphMinimumSpanningTree.addEdge(3, 4, 1.0)

        // Find the Minimum Spanning Tree (MST) using Prim's algorithm
        val primMST = graphMinimumSpanningTree.primMST()
        val kruskalMST = graphMinimumSpanningTree.kruskalMST()

        // Print the edges of the MST
        LOGGER.info("Prim's MST Edges:")
        for ((vertex1, vertex2) in primMST) {
            LOGGER.info("Edge: $vertex1 - $vertex2")
        }

        LOGGER.info("\nKruskal's MST Edges:")
        for ((vertex1, vertex2) in kruskalMST) {
            LOGGER.info("Edge: $vertex1 - $vertex2")
        }

        LOGGER.info("\n\nFinding the shortest path using Dijkstra's algorithm.")
        // Create a weighted graph using an adjacency list
        val dijkstra = WeightedAdjacencyList(6, GraphType.UNDIRECTED)

        // Add weighted edges
        dijkstra.addEdge(0, 1, 2.0)
        dijkstra.addEdge(0, 2, 4.0)
        dijkstra.addEdge(1, 2, 1.0)
        dijkstra.addEdge(1, 3, 7.0)
        dijkstra.addEdge(2, 4, 3.0)
        dijkstra.addEdge(3, 4, 1.0)
        dijkstra.addEdge(3, 5, 5.0)
        dijkstra.addEdge(4, 5, 2.0)

        // Create a WeightedGraph instance
        val weightedGraph = WeightedGraph(dijkstra)

        // Find the shortest distances from vertex 0 using Dijkstra's algorithm
        val dijkstraDistances = weightedGraph.dijkstra(0)

        // Print the shortest distances
        LOGGER.info("Shortest Distances from Vertex 0:")
        for (vertex in dijkstraDistances.indices) {
            LOGGER.info("To vertex $vertex: ${dijkstraDistances[vertex]}")
        }
    }
}
