package com.showmeyourcode.projects.datastructures.kotlin.standard.queue

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import kotlin.collections.ArrayDeque

object QueueOverview {

    fun main() {
        LOGGER.info(
            """
            A Queue is defined as a linear data structure that is open at both ends 
            and the operations are performed in First In First Out (FIFO) order. 
            
            """.trimIndent()
        )
        kotlinQueue()
    }

    /**
     * Resizable-array implementation of the deque data structure.
     * The name deque is short for "double ended queue" and is usually pronounced "deck".
     * The collection provide methods for convenient access to the both ends.
     *
     * It also implements MutableList interface and supports efficient get/set operations by index.
     *
     * It can be used as Stack or Queue.
     *
     * Ref: https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-array-deque/
     */
    private fun kotlinQueue() {
        val deck = ArrayDeque<Int>()
        deck.add(1)
        deck.add(2)
        deck.add(3)
        LOGGER.info(deck.joinToString(prefix = "[", postfix = "]"))

        deck.removeFirst()
        LOGGER.info("Remove the fist element (FIFO): ${deck.joinToString(prefix = "[", postfix = "]")}")
    }
}
