package com.showmeyourcode.projects.datastructures.kotlin.custom.tree

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER

object TreesOverview {

    fun main() {
        LOGGER.info(
            """
                A tree is a collection of nodes connected by edges (branches), and it represents a hierarchical graphic form.
                A binary tree is defined as a Tree data structure with at most 2 children.
            """.trimIndent()
        )
        val binaryTree = BinaryTree<Int>()

        // Building a sample binary tree
        binaryTree.root = TreeNode(1)
        binaryTree.root!!.left = TreeNode(2)
        binaryTree.root!!.right = TreeNode(3)
        binaryTree.root!!.left!!.left = TreeNode(4)
        binaryTree.root!!.left!!.right = TreeNode(5)

        LOGGER.info("\n>>> Binary Tree:")
        binaryTree.printTree()

        LOGGER.info("\nInorder Traversal: ${binaryTree.inorderTraversal()}")
        LOGGER.info("Preorder Traversal: ${binaryTree.preorderTraversal()}")
        LOGGER.info("Postorder Traversal: ${binaryTree.postorderTraversal()}")
        LOGGER.info("Level Order Traversal: ${binaryTree.levelOrderTraversal()}")

        LOGGER.info("\nIs the tree degenerate? ${binaryTree.isDegenerate()}")
        LOGGER.info("Is the tree complete? ${binaryTree.isComplete()}")
        LOGGER.info("Is the tree balanced? ${binaryTree.isBalanced()}")
        LOGGER.info("Is the tree perfect? ${binaryTree.isPerfect()}")
        LOGGER.info("Is the tree full? ${binaryTree.isFull()}")

        TernaryTreeOverview.main()

        AVLTreeOverview.main()

        RedBlackTreeOverview.main()
    }
}
