package com.showmeyourcode.projects.datastructures.kotlin.custom.list

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import java.security.SecureRandom

class SkipListNode(val value: Int, val level: Int) {
    // Array to store forward pointers at each level
    val forward = arrayOfNulls<SkipListNode>(level + 1)
}

sealed interface SkipList {

    fun insert(element: Int): Unit

    fun delete(element: Int): Unit

    fun contains(element: Int): Boolean

    fun size(): Int

    fun getElementsAsList(): List<Int>

    fun print(): Unit
}

class DefaultSkipList(private val maxLevel: Int) : SkipList {

    private val head = SkipListNode(createSentinel(), maxLevel)
    private val random = SecureRandom()
    private var size = 0

    override fun size(): Int = size

    override fun contains(element: Int): Boolean {
        var current = head

        // Traverse from top to bottom
        for (level in maxLevel downTo 0) {
            // Move forward while the next node's value is less than the target
            while (current.forward[level] != null && current.forward[level]!!.value < element) {
                current = current.forward[level]!!
            }
        }

        // Move to the next node at the bottom level
        current = current.forward[0] ?: return false

        // Check if the value matches the target
        return current.value == element
    }

    override fun insert(element: Int) {
        val level = randomLevel()
        val newNode = SkipListNode(element, level)
        val update = arrayOfNulls<SkipListNode>(level + 1)
        var current = head

        // Traverse from top to bottom
        for (i in maxLevel downTo 0) {
            // Move forward while the next node's value is less than the target
            while (current.forward[i] != null && current.forward[i]!!.value < element) {
                current = current.forward[i]!!
            }

            // If at the current level where insertion is happening, update the forward pointers
            if (i <= level) {
                update[i] = current
            }
        }

        // Update pointers to include the new node
        for (i in 0..level) {
            newNode.forward[i] = update[i]?.forward?.get(i)
            update[i]?.forward?.set(i, newNode)
        }

        size++
    }

    override fun delete(element: Int) {
        val update = arrayOfNulls<SkipListNode>(maxLevel + 1)
        var current = head

        // Traverse from top to bottom
        for (i in maxLevel downTo 0) {
            // Move forward while the next node's value is less than the target
            while (current.forward[i] != null && current.forward[i]!!.value < element) {
                current = current.forward[i]!!
            }
            update[i] = current
        }

        // Move to the next node at the bottom level
        current = current.forward[0] ?: return

        // Check if the value matches the target
        if (current.value == element) {
            // Update pointers to remove the node from the Skip List
            for (i in 0..maxLevel) {
                if (update[i]?.forward?.get(i) != current) {
                    break
                }
                update[i]?.forward?.set(i, current.forward[i])
            }

            size--
        }
    }

    override fun getElementsAsList(): List<Int> {
        val elements = mutableListOf<Int>()
        var current = head.forward[0]

        while (current != null) {
            elements.add(current.value)
            current = current.forward[0]
        }

        return elements
    }

    override fun print() {
        val sb = StringBuilder()
        for (i in maxLevel downTo 0) {
            sb.clear()
            var current: SkipListNode? = head

            sb.append("Level $i: ")
            while (current != null) {
                sb.append("${current.value} -> ")
                current = current.forward[i]
            }
            LOGGER.info("${sb}null")
        }
        LOGGER.info("")
    }

    // Function to generate a random level for a new node
    private fun randomLevel(): Int {
        var level = 0
        while (random.nextDouble() < 0.5 && level < maxLevel) {
            level++
        }
        return level
    }

    /**
     * In the context of data structures,
     * a sentinel is a special node or value that is used to simplify operations on the data structure.
     * In the case of a Skip List, a sentinel is often used as a placeholder or
     * marker that helps in handling edge cases and boundary conditions.
     *
     * Head Sentinel: In a Skip List, the head node often serves as a sentinel.
     * This node has the minimum possible value (or a value considered "negative infinity in the context of the data being stored).
     * This sentinel simplifies the insertion and search operations by providing a starting point for traversal.
     *
     * Tail Sentinel: Similarly, a tail sentinel may be used at the end of the Skip List,
     * with the maximum possible value (or a value considered "positive infinity").
     * This sentinel simplifies the traversal to the end of the Skip List.
     */
    private fun createSentinel(): Int {
        // Create a sentinel value representing the minimum value for comparison
        return Int.MIN_VALUE
    }
}
