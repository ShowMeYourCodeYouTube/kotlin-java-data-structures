package com.showmeyourcode.projects.datastructures.kotlin.custom.graph

import java.util.*

class WeightedGraph(override val representation: WeightedGraphRepresentation) : BaseGraph(representation) {

    fun addEdge(vertex1: Int, vertex2: Int, weight: Double) {
        representation.addEdge(vertex1, vertex2, weight)
    }

    /**
     *
     * A minimum spanning tree (MST) is defined as a spanning tree that has the minimum weight among all the possible spanning trees.
     *
     * A spanning tree is a connected graph using all vertices in which there are no circuits.
     * In other words, there is a path from any vertex to any other vertex, but no circuits.
     */
    fun primMST(): List<Pair<Int, Int>> {
        val mstEdges = mutableListOf<Pair<Int, Int>>()
        val visited = BooleanArray(representation.vertices())

        // Choose an arbitrary starting vertex (can start with any vertex)
        val startVertex = 0

        // Priority queue to store edges based on weight
        val priorityQueue = PriorityQueue<Pair<Int, Pair<Int, Double>>>(compareBy { it.second.second })

        // Add edges from the starting vertex to the priority queue
        for ((neighbor, weight) in representation.getNeighborsWithWeights(startVertex)) {
            priorityQueue.add(Pair(startVertex, Pair(neighbor, weight)))
        }

        visited[startVertex] = true

        while (priorityQueue.isNotEmpty()) {
            val (currentVertex, edge) = priorityQueue.poll()

            if (!visited[edge.first]) {
                visited[edge.first] = true
                mstEdges.add(Pair(currentVertex, edge.first))

                // Add edges from the newly added vertex to the priority queue
                for ((neighbor, weight) in representation.getNeighborsWithWeights(edge.first)) {
                    priorityQueue.add(Pair(edge.first, Pair(neighbor, weight)))
                }
            }
        }

        return mstEdges
    }

    /**
     *
     * A minimum spanning tree (MST) is defined as a spanning tree that has the minimum weight among all the possible spanning trees.
     *
     * A spanning tree is a connected graph using all vertices in which there are no circuits.
     * In other words, there is a path from any vertex to any other vertex, but no circuits.
     */
    fun kruskalMST(): List<Pair<Int, Int>> {
        val mstEdges = mutableListOf<Pair<Int, Int>>()

        // Collect all edges and sort them by weight
        val edges = mutableListOf<Pair<Pair<Int, Int>, Double>>()
        for (vertex1 in 0 until representation.vertices()) {
            for ((vertex2, weight) in representation.getNeighborsWithWeights(vertex1)) {
                edges.add(Pair(Pair(vertex1, vertex2), weight))
            }
        }
        edges.sortBy { it.second }

        // Use Union-Find to keep track of connected components
        val unionFind = UnionFind(representation.vertices())

        for ((edge, weight) in edges) {
            val (vertex1, vertex2) = edge

            // Check if adding this edge creates a cycle
            if (!unionFind.connected(vertex1, vertex2)) {
                mstEdges.add(edge)
                unionFind.union(vertex1, vertex2)
            }
        }

        return mstEdges
    }

    /**
     * Dijkstra's algorithm is an algorithm for finding the shortest paths between nodes in a weighted graph,
     * which may represent, for example, road networks.
     */
    fun dijkstra(startingVertex: Int): List<Double> {
        val distances = MutableList(representation.vertices()) { Double.POSITIVE_INFINITY }
        val priorityQueue = PriorityQueue<Pair<Int, Double>>(compareBy { it.second })

        distances[startingVertex] = 0.0
        priorityQueue.add(Pair(startingVertex, 0.0))

        while (priorityQueue.isNotEmpty()) {
            val (currentVertex, currentDistance) = priorityQueue.poll()

            // optimization
            if (currentDistance > distances[currentVertex]) continue

            for ((neighbor, weight) in representation.getNeighborsWithWeights(currentVertex)) {
                val newDistance = currentDistance + weight
                if (newDistance < distances[neighbor]) {
                    distances[neighbor] = newDistance
                    priorityQueue.add(Pair(neighbor, newDistance))
                }
            }
        }

        return distances
    }
}

/**
 * Union-Find (Disjoint Set Union) data structure
 *
 * Union-Find is used to determine the connected components in a graph.
 * We can determine whether 2 nodes are in the same connected component or not in the graph.
 * We can also determine that by adding an edge between 2 nodes whether it leads to cycle in the graph or not.
 *
 * Reference: https://www.hackerearth.com/practice/notes/disjoint-set-union-union-find/
 */
class UnionFind(size: Int) {
    private val parent: IntArray = IntArray(size) { it }

    fun find(x: Int): Int {
        if (parent[x] != x) {
            parent[x] = find(parent[x])
        }
        return parent[x]
    }

    fun union(x: Int, y: Int) {
        val rootX = find(x)
        val rootY = find(y)
        if (rootX != rootY) {
            parent[rootX] = rootY
        }
    }

    fun connected(x: Int, y: Int): Boolean {
        return find(x) == find(y)
    }
}
