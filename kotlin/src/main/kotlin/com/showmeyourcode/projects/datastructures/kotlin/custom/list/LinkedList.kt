package com.showmeyourcode.projects.datastructures.kotlin.custom.list

/**
 * A common interface for single and double linked lists.
 * It was inspired by java.util.LinkedList.
 */
interface LinkedList<E> {

    /**
     * Returns the first element in this list.
     *
     * @return the last element in this list
     * @throws NoSuchElementException if this list is empty
     */
    @Throws(NoSuchElementException::class)
    fun getFirst(): E

    /**
     * Returns the last element in this list.
     *
     * @return the last element in this list
     * @throws NoSuchElementException if this list is empty
     */
    @Throws(NoSuchElementException::class)
    fun getLast(): E

    /**
     * Removes and returns the first element from this list.
     *
     * @return the first element from this list
     * @throws NoSuchElementException if this list is empty
     */
    @Throws(NoSuchElementException::class)
    fun removeFirst(): E

    /**
     * Removes and returns the last element from this list.
     *
     * @return the last element from this list
     * @throws NoSuchElementException if this list is empty
     */
    @Throws(NoSuchElementException::class)
    fun removeLast(): E

    /**
     * Inserts the specified element at the beginning of this list.
     *
     * @param e the element to add
     */
    fun addFirst(e: E)

    /**
     * Appends the specified element to the end of this list.
     *
     *
     * This method is equivalent to [.add].
     *
     * @param e the element to add
     */
    fun addLast(e: E)

    /**
     * Returns `true` if this list contains the specified element.
     * More formally, returns `true` if and only if this list contains
     * at least one element `e` such that
     * `Objects.equals(o, e)`.
     *
     * @param o element whose presence in this list is to be tested
     * @return `true` if this list contains the specified element
     */
    fun contains(o: E): Boolean

    /**
     * Returns the number of elements in this list.
     *
     * @return the number of elements in this list
     */
    fun size(): Int

    /**
     * Appends the specified element to the end of this list.
     *
     *
     * This method is equivalent to [.addLast].
     *
     * @param e element to be appended to this list
     * @return `true` (as specified by [Collection.add])
     */
    fun add(e: E): Boolean

    /**
     * Removes the first occurrence of the specified element from this list,
     * if it is present.  If this list does not contain the element, it is
     * unchanged.  More formally, removes the element with the lowest index
     * `i` such that
     * `Objects.equals(o, get(i))`
     * (if such an element exists).  Returns `true` if this list
     * contained the specified element (or equivalently, if this list
     * changed as a result of the call).
     *
     * @param o element to be removed from this list, if present
     * @return `true` if this list contained the specified element
     */
    fun remove(o: Any): Boolean

    /**
     * Appends all of the elements in the specified collection to the end of
     * this list, in the order that they are returned by the specified
     * collection's iterator.  The behavior of this operation is undefined if
     * the specified collection is modified while the operation is in
     * progress.  (Note that this will occur if the specified collection is
     * this list, and it's nonempty.)
     *
     * @param c collection containing elements to be added to this list
     * @return `true` if this list changed as a result of the call
     */
    fun addAll(c: Collection<E>): Boolean

    /**
     * Inserts all of the elements in the specified collection into this
     * list, starting at the specified position.  Shifts the element
     * currently at that position (if any) and any subsequent elements to
     * the right (increases their indices).  The new elements will appear
     * in the list in the order that they are returned by the
     * specified collection's iterator.
     *
     * @param index index at which to insert the first element
     * from the specified collection
     * @param c collection containing elements to be added to this list
     * @return `true` if this list changed as a result of the call
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    fun addAll(index: Int, c: Collection<E>): Boolean

    /**
     * Removes all of the elements from this list.
     * The list will be empty after this call returns.
     */
    fun clear()

    /**
     * Returns the element at the specified position in this list.
     *
     * @param index index of the element to return
     * @return the element at the specified position in this list
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    operator fun get(index: Int): E

    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     *
     * @param index index of the element to replace
     * @param element element to be stored at the specified position
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    operator fun set(index: Int, element: E): E

    /**
     * Inserts the specified element at the specified position in this list.
     * Shifts the element currently at that position (if any) and any
     * subsequent elements to the right (adds one to their indices).
     *
     * @param index index at which the specified element is to be inserted
     * @param element element to be inserted
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    fun add(index: Int, element: E)

    /**
     * Removes the element at the specified position in this list.  Shifts any
     * subsequent elements to the left (subtracts one from their indices).
     * Returns the element that was removed from the list.
     *
     * @param index the index of the element to be removed
     * @return the element previously at the specified position
     * @throws IndexOutOfBoundsException {@inheritDoc}
     */
    fun remove(index: Int): E

    /**
     * Reverse the linked list.
     */
    fun reverse()

    fun toList(): List<E>
}
