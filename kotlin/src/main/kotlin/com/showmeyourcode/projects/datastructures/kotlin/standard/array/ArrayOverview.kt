package com.showmeyourcode.projects.datastructures.kotlin.standard.array

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import com.showmeyourcode.projects.datastructures.kotlin.common.word

/**
 * Ref: https://www.simplilearn.com/tutorials/data-structure-tutorial/arrays-in-data-structure
 */
object ArrayOverview {

    fun main() {
        LOGGER.info(
            """
            An array is a linear data structure that collects elements of the same data type 
            and stores them in contiguous and adjacent memory locations. 
            
            It's is mutable with fixed size by default, which means we can perform both read and writes operations on elements of array.
            
            """.trimIndent()
        )
        arrays()
    }

    // var - value/reference assigned to a variable can be changed at any point of time.
    // val - value/reference can be assigned only once to a variable and can't be changed later point in the execution.
    private fun arrays() {
        var mutableArray1 = arrayOf(word)
        val mutableArray2 = arrayOf(word)

        // this operation will reassign the reference, so we have to use var
        mutableArray1 += "TEST"
        LOGGER.info(mutableArray1.contentToString())

        // because the structure is mutable, we can 'set' values
        mutableArray2[0] = "NEW STRING"
        mutableArray1[1] = "ANOTHER STRING"

        LOGGER.info("Array 1: ${mutableArray1.contentToString()} Array2: ${mutableArray2.contentToString()}")
    }
}
