package com.showmeyourcode.projects.datastructures.kotlin.custom.tree

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER

class BinaryTree<T : Comparable<T>> : Tree<T> {
    var root: TreeNode<T>? = null

    override fun insert(value: T) {
        fun insertRecursive(node: TreeNode<T>?, value: T): TreeNode<T> {
            if (node == null) {
                return TreeNode(value)
            }

            if (value < node.value) {
                node.left = insertRecursive(node.left, value)
            } else if (value > node.value) {
                node.right = insertRecursive(node.right, value)
            }

            return node
        }

        root = insertRecursive(root, value)
    }

    override fun delete(value: T) {
        fun deleteRecursive(node: TreeNode<T>?, value: T): TreeNode<T>? {
            if (node == null) {
                return null
            }

            if (value < node.value) {
                node.left = deleteRecursive(node.left, value)
            } else if (value > node.value) {
                node.right = deleteRecursive(node.right, value)
            } else {
                if (node.left == null) {
                    return node.right
                } else if (node.right == null) {
                    return node.left
                }

                node.value = minValue(node.right!!)
                node.right = deleteRecursive(node.right, node.value)
            }
            return node
        }

        root = deleteRecursive(root, value)
    }

    private fun minValue(node: TreeNode<T>): T {
        var current = node
        while (current.left != null) {
            current = current.left!!
        }
        return current.value
    }

    override fun find(value: T): Boolean {
        return findRecursive(root, value)
    }

    private fun findRecursive(node: TreeNode<T>?, value: T): Boolean {
        if (node == null) {
            return false
        }

        if (value == node.value) {
            return true
        }

        return if (value < node.value) {
            findRecursive(node.left, value)
        } else {
            findRecursive(node.right, value)
        }
    }

    fun inorderTraversal(node: TreeNode<T>? = root, result: MutableList<T> = mutableListOf()): List<T> {
        if (node != null) {
            inorderTraversal(node.left, result)
            result.add(node.value)
            inorderTraversal(node.right, result)
        }
        return result
    }

    fun preorderTraversal(node: TreeNode<T>? = root, result: MutableList<T> = mutableListOf()): List<T> {
        if (node != null) {
            result.add(node.value)
            preorderTraversal(node.left, result)
            preorderTraversal(node.right, result)
        }
        return result
    }

    fun postorderTraversal(node: TreeNode<T>? = root, result: MutableList<T> = mutableListOf()): List<T> {
        if (node != null) {
            postorderTraversal(node.left, result)
            postorderTraversal(node.right, result)
            result.add(node.value)
        }
        return result
    }

    fun levelOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        val queue: MutableList<TreeNode<T>?> = mutableListOf()

        root?.let { queue.add(it) }

        while (queue.isNotEmpty()) {
            val current = queue.removeAt(0)
            current?.let {
                result.add(it.value)
                it.left?.let { left -> queue.add(left) }
                it.right?.let { right -> queue.add(right) }
            }
        }

        return result
    }

    /**
     * Prints the binary tree for visualization.
     */
    override fun printTree() {
        root?.let {
            val queue: MutableList<TreeNode<T>?> = mutableListOf(it)

            while (queue.isNotEmpty()) {
                val levelSize = queue.size

                for (i in 0 until levelSize) {
                    val current = queue.removeAt(0)
                    current?.let {
                        print("${it.value} ")

                        if (it.left != null) queue.add(it.left)
                        if (it.right != null) queue.add(it.right)
                    }
                }

                LOGGER.info("") // Move to the next line for the next level
            }
        }
    }

    /**
     * Checks if the binary tree is complete.
     *
     * Definition:
     * A binary tree is complete if all levels, except possibly the last, are completely filled,
     * and all nodes are as left as possible.
     */
    fun isComplete(): Boolean {
        return isCompleteRecursive(root, 0, size()) == size()
    }

    private fun isCompleteRecursive(node: TreeNode<T>?, index: Int, totalNodes: Int): Int {
        if (node == null) {
            return 0
        }

        return 1 + isCompleteRecursive(node.left, 2 * index + 1, totalNodes) +
            isCompleteRecursive(node.right, 2 * index + 2, totalNodes)
    }

    /**
     * Checks if the binary tree is balanced.
     *
     * Definition:
     * A binary tree is balanced if the height of the two subtrees of any node
     * never differs by more than one.
     */
    fun isBalanced(): Boolean {
        return isBalancedRecursive(root) != -1
    }

    private fun isBalancedRecursive(node: TreeNode<T>?): Int {
        if (node == null) {
            return 0
        }

        val leftHeight = isBalancedRecursive(node.left)
        if (leftHeight == -1) {
            return -1
        }

        val rightHeight = isBalancedRecursive(node.right)
        if (rightHeight == -1) {
            return -1
        }

        if (Math.abs(leftHeight - rightHeight) > 1) {
            return -1
        }

        return 1 + Math.max(leftHeight, rightHeight)
    }

    /**
     * Checks if the binary tree is perfect.
     *
     * Definition:
     * A binary tree is perfect if all interior nodes have two children,
     * and all leaves have the same depth or same level.
     */
    fun isPerfect(): Boolean {
        val height = height(root)
        return isPerfectRecursive(root, height, 0)
    }

    private fun isPerfectRecursive(node: TreeNode<T>?, height: Int, level: Int): Boolean {
        if (node == null) {
            return true
        }

        if (node.left == null && node.right == null) {
            return level == height
        }

        return (node.left != null && node.right != null) &&
            isPerfectRecursive(node.left, height, level + 1) &&
            isPerfectRecursive(node.right, height, level + 1)
    }

    /**
     * Checks if the binary tree is full.
     *
     * Definition:
     * A binary tree is full if every node has either zero or two children.
     */
    fun isFull(): Boolean {
        return isFullRecursive(root)
    }

    private fun isFullRecursive(node: TreeNode<T>?): Boolean {
        if (node == null) {
            return true
        }

        if (node.left == null && node.right == null) {
            return true
        }

        return node.left != null && node.right != null &&
            isFullRecursive(node.left) && isFullRecursive(node.right)
    }

    private fun height(node: TreeNode<T>?): Int {
        if (node == null) {
            return 0
        }

        val leftHeight = height(node.left)
        val rightHeight = height(node.right)

        return 1 + Math.max(leftHeight, rightHeight)
    }

    private fun size(): Int {
        return sizeRecursive(root)
    }

    private fun sizeRecursive(node: TreeNode<T>?): Int {
        return node?.let { 1 + sizeRecursive(it.left) + sizeRecursive(it.right) } ?: 0
    }

    fun isDegenerate(): Boolean {
        return isDegenerateRecursive(root) != -1
    }

    private fun isDegenerateRecursive(node: TreeNode<T>?): Int {
        if (node == null) {
            return 0
        }

        val leftHeight = isDegenerateRecursive(node.left)
        if (leftHeight == -1) {
            return -1
        }

        val rightHeight = isDegenerateRecursive(node.right)
        if (rightHeight == -1) {
            return -1
        }

        // A degenerate tree has one child (either left or right), not both.
        return if ((node.left != null && node.right == null) || (node.left == null && node.right != null)) {
            1 + Math.max(leftHeight, rightHeight)
        } else {
            -1
        }
    }
}
