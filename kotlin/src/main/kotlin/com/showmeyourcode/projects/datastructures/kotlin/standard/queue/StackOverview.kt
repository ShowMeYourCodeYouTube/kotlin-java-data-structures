package com.showmeyourcode.projects.datastructures.kotlin.standard.queue

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER

object StackOverview {

    fun main() {
        LOGGER.info(
            """
            A stack is a linear data structure that follows the principle of Last In First Out (LIFO). 
            This means the last element inserted inside the stack is removed first.
            
            """.trimIndent()
        )
        kotlinStack()
    }

    /**
     * ArrayDeque can be used as Stack or Queue.
     * Reference: https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/-array-deque/
     */
    private fun kotlinStack() {
        val deck = ArrayDeque<Int>()
        deck.add(1)
        deck.add(2)
        deck.add(3)
        LOGGER.info(deck.joinToString(prefix = "[", postfix = "]"))

        deck.removeLast()
        LOGGER.info("Remove the last element (LIFO): ${deck.joinToString(prefix = "[", postfix = "]")}")
    }
}
