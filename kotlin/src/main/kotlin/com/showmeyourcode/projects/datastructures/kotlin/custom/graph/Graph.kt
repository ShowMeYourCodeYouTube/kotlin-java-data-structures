package com.showmeyourcode.projects.datastructures.kotlin.custom.graph

import java.util.*

enum class GraphType {
    DIRECTED,
    UNDIRECTED
}

open class BaseGraph(protected open val representation: GraphRepresentation) {

    fun bfs(startingVertex: Int): List<Int> {
        val visited = mutableListOf<Int>()
        val queue: Queue<Int> = LinkedList()

        visited.add(startingVertex)
        queue.add(startingVertex)

        while (queue.isNotEmpty()) {
            val currentVertex = queue.poll()

            for (neighbor in representation.getNeighbors(currentVertex)) {
                if (neighbor !in visited) {
                    visited.add(neighbor)
                    queue.add(neighbor)
                }
            }
        }

        return visited
    }

    fun dfs(startingVertex: Int, visited: MutableList<Int> = mutableListOf()): List<Int> {
        visited.add(startingVertex)

        for (neighbor in representation.getNeighbors(startingVertex)) {
            if (neighbor !in visited) {
                dfs(neighbor, visited)
            }
        }
        return visited
    }

    fun toList(): List<List<Int>> {
        return representation.toList()
    }

    fun hasCycleBFS(): Boolean {
        val inDegrees = IntArray(representation.vertices()) { 0 }
        val queue: Queue<Int> = LinkedList()

        for (i in 0 until representation.vertices()) {
            for (j in representation.getNeighbors(i)) {
                inDegrees[j]++
            }
        }

        for (v in 0 until representation.vertices()) {
            if (inDegrees[v] == 0) {
                queue.add(v)
            }
        }

        var processedVertices = 0
        while (queue.isNotEmpty()) {
            val vertex = queue.poll()
            processedVertices++

            for (n in representation.getNeighbors(vertex)) {
                inDegrees[n]--
                if (inDegrees[n] == 0) {
                    queue.add(n)
                }
            }
        }

        return processedVertices != representation.vertices()
    }

    // FUNCTION hasCycleBFS(graph):
    //    vertices = graph.numberOfVertices()
    //    inDegrees = ARRAY of size vertices initialized to 0
    //    queue = EMPTY QUEUE
    //    processedVertices = 0
    //
    //    // Step 1: Calculate in-degrees of all vertices
    //    FOR vertex FROM 0 TO vertices - 1:
    //        FOR each neighbor IN graph.getNeighbors(vertex):
    //            inDegrees[neighbor]++
    //
    //    // Step 2: Add all vertices with in-degree 0 to the queue
    //    FOR vertex FROM 0 TO vertices - 1:
    //        IF inDegrees[vertex] == 0:
    //            queue.enqueue(vertex)
    //
    //    // Step 3: Process vertices in BFS manner
    //    WHILE queue is NOT EMPTY:
    //        current = queue.dequeue()
    //        processedVertices++
    //
    //        // Reduce the in-degree of all neighbors
    //        FOR each neighbor IN graph.getNeighbors(current):
    //            inDegrees[neighbor]--
    //            IF inDegrees[neighbor] == 0:
    //                queue.enqueue(neighbor)
    //
    //    // Step 4: Check if all vertices were processed
    //    IF processedVertices == vertices:
    //        RETURN False // No cycle
    //    ELSE:
    //        RETURN True  // Cycle detected

    fun hasCycleDFS(): Boolean {
        fun hasCycleDFS(vertex: Int, visited: BooleanArray, recursionStack: BooleanArray): Boolean {
            visited[vertex] = true
            recursionStack[vertex] = true

            for (neighbor in representation.getNeighbors(vertex)) {
                if (!visited[neighbor]) {
                    if (hasCycleDFS(neighbor, visited, recursionStack)) {
                        return true
                    }
                } else if (recursionStack[neighbor]) {
                    return true
                }
            }

            recursionStack[vertex] = false
            return false
        }
        val visited = BooleanArray(representation.vertices())
        val recursionStack = BooleanArray(representation.vertices())

        for (vertex in 0 until representation.vertices()) {
            if (!visited[vertex] && hasCycleDFS(vertex, visited, recursionStack)) {
                return true
            }
        }

        return false
    }

    /**
     * Simple Graph: A graph with no loops (edges connected at both ends to the same vertex) and no more than one edge between any two vertices.
     */
    fun isSimple(): Boolean {
        for (vertex in 0 until representation.vertices()) {
            val neighbors = representation.getNeighbors(vertex)
            val neighborSet = mutableSetOf<Int>()

            for (neighbor in neighbors) {
                if (neighbor == vertex || !neighborSet.add(neighbor)) {
                    return false
                }
            }
        }

        return true
    }

    /**
     * Regular Graph: A graph where each vertex has the same number of neighbors (i.e., each vertex has the same degree).
     */
    fun isRegular(): Boolean {
        if (representation.vertices() == 0) return true

        val degree = representation.getNeighbors(0).size

        for (vertex in 1 until representation.vertices()) {
            if (representation.getNeighbors(vertex).size != degree) {
                return false
            }
        }

        return true
    }

    /**
     * A graph with only one vertex and no edges.
     */
    fun isTrivial(): Boolean {
        return representation.vertices() == 1
    }

    /**
     * A complete graph is an undirected graph in which every pair of distinct vertices is connected by a unique edge.
     * In other words, every vertex in a complete graph is adjacent to all other vertices.
     */
    fun isComplete(): Boolean {
        val vertexCount = representation.vertices()
        for (vertex in 0 until vertexCount) {
            if (representation.getNeighbors(vertex).size != vertexCount - 1) {
                return false
            }
        }
        return true
    }

    /**
     * An acyclic graph is a graph having no graph cycles.
     * In graph theory, a cycle in a graph is a non-empty trail in which only the first and last vertices are equal.
     */
    fun isAcyclic(): Boolean {
        return !hasCycleDFS()
    }
}

class Graph(override val representation: GraphRepresentation) : BaseGraph(representation) {

    fun addEdge(vertex1: Int, vertex2: Int) {
        representation.addEdge(vertex1, vertex2)
    }
}
