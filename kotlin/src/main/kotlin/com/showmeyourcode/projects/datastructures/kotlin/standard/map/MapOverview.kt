package com.showmeyourcode.projects.datastructures.kotlin.standard.map

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER

object MapOverview {

    fun main() {
        LOGGER.info(
            """
                A map data structure (also known as a dictionary, associative array, or hash map) 
                is defined as a data structure that stores a collection of key-value pairs.
                
            """.trimIndent()
        )
        immutableMap()
        mutableHashMap()
        mutableMap()
    }

    private fun immutableMap() {
        val immutableMap = mapOf("5" to 5, "4" to 4, "3" to 3, "1" to 1, "2" to 2)
        LOGGER.info("All keys (immutable LinkedHashMap): ${immutableMap.keys}")
    }

    private fun mutableHashMap() {
        val mutableMap = hashMapOf("5" to 5, "4" to 4, "3" to 3, "1" to 1, "2" to 2)
        mutableMap["6"] = 6
        mutableMap.toMutableMap()
        LOGGER.info("All keys (mutable HashMap): ${mutableMap.keys}")
    }

    private fun mutableMap() {
        val mutableMap = mutableMapOf("5" to 5, "4" to 4, "3" to 3, "1" to 1, "2" to 2)
        mutableMap["6"] = 6
        LOGGER.info("All keys (mutable LinkedHashMap): ${mutableMap.keys}")
    }
}
