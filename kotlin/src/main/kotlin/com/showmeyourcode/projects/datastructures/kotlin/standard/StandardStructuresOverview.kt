package com.showmeyourcode.projects.datastructures.kotlin.standard

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import com.showmeyourcode.projects.datastructures.kotlin.common.word
import com.showmeyourcode.projects.datastructures.kotlin.standard.array.ArrayOverview
import com.showmeyourcode.projects.datastructures.kotlin.standard.array.MatrixOverview
import com.showmeyourcode.projects.datastructures.kotlin.standard.array.MultidimensionalArrayOverview
import com.showmeyourcode.projects.datastructures.kotlin.standard.concurrency.ConcurrencyOverview
import com.showmeyourcode.projects.datastructures.kotlin.standard.list.ListOverview
import com.showmeyourcode.projects.datastructures.kotlin.standard.map.MapOverview
import com.showmeyourcode.projects.datastructures.kotlin.standard.queue.QueueOverview
import com.showmeyourcode.projects.datastructures.kotlin.standard.queue.StackOverview
import com.showmeyourcode.projects.datastructures.kotlin.standard.set.SetOverview

fun main() {
    LOGGER.info("\nStarting Data Structure Overview Application - standard implementations (Kotlin)...")
    LOGGER.info("Test data: '$word'")

    LOGGER.info("\n===> ARRAY")
    ArrayOverview.main()

    LOGGER.info("\n===> MATRIX")
    MatrixOverview.main()

    LOGGER.info("\n===> MULTIDIMENSIONAL ARRAY")
    MultidimensionalArrayOverview.main()

    LOGGER.info("\n===> QUEUE")
    QueueOverview.main()

    LOGGER.info("\n===> STACK")
    StackOverview.main()

    LOGGER.info("\n===> LIST")
    ListOverview.main()

    LOGGER.info("\n===> SET")
    SetOverview.main()

    LOGGER.info("\n===> MAP")
    MapOverview.main()

    LOGGER.info("\n===> CONCURRENCY")
    ConcurrencyOverview.main()
}
