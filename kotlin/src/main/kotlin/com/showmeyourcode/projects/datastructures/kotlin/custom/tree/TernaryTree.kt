package com.showmeyourcode.projects.datastructures.kotlin.custom.tree

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER

class TernaryTreeNode<T : Comparable<T>>(var value: T) {
    var left: TernaryTreeNode<T>? = null
    var middle: TernaryTreeNode<T>? = null
    var right: TernaryTreeNode<T>? = null
}

class TernaryTree<T : Comparable<T>> {
    var root: TernaryTreeNode<T>? = null

    fun insert(value: T) {
        root = insertRecursive(root, value)
    }

    private fun insertRecursive(node: TernaryTreeNode<T>?, value: T): TernaryTreeNode<T> {
        if (node == null) {
            return TernaryTreeNode(value)
        }

        when {
            value < node.value -> node.left = insertRecursive(node.left, value)
            value > node.value -> node.right = insertRecursive(node.right, value)
            else -> node.middle = insertRecursive(node.middle, value)
        }

        return node
    }

    fun delete(value: T) {
        root = delete(root, value)
    }

    private fun delete(node: TernaryTreeNode<T>?, value: T): TernaryTreeNode<T>? {
        if (node == null) {
            return null
        }

        when {
            value < node.value -> node.left = delete(node.left, value)
            value > node.value -> node.right = delete(node.right, value)
            else -> {
                if (node.middle != null) {
                    node.middle = delete(node.middle, value)
                } else if (node.right != null) {
                    val successor = findMin(node.right!!)
                    node.value = successor.value
                    node.right = delete(node.right, successor.value)
                } else {
                    return null
                }
            }
        }

        return node
    }

    private fun findMin(node: TernaryTreeNode<T>): TernaryTreeNode<T> {
        var current = node
        while (current.left != null) {
            current = current.left!!
        }
        return current
    }

    fun find(value: T): Boolean {
        return findRecursive(root, value)
    }

    private fun findRecursive(node: TernaryTreeNode<T>?, value: T): Boolean {
        if (node == null) {
            return false
        }

        return when {
            value < node.value -> findRecursive(node.left, value)
            value > node.value -> findRecursive(node.right, value)
            else -> true
        }
    }

    fun preOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        preOrderRecursive(root, result)
        return result
    }

    private fun preOrderRecursive(node: TernaryTreeNode<T>?, result: MutableList<T>) {
        if (node != null) {
            result.add(node.value)
            preOrderRecursive(node.left, result)
            preOrderRecursive(node.middle, result)
            preOrderRecursive(node.right, result)
        }
    }

    fun inOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        inOrderRecursive(root, result)
        return result
    }

    private fun inOrderRecursive(node: TernaryTreeNode<T>?, result: MutableList<T>) {
        if (node != null) {
            inOrderRecursive(node.left, result)
            result.add(node.value)
            inOrderRecursive(node.middle, result)
            inOrderRecursive(node.right, result)
        }
    }

    fun postOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        postOrderRecursive(root, result)
        return result
    }

    private fun postOrderRecursive(node: TernaryTreeNode<T>?, result: MutableList<T>) {
        if (node != null) {
            postOrderRecursive(node.left, result)
            postOrderRecursive(node.middle, result)
            postOrderRecursive(node.right, result)
            result.add(node.value)
        }
    }

    fun levelOrderTraversal(): List<T> {
        val result = mutableListOf<T>()
        if (root != null) {
            val queue = mutableListOf<TernaryTreeNode<T>>()
            queue.add(root!!)

            while (queue.isNotEmpty()) {
                val current = queue.removeAt(0)
                result.add(current.value)

                if (current.left != null) {
                    queue.add(current.left!!)
                }
                if (current.middle != null) {
                    queue.add(current.middle!!)
                }
                if (current.right != null) {
                    queue.add(current.right!!)
                }
            }
        }
        return result
    }
}

object TernaryTreeOverview {

    fun main() {
        LOGGER.info("\n>>> Ternary Tree:")
        val ternaryTree = TernaryTree<Int>()

        // Example: Insert nodes into the ternary tree
        ternaryTree.insert(10)
        ternaryTree.insert(5)
        ternaryTree.insert(15)
        ternaryTree.insert(3)
        ternaryTree.insert(7)

        // Example: Perform traversals
        val preOrderResult = ternaryTree.preOrderTraversal()
        LOGGER.info("\nPre-order traversal: $preOrderResult")

        val inOrderResult = ternaryTree.inOrderTraversal()
        LOGGER.info("In-order traversal: $inOrderResult")

        val postOrderResult = ternaryTree.postOrderTraversal()
        LOGGER.info("Post-order traversal: $postOrderResult")

        // Example: Delete a node and find a node
        ternaryTree.delete(5)
        val inOrderResultAfterDelete = ternaryTree.inOrderTraversal()
        LOGGER.info("In-order traversal after deleting 5: $inOrderResultAfterDelete")

        val valueToFind = 10
        val found = ternaryTree.find(valueToFind)
        LOGGER.info("Is $valueToFind found in the tree? $found")
    }
}
