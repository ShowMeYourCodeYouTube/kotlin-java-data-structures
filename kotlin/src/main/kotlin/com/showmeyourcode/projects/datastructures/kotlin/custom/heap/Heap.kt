package com.showmeyourcode.projects.datastructures.kotlin.custom.heap

interface Heap {

    companion object {
        const val EMPTY_HEAP_ERROR = "Heap is empty"
    }

    fun pop(): Int

    fun peek(): Int

    fun size(): Int

    fun toList(): List<Int>

    operator fun plusAssign(value: Int)
}
