package com.showmeyourcode.projects.datastructures.kotlin.custom.graph

interface GraphRepresentation {
    fun getNeighbors(vertex: Int): List<Int>
    fun toList(): List<List<Int>>
    fun addEdge(vertex1: Int, vertex2: Int)
    fun vertices(): Int
    fun graphType(): GraphType

    /**
     * The 'adjacencyList' should have size equal to number of nodes.
     * The position in the list corresponds to a list of connections/edges to particular vertices
     * e.g. ((1,2),(2),() represents a graph where:
     * - vertex '0' has edges to 1,2 vertices,
     * - vertex '1' has a single edge to 2,
     * - vertex 2 doesn't have any outgoing edges.
     */
    fun addEdges(adjacencyList: List<List<Int>>) {
        adjacencyList.forEachIndexed { index, neighbors ->
            neighbors.forEach { neighbor ->
                addEdge(index, neighbor)
            }
        }
    }
}

class AdjacencyList(private val vertices: Int, private val graphType: GraphType) : GraphRepresentation {
    private val adjacencyList: MutableList<MutableList<Int>> = MutableList(vertices) { mutableListOf() }

    override fun addEdge(vertex1: Int, vertex2: Int) {
        adjacencyList[vertex1].add(vertex2)
        //  This ensures the edge is bidirectional, meaning there is also an edge from vertex2 to vertex1.
        //
        // In the Undirected implementation both vertices are marked as having an edge to each other,
        // while in the Directed implementation the first is marked as having an edge to the second.
        // Reference: https://math.stackexchange.com/questions/1434365/should-an-undirected-graph-and-a-directed-graph-with-the-same-vertices-and-edges
        if (graphType == GraphType.UNDIRECTED) {
            adjacencyList[vertex2].add(vertex1)
        }
    }

    override fun getNeighbors(vertex: Int): List<Int> {
        return adjacencyList[vertex]
    }

    override fun toList(): List<List<Int>> {
        return adjacencyList.map { it.toList() }
    }

    override fun vertices(): Int {
        return vertices
    }

    override fun graphType(): GraphType {
        return graphType
    }
}

class AdjacencyMatrix(private val vertices: Int, private val graphType: GraphType) : GraphRepresentation {
    private val adjacencyMatrix: Array<BooleanArray> = Array(vertices) { BooleanArray(vertices) }

    override fun addEdge(vertex1: Int, vertex2: Int) {
        adjacencyMatrix[vertex1][vertex2] = true
        //  This ensures the edge is bidirectional, meaning there is also an edge from vertex2 to vertex1.
        if (graphType == GraphType.UNDIRECTED) {
            adjacencyMatrix[vertex2][vertex1] = true
        }
    }

    override fun getNeighbors(vertex: Int): List<Int> {
        return adjacencyMatrix[vertex].indices.filter { adjacencyMatrix[vertex][it] }
    }

    override fun toList(): List<List<Int>> {
        return adjacencyMatrix.map { row ->
            row.indices.filter { row[it] }.toList()
        }
    }

    override fun vertices(): Int {
        return vertices
    }

    override fun graphType(): GraphType {
        return graphType
    }
}
