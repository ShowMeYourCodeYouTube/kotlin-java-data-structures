package com.showmeyourcode.projects.datastructures.kotlin.exercise.maps

import java.util.*

data class SaleRecord(val product: String, val amount: Double, val month: String)

fun analyzeMonthlySales(sales: List<SaleRecord>): SortedMap<String, Double> {
    val monthlySales = sortedMapOf<String, Double>()

    for (sale in sales) {
        monthlySales.merge(sale.month, sale.amount) { current, new -> current + new }
    }

    return monthlySales
}
