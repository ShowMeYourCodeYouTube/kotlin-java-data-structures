package com.showmeyourcode.projects.datastructures.kotlin.standard.set

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import com.showmeyourcode.projects.datastructures.kotlin.common.printAll
import com.showmeyourcode.projects.datastructures.kotlin.common.word

object SetOverview {

    fun main() {
        LOGGER.info(
            """
            A set is a data structure that stores unique elements of the same type.
            It is an unordered collection of objects in which duplicate values cannot be stored.
            
            """.trimIndent()
        )
        immutableSet()
        mutableSet()
    }

    private fun mutableSet() {
        // by default LinkedHashSet - the ordered is preserved (compare with HashSet below)
        val mutableSet = mutableSetOf("A")
        mutableSet.add("C")
        mutableSet.add("X")
        mutableSet.add("B")

        printAll(mutableSet, "Mutable set(LinkedHashSet): ")

        // Below you can see that the output seems to be ordered/sorted, so... does it mean that HashSet is ordered? No. Below you can find a detailed explanation.
        // It makes no guarantees as to the iteration order of the set; in particular, it does not guarantee that the order will remain constant over time.
        // The hashCode of Strings of length 1 hash just the sole char, and its hash code is its own numeric value. Voilà, all is ordered.
        //
        // Ref: https://stackoverflow.com/questions/48560736/why-is-hashset-maintaining-natural-alphabetical-order
        val mutableHashSet = HashSet<String>()
        mutableHashSet.add("C")
        mutableHashSet.add("A")
        mutableHashSet.add("B")
        mutableHashSet.add("E")
        mutableHashSet.add("F")
        mutableHashSet.add("D")
        mutableHashSet.remove("E")

        printAll(mutableHashSet, "Mutable set(HashSet): ")
    }

    private fun immutableSet() {
        val immutableSet = setOf(word)
        printAll(immutableSet, "Immutable set: ")
    }
}
