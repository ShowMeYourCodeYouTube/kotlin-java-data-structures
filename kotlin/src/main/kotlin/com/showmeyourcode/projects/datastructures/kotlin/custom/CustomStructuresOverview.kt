package com.showmeyourcode.projects.datastructures.kotlin.custom

import com.showmeyourcode.projects.datastructures.kotlin.common.Logging.LOGGER
import com.showmeyourcode.projects.datastructures.kotlin.common.word
import com.showmeyourcode.projects.datastructures.kotlin.custom.tree.TreesOverview

fun main() {
    LOGGER.info("\nStarting Data Structure Overview Application - custom implementations (Kotlin)...")
    LOGGER.info("Test data: '$word'")

    LOGGER.info("\n===> LIST")
    LinkedListOverview.main()
    SkipListOverview.main()

    LOGGER.info("\n===> Heap")
    HeapOverview.main()

    LOGGER.info("\n===> Tree")
    TreesOverview.main()

    LOGGER.info("\n===> GRAPH")
    GraphOverview.main()
}
