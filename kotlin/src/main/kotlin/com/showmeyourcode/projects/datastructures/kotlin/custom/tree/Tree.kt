package com.showmeyourcode.projects.datastructures.kotlin.custom.tree

class TreeNode<T>(var value: T) {
    var left: TreeNode<T>? = null
    var right: TreeNode<T>? = null
}

sealed interface Tree<T : Comparable<T>> {

    fun insert(value: T)

    fun delete(value: T)

    fun find(value: T): Boolean

    fun printTree()
}
