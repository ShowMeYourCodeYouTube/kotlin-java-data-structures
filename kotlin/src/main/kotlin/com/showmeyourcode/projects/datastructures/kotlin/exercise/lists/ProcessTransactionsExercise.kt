@file:Suppress("ktlint:filename")

package com.showmeyourcode.projects.datastructures.kotlin.exercise.lists

data class TransactionSummary(
    val sortedIncomes: List<Int>,
    val sortedExpenses: List<Int>,
    val totalIncome: Int,
    val totalExpense: Int,
    val netBalance: Int
)

/**
 * You are given a list of transactions represented as strings.
 * Each transaction is in the format: "type:amount", where type is either "income" or "expense", and amount is a positive integer.
 * Your task is to write a Kotlin function that processes this list of transactions to provide a summary.
 * <br>
 */
fun processTransactions(transactions: List<String>): TransactionSummary {
    val incomes = mutableListOf<Int>()
    val expenses = mutableListOf<Int>()

    for (transaction in transactions) {
        val (type, amountStr) = transaction.split(":")
        val amount = amountStr.toInt()
        when (type) {
            "income" -> incomes.add(amount)
            "expense" -> expenses.add(amount)
        }
    }

    val totalIncome = incomes.sum()
    val totalExpense = expenses.sum()
    val netBalance = totalIncome - totalExpense

    return TransactionSummary(
        sortedIncomes = incomes.sortedDescending(),
        sortedExpenses = expenses.sortedDescending(),
        totalIncome = totalIncome,
        totalExpense = totalExpense,
        netBalance = netBalance
    )
}
