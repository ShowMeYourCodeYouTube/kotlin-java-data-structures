plugins {
    id("org.jlleitschuh.gradle.ktlint")
    id("info.solidsoft.pitest")
}

val coroutinesVersion: String = "1.6.4"
val arrowVersion: String = "1.1.5"
val kotestVersion: String = "5.4.2"
val arrowTestVersion: String = "1.2.5"
val mockkVersion: String = "1.12.8"

kotlin {
    jvmToolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

configure<info.solidsoft.gradle.pitest.PitestPluginExtension> {
    targetClasses.set(
        listOf(
            "com.showmeyourcode.projects.datastructures.kotlin.custom.*"
        )
    )
}

dependencies {
    api(project(":common"))

    implementation("org.jetbrains.kotlin:kotlin-stdlib")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
    // functional programming
    implementation("io.arrow-kt:arrow-core-jvm:$arrowVersion")

    testImplementation("io.mockk:mockk:$mockkVersion")
    testImplementation("io.kotest:kotest-runner-junit5-jvm:$kotestVersion")
    testImplementation("io.kotest:kotest-assertions-core-jvm:$kotestVersion")
    testImplementation("io.kotest.extensions:kotest-assertions-arrow-jvm:$arrowTestVersion")
    testImplementation("io.kotest:kotest-property-jvm:$kotestVersion")
    testImplementation("io.kotest:kotest-assertions-json-jvm:$kotestVersion")
    testImplementation("io.kotest.extensions:kotest-property-arrow:$arrowTestVersion")
    testImplementation("io.kotest:kotest-assertions-core-jvm:$kotestVersion")
}
